#!/bin/bash  

# ============ Environment Variables ============= 
SAI="sudo apt install -y "
STD='\033[0;0;39m'
RED='\033[0;41;30m'
# set $D to display date and time 
	D=$(date +"%T")
# Start in users home directory
	cd $HOME
	WorkDir=($HOME/AnsibleWorkings/$JobID)

# =================== Modules =====================

InstallAnsibleGlobal(){
# Setup Ansible
    sudo apt update
    $SAI software-properties-common ansible
# Check your yml	
#	$SAI yamllint ansible-lint
# create a working folder for ansible projects
	mkdir $HOME/AnsibleWorkings
	ValidateInstall
}

NewProject(){
# Capture the new request ID data
    read -rp "Enter the given Project Name (leave blank to quit): " JobID 
# validate data was entered.  
    if [ "$JobID" = "" ]; 
    then 
    lastmessage="Please enter a Project Name"
	else 
# Prepare test folder.
    echo "$JobID"
	fi
# Validate folder does not already exist and inform user if so.
if [ ! -f "$WorkDir" ]; 
	then 
# create and enter the new test folder.     
    mkdir -pv $HOME/AnsibleWorkings/$JobID
	cd $HOME/AnsibleWorkings/$JobID
# Create required folders & files.    
	touch logs.txt
	touch $JobID.yml
	touch inventory.yml
	touch hosts
# Setup ansible config to use created project folder
	echo -e "[defaults] 
	inventory = $HOME/AnsibleWorkings/$JobID/hosts " >> $HOME/AnsibleWorkings/$JobID/ansible.cfg

	
	lastmessage="Created folder in: $WorkDir." 
    echo "A1- New Project $WorkDir created - $D $lastmessage" >> $HOME/AnsibleWorkings/$JobID/logs.txt
	else 
	lastmessage="Sorry $USER, That name already exists, try again."
	NewProject
	fi
}

LoadProject(){
# require selector menu for displayed folders.
# Display existing projects (list folders). 
    ls $HOME/AnsibleWorkings

# Capture the request ID data
    read -rp "Enter the Project Name (leave blank to quit): " JobID 
# Create folder based on Request ID.  
    if [ "$JobID" = "" ]; 
    then 
    lastmessage="Please enter a Project Name"
# Prepare test folder.
	else 
# Add JobID to Username to Prepare project name.
    
    echo "Existing files within the $JobID project" 
    cd $HOME/AnsibleWorkings/$JobID
	ls
    lastmessage="- Loaded project: $JobID. -"
    echo "B1- Load Project- $D $lastmessage" >> $WorkDir/logs.txt
#	cd $HOME/AnsibleWorkings/$JobID
	fi

}

InstallAnsiblePIP(){
# Install ansible via PIP
# Install curl
	$SAI curl
# Install python
	$SAI python
# Install PIP
	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	python get-pip.py --user
# Install Ansible
	python -m pip install --user ansible
# Inform user of completion
	ValidateInstall
}

ConfigureAnsible(){
# Create hosts file within a working directory for localhost management.	
	echo -e "[local]
	localhost" | tee $WorkDir/hosts
# Define default settings
	echo -e "[defaults]
	inventory = $WorkDir/hosts" | tee $WorkDir/ansible.cfg 
}	

ListHosts(){
# list a specific file in a specific directory
#	cd $WorkDir	
	ansible -i $WorkDir/hosts.ini --list-hosts all
# List all hosts	
#	ansible --list-hosts all
# /etc/ansible/hosts	
}

ValidateInstall(){
# Validate install by printing version.
    AnsibleVersion=$(ansible --version)
	lastmessage=""$AnsibleVersion

}

ValidateYML(){
# Validate your playbooks YML
	read -rp "Enter playbook to validate or blank for current project: " check_playbook
# Validate input	
	if [[ "$check_playbook" == "" ]];
	then 
	check_playbook="~/$JobID.yml"
	lastmessage=" Results: " $ansyml_check
	else
# check yml with yamllint not using due to conflicting output.
#	yml_check=$(yamllint $WorkDir/$check_playbook)
#  check yml with  ansible-lint.
	ansyml_check=$(ansible-lint $WorkDir/$check_playbook)
# notification message to display on the menu screen
	lastmessage=" Results: " $ansyml_check
	fi
}

RunPlaybook(){
# enter playbook name to execute
	read -rp "Enter the name of the Playbook to run or blank for current project : " run_play
# Assign project folder as playbook name if blank
	if [ "$run_play" == ""];
	then 
	run_play="$JobID.yml"
	else
# execute playbook with very, very verbose output
	ansible-playbook --ask-become-pass playbook.yml -vvv
#	ansible-playbook $WorkDir/$run_play.yml -vvv
	fi
}

CreatePlaybook(){
# enter playbook name to create and execute
	read -rp "Enter your desired Playbook name: " run_play
	new_play=($run_play.yml)
# Open nano to edit new playbook.
	nano $WorkDir$run_play.yml
# validate yml and display errors in new playbook.
	ansyml_check=$(ansible-lint $WorkDir$new_play)
	echo "Ansible YML validation results: " $ansyml_check
# execute playbook
	ansible-playbook --ask-become-pass $WorkDir$new_play -vvv
# notification message to display on the menu screen.	
	lastmessage="Ansible playbook created, validated and executed. " 
}


AutoSSHKey(){
# setup SSH
	printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
}

AddUser2Group(){
# Add a user to a Group:

	read -rp "Enter the name of the target User: " add_user
	read -rp "Enter the name of the target Group: " add_group
	if [[ "$add_user" == "" ]]
	then 
	lastmessage="Enter a name please....."
	else
	sudo usermod --append --groups $add_group $add_user	
# notification message to display on the menu screen
	lastmessage="added $add_user to $add_group "
	fi
}

SSHlogin(){
# Login to container via SSH
	contname	
	sshcontip=$( lxc list $newcon --format csv -c 4 | awk '{ print $1; }')
	ssh ubuntu@$sshcontip 
}

SSHKey2Con(){
# copy SSH key from Host to Container
	contname
	cp $HOME/.ssh/id_rsa.pub authorized_keys
	lxc file push authorized_keys $newcon/$HOME.ssh/
}

pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

contname(){	
	status
#New Container name
	read -rp "Enter the name of the target Container: " newcon
	if [[ "$newcon" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo "$newcon"
	fi
}

status(){
# List available containers	
	lxc list --format csv -c ncs46tpaSP
}

InsBuiEss(){
# Install build essentila for development:	
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "sudo apt install build-essential -y "
}

SetupSSHKeys(){
# generate ssh keys on the control machine. and copy it to other machines.
	printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
	SSHLogin
}

SSHLogin(){
	read -rp "Enter the target name (leave blank to quit): " SSH_Target
# validate data was entered.  
    if [ "$SSH_Target" = "" ]; 
    then 
    lastmessage="Please enter an SSH target"
	else 
# Login with generated key
#	ansible --become-user BECOME_ubuntu
    ssh ubuntu@$SSH_Target mkdir -p .ssh
    cat .ssh/id_rsa.pub | ssh ubuntu@$SSH_Target 'cat >> .ssh/authorized_keys'
    ssh ubuntu@$SSH_Target
	fi
}


MakeHosts(){
# Create virtual hosts
	echo "makehosts"
}	

Last10Logs(){
# Display the last ten log entries.
	echo "--The last ten logs:"
    tail $WorkDir/logs.txt
}


# ================== Main Menu ====================

MainMenu(){
# Clears the screen
	clear
# Define this menus title.	
	MenuTitle=" - Ansible Menu - "
# Describe what this menu does
	Description=" Ansible Config Menu "

# Display the menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "  
    echo " =========================================== "
	echo " -- Current Project: $JobID -- " ; ls
    echo " =========================================== "
	echo "0.  Install Ansible Globally"
	echo "01  Add a user to a group "
	echo "1.  configure SSH Keys "
	echo "2.  Configure Ansible " 
	echo "3.  List Hosts"
	echo "10. New Project"
	echo "11. Load a Project"
	echo "30. Run a playbook "
	echo "31. Validate a playbooks YML"
	echo "32. Create a playbook"
	echo "40. Install Ansible Isolated in PIP "
	echo "50. SSH Login test  "
	echo "51. Create SSH key "
	echo "52. Copy SSH key to a container"
	echo "53. Available containers"
	echo "60. Validate Host Config "
	echo "61  Validate Remote config"
	echo "7.  " 
    echo "80.  Display the last ten log entries."
	echo "99.  Quit			Exit this Menu"
	echo "-----------------------------------"
	echo " $lastmessage $Last10Logs " 
	 
}

read_options(){
# Maps the displayed options to command modules
	local choice
# Inform user how to proceed and capture input.	
	read -p "Enter the desired item number or command: " choice	
# Execute selected command modules	
	case $choice in
    0)   InstallAnsibleGlobal ;;
	01)  AddUser2Group ;;
	1)   SetupSSHKeys ;;
	2)   ConfigureAnsible ;;
	3)   ListHosts ;;
	10)  NewProject ;;
	11)  LoadProject ;;
	30)	 RunPlaybook ;;
	31)  ValidateYML ;;
	32)  CreatePlaybook ;;
	40)  InstallAnsiblePIP ;;
	50)  SSHlogin ;;
	51)  AutoSSHKey ;;
	52)  SSHKey2Con ;;
	53)  status ;;
	60)  ansible -i $WorkDir --connection=local local -m ping ;;
	61)  ansible -i $WorkDir remote -m ping ;;
	7)   ;;
	80)  Last10Logs ;;
    9)   ;;
    10)  ;;
# Quit this menu
    99) clear && echo " See you later $USER! " && exit 0 ;;
# Capture non listed inputs and send to BASH.
	*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

# --------------- Main loop --------------------------
# Continues to iterate through this menu loop. 
while true
do
	show_menus
	read_options
done

}

# =================== Run Commands ===============
# commands to run in this file at start (or nothing will happen).
MainMenu

# https://computingforgeeks.com/deploy-kubernetes-cluster-on-ubuntu-with-kubeadm/