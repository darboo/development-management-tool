#!/bin/bash  

# Install and configure a local OpenStack, Openshift for virtualisation management and education.


# ============ Environment Variables ============= 

# Options to change the printed text colour
STD='\033[0;0;39m'
RED='\033[0;41;30m'

# =================== Modules =====================
# Add your desired programme modules

PrintUsername(){
# Change text colour and prints the current user variable.
	echo -e "${RED} Current user is: ${STD}" $USER
}

DateTime(){
# create a variable which defines and executes the desired command (date).
	DT=$(date)
# Change text colour and display contents of the DT variable.
	echo -e "${RED} Today is: ${STD}" $DT
}

InstallMicrostack(){
# Uses snap to install openstack in a container upon the local machine. https://microstack.run/
	sudo snap install microstack --beta --devmode
# Configure networks and OpenStack databases
	sudo microstack init --auto --control
# Launch a VM to test all is good
	microstack launch cirros --name test
}

InstallOKD(){

# Download the OpenShift Client    
    wget https://mirror.openshift.com/pub/openshift-v4/clients/oc/latest/linux/oc.tar.gz
# Extract into usr/local/bin folder
#    sudo unzip/gzip -d oc.tar.gz usr/local/bin

# Download OpenShift OKD
    oc adm release extract --tools quay.io/openshift/okd:4.5.0-0.okd-2020-07-14-153706-ga
# Extract into usr/local/bin folder



# Create cluster
    openshift-install create cluster


}

# ================== Main Menu ====================

MainMenu(){
# Clears the screen
	clear
# Define this menus title.	
	MenuTitle=" - OKD Menu - "
# Describe what this menu does
	Description=" Setup and configure OpenShift "

# Display the menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "  
	echo "-----------------------------------"
	echo "0.  "
	echo "1.  Setup Microstack "
	echo "2.  Setup OKD on Microstack"
	echo "3.  "
	echo "4.  "
	echo "5.  "
	echo "6.  "
	echo "7.  " 
    echo "8.  "
    echo "9.  "
	echo "10. "
	echo "99.  Quit				   Exit this Menu"
	echo " "
	echo " $lastmessage " 
	echo " " 
}

read_options(){
# Maps the displayed options to command modules	
	local choice
# Inform user how to proceed and capture input.	
	read -p "Enter the desired item number or command: " choice
# Execute selected command modules	
	case $choice in
        0)   ;;
		1)   InstallMicrostack ;;
		2)   InstallOKD ;;
		3)   ;;
		4)   ;;
		5)   ;;
		6)   ;;
		7)   ;;
		8)   ;;
        9)   ;;
        10)  ;;
# Quit this menu
        99) clear && echo " See you later $USER! " && exit 0;;
# Capture non listed inputs and send to BASH.
		*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

# =================== Run Commands ===============
# commands to run in this file at start.
MainMenu

