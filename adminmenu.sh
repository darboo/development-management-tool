#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;0;31m'
CYAN='\033[0;0;36m'
BLUE='\033[0;0;34m'
ORANGE='\033[0;0;33m'
GREEN='\033[0;0;32m'
MAGENTA='\033[0;0;35m'
STD='\033[0;0;39m'
HighlightRED='\033[0;41;30m'
DateTime=$(date)
SSI="sudo snap install -y "
PubKey=$( cat ~/.ssh/id_rsa.pub )
CurrentUsersID=$(id)
UsersAndGroups=$(cat /etc/{subuid,subgid})
LocalIP=$(ip addr show | grep "inet " | grep -v "127.0.0.1" | grep -v "virbr0" | awk '{ print $2 }')
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}  
REPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"
clear
SAI="sudo apt install -y "
SSI="sudo snap install "

# 1 -------------------- Admin Menu --------------------

CaptureUser(){
# capture desired user
	read -p " Please provide the user name: " UserName
}

CreateUser(){
# Setup new User
  sudo adduser $UserName
}

Add2Sudogroup(){
# Add user to sudoers group as Root
	sudo /sbin/usermod -aG sudo $UserName
}

setupansiblePIP(){
    SWupdate
    SWupgrade
    $SAI software-properties-common
    $SAI python-pip
    pip install ansible
    SWautoremove
    cp -R /etc/ansible /home/$USER/ansible
	ssh-keygen # create a key to login to containers securely
#    cd /home/$USER/ansible
	AnsibleVersion ansible --version
    lastmessage="- installed $AnsibleVersion -" 
}

MenuSetup(){
# Install the required software for this menu 
	SWupdate
	$SAI openssh-server  nano xclip software-properties-common mc tasksel tasksel-data
	InstallBootRepair
    touch ~/development-management-tool/ops0.txt
	lastmessage="Installed: openssh-server nano xclip software-properties-common 
	Boot-Repair mc & tasksel."
}

setupansible(){
 sudo apt-add-repository --yes --update ppa:ansible/ansible
 sudo apt update
 $SAI ansible
}

InstallBootRepair(){
	sudo add-apt-repository ppa:yannubuntu/boot-repair
	SWupdate
	$SAI boot-repair 
}

# Update 
SWupdate(){
	sudo apt update
	lastmessage=" OS Updated "
}
# upgrade 
SWupgrade(){
    sudo apt upgrade -y
    lastmessage=" OS Upgraded " 
}
# autoremove
SWautoremove(){
    sudo apt autoremove -y
    lastmessage=" Autoremoved unecassary software "
}

# autoclean
SWautoclean(){
    sudo apt autoremove -y
    lastmessage=" Autocleaned OS"
}

# fix Broken Packages
dpkgfix(){
	sudo dpkg --configure -a
	sudo apt --fix-broken install
	lastmessage=" Package Manager Repaired "
}

SetupSSHServer(){
	sudo apt install openssh-server
	sudo systemctl enable ssh
	sudo ufw allow ssh
	sudo systemctl status ssh
}

# SSH Setup
setupssh(){
	echo "Create SSH key and copy to target machine"
	ssh-keygen
	read -p " Provide USER@SERVER details for target SSH machine" sshtarget
	ssh-copy-id $sshtarget
}

# SSH Login
loginssh(){
	read -p " Provide USER@SERVER details for target SSH machine" namessh
	ssh $namessh
# Copy key to clipboard
	cat ~/.ssh/id_rsa.pub | xclip -sel clip	
	lastmessage=" Logged into $namessh "
}

BootFix(){
# Run boot-repair (if installed)
	boot-repair
}

runtasksel(){
	sudo tasksel
}

UbuntuSetup(){
# Install desired apllications for a remote development environment
# WebBrowser
	$SSI brave
# Terminal
    $SAI xterm
# File Browser
    $SAI nemo
# IDE
	$SSI codium --classic
# 2nd sshdaemon on port 1022- open port on firewall: iptables -I INPUT -p tcp --dport 1022 -j ACCEPT
}

ListGroups(){
	less /etc/group
	groups $USER
}

ViewUserID(){
# view user id, subuid and subgid
	id
	lastmessage=$(cat /etc/{subuid,subgid})
}

ViewSources(){
# View sources for M16
	cat /etc/apt/sources.list.d/armbian.list
}

AddSources(){
# Add the below list to the armbian.list
	echo "
deb http://ports.ubuntu.com/ xenial main restricted universe multiverse
#deb-src http://ports.ubuntu.com/ xenial main restricted universe multiverse

deb http://ports.ubuntu.com/ xenial-security main restricted universe multiverse
#deb-src http://ports.ubuntu.com/ xenial-security main restricted universe multiverse

deb http://ports.ubuntu.com/ xenial-updates main restricted universe multiverse
#deb-src http://ports.ubuntu.com/ xenial-updates main restricted universe multiverse

deb http://ports.ubuntu.com/ xenial-backports main restricted universe multiverse
#deb-src http://ports.ubuntu.com/ xenial-backports main restricted universe multiverse
	" >> /etc/apt/sources.list.d/armbian.list
}

SetupTensorflowGPU1(){
	sudo apt update
	sudo apt install python3-dev python3-pip
	curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o Miniconda3-latest-Linux-x86_64.sh
	bash Miniconda3-latest-Linux-x86_64.sh
# Restart Shell
}

SetupTensorflowGPU2(){
	conda -V
	conda update -n base -c defaults conda
	conda create --name tensorflow python=3.9
	conda activate tensorflow
# CPU Version
#	pip install tensorflow-cpu
#	pip install tensorflow-cpu==package_version

# GPU Version
	conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0
	mkdir -p $CONDA_PREFIX/etc/conda/activate.d
	echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/' > $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
	pip install --upgrade pip
	pip install tensorflow
}

SetupTensorflowGPU3(){
# Test - The output prints a Tensor with random values, indicating the installation worked.
	python -c "import tensorflow as tf; print(tf.random.normal([10,10]))"

# Source: https://phoenixnap.com/kb/how-to-install-tensorflow-ubuntu

}

GitBranch+IP(){
# Display the current branch and host's IP address.
	cd ~/development-management-tool/
	BranchName=$(git branch)
	cd ..
	echo -e "${STD}	Git Branch = ${BLUE}$BranchName${STD} & local IP's = ${CYAN}$LocalIP${STD}"
}

adminmenu(){

MenuTitle=" Admin Menu "
Description=" System administration and management tools "
GitBranch+IP
    admin_menu(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo ""
	echo "1.  Install SSH, Boot-Repair, nano, xclip, etc."
	echo "2.  Install Ansible "
	echo "3.  Setup OpenSSH Server"
	echo "4.  Update the Host machine "
	echo "5.  Upgrade the Host machine "
	echo "6.  Autoremove "
    echo "7.  Autoclean "
    echo -e "${GREEN}8.  Install VSCodium, XTERM, & Brave. ${STD}"
	echo "9.  Fix Broken Packages "
	echo "10. SSH Setup"
	echo "11. SSH Login"
	echo "12. Boot Repair "
	echo "13. Display all groups and those this user is atached to."
	echo "14. Add a user to sudo group (as Root User) "
	echo "15. view user ID, subuid & subgid info."
	echo "20. Create a new User"
	echo "25. Install Tensorflow GPU with Miniconda"
	echo "26. Continue Tensorflow install after restarting shell."
	echo "27. Test Tensorflow Install."
#	echo "30. View Source List "
#	echo "31. Add backports to M16 source list "
	echo -e "${GREEN}40. File Browser - Midnight Comander ${STD}"
	echo "42. Additional Software Installer - TaskSel "
    echo "80. Reboot Server "
	echo "81. Shutdown Server "
	echo "99. Return to Main Menu " 
    echo "------------------------"
    echo " $lastmessage"
	echo "------------------------"

}
    admin_options(){
	local choice
	read -p "Enter choice or BaSH command: " choice
	case $choice in
		1) MenuSetup ;;
		2) setupansible ;;
		3) SetupSSHServer ;;
		4) SWupdate ;;
		5) SWupgrade ;;
		6) SWautoremove ;;
        7) SWautoclean ;;
        8) UbuntuSetup ;;
		9) dpkgfix ;;
		10) setupssh ;;
		11) loginssh ;;
		12) BootFix ;;
		13) ListGroups ;;
		14) CaptureUser && Add2Sudogroup ;;
		15) ViewUserID ;;
		16) Add2Path ;;
		20) CaptureUser && CreateUser ;;
		25) SetupTensorflowGPU1 ;;
		26) SetupTensorflowGPU2 ;;
		27) SetupTensorflowGPU3 ;;
#		30) ViewSources ;;
#		31) AddSources ;;
		40) xterm mc & disown ;;
		42) sudo tasksel ;;
		80) sudo reboot ;;
		81) sudo shutdown now ;;
		99) exit ;;
		*) echo -e "${RED} $choice is not a displayed option, trying /bin/bash.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

while true
do
	admin_menu
	admin_options
done
}   

# required to start the menu
adminmenu

