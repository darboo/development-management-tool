#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

# Set git push default to stop that message appering
	git config --global push.default simple
	clear
	cd ~/development-management-tool

# 3 -------------------- GIT Commands -------------------------


file2git(){
	read -rp "Enter the name of the file or,' . 'for all files: " gitfile 
}
all2git(){
	gitfile="."
}
gitpull(){
	git pull
}
gitadd(){
	git add $gitfile
}
gitcommit(){
	git commit 
}
gitpush(){
	git push 
}

GitPushSSH(){
# push via SSH
	git push git@bitbucket.org:darboo/development-management-tool.git


}

WorkingGitFolder(){
# Change folder to a working git repository
	read -rp "Enter the path to desired Git folder: " GitFolder 
	# validate data was entered.  
    if [ "$GitFolder" = "" ]; 
    then 
    lastmessage="Please enter some data"
	clear
    gitmenu
# Prepare test folder.
	else 
	cd $GitFolder
	lastmessage="changed current directory to: $GitFolder" 
	fi

ChangeBranch(){
# Pull a different branch
# Capture branch name
	read -rp "Enter the full branch name (eg; origin/editing) " ThisBranch
# check out desired branch
	git checkout -t $ThisBranch

}

}
gitUserConfig(){
	read -rp "Enter your email: " gitmail
	git config $gitglo user.email "$gitmail"
	read -rp "Enter your username: " gituser
	git config $gitglo user.name "$gituser"
}
newgit(){
	read -rp "Enter the name of your new repository " GitRepo
	read -rp "Enter the path for your new repository or leave empty to place it in your Documents " NewDir
	if [ -z "$NewDir" ]; then DocsFolder;
	else 
	echo " Creating in $NewDir "
	fi
	PathFile=$NewDir/$GitRepo
	echo $PathFile
	git init $PathFile
}
DocsFolder(){
	NewDir=/home/$USER/Documents
}

#----------------------------------------------------------
gitmenu(){
git_menu(){
	$CurrentBranch
	echo "" 
	echo " GIT - MENU"
	echo " Common GIT commands in a simple menu. "
    echo "------------------------"
	echo "1.  Update the repository within the current folder "
	echo "2.  Add a file to commit within the repository. "
	echo "3.  Commit changes "
	echo "34. "
	echo "4.  Push commited changes to a remote repository "
	echo "5.  Add All Files & Commit "
	echo "6.  Add All Files, Commit & Push "
	echo "7.  User Configuration.                       Identify yourself to Git "
	echo "8.  Create and intialise a new repository in a location of your choice " 
	echo "9.  Change to another branch "
	echo "10. Commit Message"
	echo "20  Change Working Git Folder"	
	echo "21"
	echo "99. Exit " 
echo "------------------------"
echo " $lastmessage "
echo "------------------------"

}

git_options(){
	local choice
	read -p "Enter choice or command: " choice
	case $choice in
		1) gitpull ;;
		2) file2git && gitadd ;;
		3) gitcommit ;;
		4) gitpush ;;
		5) all2git && gitadd && gitcommit ;;
		6) all2git && gitadd && gitcommit && gitpush ;;
		7) clear && gitUserConfig ;;
		8) newgit ;;
		9) ChangeBranch ;;
		10) ;;
		20) WorkingGitFolder ;;
		21) ;;
		99) clear && echo "Goodbye $USER" && exit ;;
		*) echo -e "${RED} $choice is not a displayed option, trying /bin/bash.....${STD}" echo -e $choice | /bin/bash
	esac
}

while true
do
	git_menu
	git_options
done
}

gitmenu
