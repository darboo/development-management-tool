#!/bin/bash 

# Dev tools for rails configuration menu.
# Designed for container usage (Docker) hosted on OL7.

# ---------------------- Arrays & Variables ---------------------
  RED='\033[0;41;30m' #Red background
  STD='\033[0;0;39m'
  T=$(date +"%T")
  D=$(date +"%F")
  ProjectFolder="$HOME/RailsProjects"

  # Set git push default to stop that message appering
  git config --global push.default simple

  DataTypes=("binary" "boolean" "date" "datetime" "decimal" "float" "integer" "string" "text" "time" "timestamp")

# --------------------- Install and setup ---------------------

InstallTools(){
# Tasks which must be perfomed as root within desired container. 
  apt update
  apt upgrade -y
  apt autoremove -y
# instal required dev tools
  sudo apt install -y curl bash-completion nano make g++ ruby-dev
# Create projects folder
  mkdir -v $ProjectFolder
  cd $ProjectFolder
}

Soft2Install(){
# Install some software of choice
# Capture desired software
  read -rp "add list of desired software" Software2Get
  apt update
  apt install $Software2Get -y
  apt autoremove -y
}

RoRInstall(){
# https://gorails.com/setup/ubuntu/24.04

sudo apt-get update
sudo apt install -y build-essential rustc libssl-dev libyaml-dev zlib1g-dev libgmp-dev pkg-config

# Install Ruby using a version manager called Mise. This allows you to easily update Ruby and switch between versions anytime.
curl https://mise.run | sh
echo 'eval "$(~/.local/bin/mise activate)"' >> ~/.bashrc
source ~/.bashrc
mise doctor
# need to exit shell to initialize mise
# Then install Ruby with Mise:
mise use --global ruby@3
ruby --version

# Ensure you're using the latest version of Rubygems.
gem update --system

gem install rails -v 8.0.0
rails -v
}

MakeLicenceManager(){
# 
#  rails generate scaffold SoftwareLicenceManagement status:string contract_lead:string supplier:string contract_description:text contract_start_date:date contract_expiry_date:date po_expiry_date:date contract_duration_months:integer contract_value:decimal annual_contract_value:decimal forecast_next_year:decimal service_or_project_owner:string business_area:string service_strategy:text contract_strategy:text renewed:boolean current_po_number:text cost_code:text cds_costcode:boolean cost_code_description:text procurement_route:text supplier_contact:text service_cancellation_date:date procurement_right_to_extend:boolean notice_period_days:integer paper_copy:boolean invoice_frequency_months:integer current_contract_attached:text contract_licence_type:text licence_details:text licence_quantity:integer licence_key:string info_to_snow:date date_item_added_to_list:date expiry_alert_date:date contract_review_date:date dsl:boolean item_type:string path:string licence_purpose:string propriety_licence_type:string open_source_licence_type:string high_value:boolean alternative_solution:text target_dept:string user_satisfaction:string required_features:text perceived_licence_limitations:text
 rails generate scaffold Contract \
  status:string \
  contract_lead:string \
  supplier:string \
  contract_description:text \
  contract_start_date:date \
  contract_expiry_date:date \
  po_expiry_date:date \
  contract_duration_months:integer \
  contract_value:decimal \
  annual_contract_value:decimal \
  forecast_next_year:decimal \
  service_or_project_owner:string \
  business_area:string \
  service_strategy:text \
  contract_strategy:text \
  renewed:boolean \
  current_po_number:text \
  cost_code:text \
  cds_costcode:boolean \
  cost_code_description:text \
  procurement_route:text \
  supplier_contact:text \
  service_cancellation_date:date \
  procurement_right_to_extend:boolean \
  notice_period_days:integer \
  paper_copy:boolean \
  invoice_frequency_months:integer \
  current_contract_attached:boolean \
  contract_licence_type:text \
  contract_licence_details:text \
  licence_quantity_seats:integer \
  licence_key:string \
  date_item_added_to_list:date \
  expiry_alert_date:date \
  contract_review_date:date \
  dsl:boolean \
  contract_url:string \
  licence_purpose:string \
  propriety_licence_type:string \
  open_source_licence_type:string \
  high_value:boolean \
  alternative_solution:text \
  target_dept:string \
  user_satisfaction:string \
  required_features:text \
  perceived_licence_limitations:text
 rails db:migrate
}

WinSystenInfo(){
# Create a model for capturing windows computer hardware specifications. 
rails generate scaffold SystemInfo \
  host_name:string \
  os_name:string \
  os_version:string \
  os_manufacturer:string \
  os_configuration:string \
  os_build_type:string \
  registered_owner:string \
  registered_organization:string \
  product_id:string \
  original_install_date:datetime \
  system_boot_time:datetime \
  system_manufacturer:string \
  system_model:string \
  system_type:string \
  bios_version:string \
  windows_directory:string \
  system_directory:string \
  boot_device:string \
  system_locale:string \
  input_locale:string \
  time_zone:string \
  total_physical_memory:integer \
  available_physical_memory:integer \
  virtual_memory_max_size:integer \
  virtual_memory_available:integer \
  virtual_memory_in_use:integer \
  page_file_location:string \
  domain:string \
  logon_server:string \
  has_many:network_cards \
  processor:belongs_to \
  hotfixes:has_many

rails generate scaffold NetworkCard \
  system_info:references \
  name:string \
  dhcp_enabled:boolean \
  dhcp_server:string \
  ip_addresses:text

rails generate scaffold Hotfix \
  system_info:references \
  kb_number:string

rails generate scaffold Processor \
  system_info:references \
  manufacturer:string \
  model:string \
  speed:string \
  cores:integer 
  
rails db:migrate 
}

CreateViewsFromControllers(){
# Make controller and add a data output variable to views.
  rm-rf $OutputFiles/Controller.txt
  rm -rf $OutputFiles/Views.txt
  OutputFiles="$ProjectFolder/$ProjectName"
  RunSoap="/soapuiprojectdata/SoapcmdUI.sh"
  ModulesList=( AddProxy ) # CloneInputRepo PullInput CloneOutputRepo PushOutputRepo ToolUpdate ViewFolder )
  for i in "${ModulesList[@]}"
  do
  echo  "  @$i = BT $RunSoap $i BT " | sed 's#BT#`#' | >> $OutputFiles/Controller.txt
  echo  "  <p> <%= @$i %> </p> " >> $OutputFiles/Views.txt
  done
  ls $OutputFiles/*.txt
  more $OutputFiles/Controller.txt
  more  $OutputFiles/Views.txt
}

FileUpload(){
# create rails project to upload files required for SoapUI API run application for each tv01, ufast01, 02, 03 BPREP & PROD etc. in parrallel.
# Source: https://www.microverse.org/blog/how-to-build-an-image-upload-feature-with-rails-and-active-storage
    ProjectName="testfile"
# create new project
    rails new $ProjectFolder/$ProjectName
    cd $ProjectFolder/$ProjectName
# Setup active storage

    rails active_storage:install
# migrate to update db
    rails db:migrate
# Edit storage configuration.
    nano $ProjectFolder/$ProjectName/config/storage.yml
#
}

# https://www.timdisab.com/uploading-files-in-ruby-on-rails-with-active-storage
 
# ---------------------- Project Creation ---------------------

# Create a new rails project & git init
 

NameProject(){
# Create a new rails project
    read -rp "Name of this project: " ProjectName
# Ensure a name was entered or stop.
    if [ $ProjectName=="" ]
    then
    echo "A name is rquired"
    else echo $ProjectName
    fi
}

ProjectSelect(){
# Display a list of projects  
  ls $ProjectFolder/
  read -rp "Choose a project " ProjectName
  if [ $ProjectName == "" ]
  then
  ProjectName="alpha"
  echo "No project selected, using Dev Folder:" $ProjectName
  else
  echo "Using selected folder: " $ProjectName
  fi
  cd $ProjectFolder/$ProjectName
  }

NewRorApp(){
    NameProject
    mkdir -v $ProjectFolder/$ProjectName
    cd $ProjectFolder/$ProjectName
    rails new .
}

GitInit(){
# Initialize Git within the created folder
    git init $ProjectFolder/$ProjectName
}

ValidateProjectCreation(){
#  confirm the existance of ~/projects/$ProjectName and then initialize git.
if [ -f "$ProjectFolder/$ProjectName" ];
then
cd $ProjectFolder/$ProjectName
lastmessage="$ProjectName is ready.";
else
lastmessage="That did not work, check above for clues."
fi
}

# ---------- Options to manage the server --------------

RailsUp(){
  #Start the server and configure for localhost:3000. 
    rails s -b 0.0.0.0 #-d
}

RailsUpBackground(){
  #Start the server in the background and configure for localhost:3000. 
    rails s -b 0.0.0.0 -d
    ProcessID=$(cat $ProjectFolder/$ProjectName/tmp/pids/server.pid)
    echo "PID for" $ProjectName "is " $ProcessID
    ps aux
}

StopRailsBackground(){
# view running process which include the word spring
    ps aux
    KillAppPID=$( ps aux | grep "bin/rails s -b 0.0.0.0 -d" | awk '{ print $2 }' )
    PID2Kill=$( ps aux | grep "spring server | $ProjectName" | awk '{ print $2 }' )
    PumaPID2Kill=$( ps aux | grep "puma " | awk '{ print $2 }' )
    echo "spring server | $ProjectName is PID:" $PID2Kill
    echo "puma PID:" $PumaPID2Kill
    kill $PumaPID2Kill
    kill $PID2Kill
    kill $KillAppPID
    ps aux
} 

ListPIDs(){
   ps aux | grep "bin/rails s -b 0.0.0.0 -d"
   ps aux | grep "spring server | $ProjectName"
   ps aux | grep "puma "
}

# ------------- Creation & Editing --------------------

CreatePage(){
# Creates a new page with a controller to host app module with input/output.
# Capture Page name:
  read -rp "Please enter an name for this page: " Newpage
  if [ $Newpage == ""]
  then echo "Cannot proceed without a name...."
  else rails g controller $ProjectName $Newpage
  fi
  lastmessage="Created $Newpage in $ProjectName"
}

ShowControllers(){
# Display controllers to edit
  rake routes
  ls $ProjectFolder/$ProjectName/app/controllers/*.rb
}

EditController(){
  read -rp "Choose controller to edit: " EditThisController
  if [ $EditThisController == "" ]
  then echo "Need a controller name... "
  else nano $ProjectFolder/$ProjectName/app/controllers/$EditThisController
  fi
}

SetupHomePage(){
# Creates a page which will contain links to the various resource index pages.
    rails g controller $ProjectName index
# Set created page as root (index)
    sed -i "3s/.*/root '$ProjectName#index'/" $ProjectFolder/$ProjectName/config/routes.rb
# Display created files
    more $ProjectFolder/$ProjectName/config/routes.rb
}

EditModel(){
# show views in project folder
  ls $ProjectFolder/$ProjectName/app/models/*.rb
# Open created file for editing.
  read -rp "Choose model to edit: " EditThisView
  if [ $EditThisView == "" ]
  then echo "Need a file name... "
  else nano $ProjectFolder/$ProjectName/app/models/$EditThisView
  fi
}

EditPostsViewPages(){
# show views in project folder
  ls $ProjectFolder/$ProjectName/app/views/posts/
# Open created file for editing.
  read -rp "Choose view to edit: " EditThisView
  if [ $EditThisView == "" ]
  then echo "Need a file name... "
  else nano $ProjectFolder/$ProjectName/app/views/posts/$EditThisView
  fi
}
 
EditRoutes(){
# manually add a GET or Resource to a controller module.
  nano  $ProjectFolder/$ProjectName/config/routes.rb
}

EditFile(){
# Edit a selected file
# Display a list of files in project folder
  ls $ProjectFolder/$ProjectName/
# Capture file to edit.
  read -rp "Select a file to edit: " File2Edit
# Open file in nano.
  nano $ProjectFolder/$ProjectName/$File2Edit
}

EditHomePage(){
# Open created file for editing.
    nano $ProjectFolder/$ProjectName/app/views/rewq/index.html.erb
}

FormMaker(){
# Create Scenario Task Action Result capture form
  rails g scaffold WorkRequest Work_request_id:integer 
# Update datbase
  rake db:migrate
} 

NewTable(){
# Create a data capture page mapped to the internal database for manipulation.
  cd $ProjectFolder/$ProjectName/
#create scaffolding for Database object CRUD:
  read -rp "Name for this new page/DB table: " Table_Name
  read -rp "Name for the new input field: " Field_Name
# Display available data data types

  printf '%s\n' "${DataTypes[@]}"
  read -rp "Select Data Type: " Data_Type
# Display data to generate
  echo "" $Table_Name $Field_Name:$Data_Type
# Execute captured data 
  rails g scaffold $Table_Name $Field_Name:$Data_Type
# update db 
  rails db:migrate
  #MoreTables
} 

NewController(){
# Create a controller for a
    read -rp "Name for this new DB table: " Table_Name
    rails generate controller $Table_Name new
}

NewModel(){
# Create a new field for existing db table/page
    ProjectSelect
    read -rp "Name for this new field: " Field_Name
    printf '%s\n' "${DataTypes[@]}"
    read -rp "Data Type: " Data_Type
    rails generate model $Table_Name $Field_Name:$Data_Type
    rake db:migrate
}

MoreTables(){
# Add more tables
  read -rp "Add another field to this table? Y/N" More_Fields
} 

NewColumn(){
# Add a new column:datatype to an existing field:
    read -rp "Add a field to which table? " ThisTable
    read -rp "name this new field: " ThisItem
    read -rp "Data Type: " Data_Type
    rails generate migration Add$ThisItem To $ThisTable
    $ThisItem:$Data_Type
    rails db:migrate
}

# ------------------- Removal ---------------------
#Delete a scaffold and database entry:fibre_delivery_activity
DelTable(){
  ProjectSelect
  read -rp "Name of DB table to Delete/Drop: " Table_Name
  rails destroy $Table_Name
  rails db:migrate
  rails db:drop:$Table_Name
  rails db:migrate
}


DelScaffold(){
  read -rp "Name of the Scaffold to remove: " Scaff_Name
  rails destroy $Scaff_Name
  rails db:migrate
}

DeleteProject(){
    ProjectSelect
    rm -rf $ProjectFolder/$ProjectName
}

# 3 -------------------- GIT Commands -------------------------

gitUserConfig(){
read -rp "Enter your email: " gitmail
git config user.email "$gitmail"
read -rp "Enter your username: " gituser
git config user.name "$gituser"
}

TryATing(){
    NewRorApp
    GitInit
    #ProjectSelect
    SetupHomePage
    RailsUp
}

UpdateSoftware(){
  cd ~/railsdev
  git add .
  git stash
  git pull
  echo " Exited to apply update.. " && exit 0
}

# ----------------- Display menu options -----------------

MainMenu(){
# Create ROR apps.
    MenuTitle="Ruby on Rails Dev Tool"
    Description="Manage & develop RoR Projects from a BASH menu driven interface"
show_menus() {
echo " "
echo " $MenuTitle "
echo " $Description "
echo -e "Current Project: ${RED} $ProjectName ${STD}"
echo "-----------------------------------"
echo "0.  Tool Setup."
echo "1.  Install Ruby3 and latest Rails."
echo "2.  Create Licence Manager Model "
echo "3.  Create Sysinfo Model "
echo "4.  Bundle install (no admin) "
echo "5.  Install desired software"
echo "6.  Things in the background.. "
echo "8.  Create a capture form from a file"
echo "10. Install RoR tools. "
echo "11. New                  Create a new RoR project " #& GIT init "
echo "12. Select Project"
echo "13. Start Server         Start the Rails server.  "
echo "14. Start server in the background. "
echo "15. Create a home/index page. "
echo "16. Edit the new home page. "
echo "17. Stop a background server. "
echo "18. Select a project file to edit."
echo "19. Edit routes file."
echo "20. Create resource, page and Database table for interaction (CRUD). "
echo "21. Add a data field to an existing page (new column)."
echo "22. New Model"
echo "23. Create a new page within the current project."
echo "24. View existing Controllers"
echo "25. Edit Controller"
echo "26. select and edit a model."
echo "27. Edit POST View files."
echo "28. Create SoapUI interface components"
echo "31. File upload. "

echo "70. Delete a page "
echo "72. Delete Scaffold "
echo "75. Delete Project"

echo "90. Trying a Thing"
echo "95. "
echo "99. Quit "
echo " "
echo " $lastmessage "
echo "Existing Projects: " && ls $ProjectFolder
ListPIDs
}

read_options(){
local choice
read -p "Enter the desired item number: " choice

case $choice in
  0)  InstallTools ;;
  1)  RoRInstall ;; 
  2)  MakeLicenceManager ;;
  3)  WinSystenInfo ;;
  4)  bundle config set --local path 'vendor/bundle' && bundle install ;;
  5)  Soft2Install ;;
  6)  ps aux | grep 'spring' ;;
  8)  FormMaker ;;
  10)  InstallTools ;;

  11)  NewRorApp && GitInit ;;
  12)  ProjectSelect ;;

  13)  RailsUp ;;
  14)  RailsUpBackground ;;

  15)  SetupHomePage ;;
  16)  EditHomePage ;;

  17)  StopRailsBackground ;;
  
  18)  EditFile ;;
  19)  EditRoutes ;;
  20)  NewTable ;;
  21)  NewColumn ;;
  22)  NewModel ;;
  23)  CreatePage ;;
  24)  ShowControllers ;;
  25)  ShowControllers && EditController ;;
  26)  EditModel ;;
  27)  EditPostsViewPages ;;
  28)  CreateViewsFromControllers ;;
  
  31)  FileUpload ;;
  70) DelTable ;;
  72) DelScaffold ;;
  75) DeleteProject ;;

  90) TryATing ;;
  95)  ;;
  99) clear && echo " Goodbye $USER! " && exit 0;;
  *) echo -e "${RED} sending $choice to bin/bash.....${STD}" && echo -e $choice | /bin/bash
esac
}

# --------------- Main loop --------------------------

while true
do
show_menus
read_options
done
}

    if [ $1 == ""]
    then RunModule="MainMenu"
    else RunModule=$(echo "$1 $2")
    fi
    $RunModule

# This is the selector, used to run through modules

#MainMenu
#ProjectList
#NameProject
#NewRorApp
#ValidateProjectCreation
#ProjectSelect
#GitInit
#TryATing
#SetupHomePage
#RailsUp
