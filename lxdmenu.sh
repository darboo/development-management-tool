#!/bin/bash


# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
CYAN='\033[0;0;36m'
BLUE='\033[0;0;34m'
ORANGE='\033[0;0;33m'
GREEN='\033[0;0;32m'
MAGENTA='\033[0;0;35m'
STD='\033[0;0;39m'
DateTime=$(date)
REPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"
clear
SAI="sudo apt install -y "

# -------------------- LXD Menu ---------------------

# 2a ********** install LXD and its requirements *****************

InstallCheck(){
 #   dirhere="/home/$USER/demato/lxd0.txt"
if [ -f "/home/$USER/development-management-tool/lxd0.txt" ]
	then 
	lastmessage="Proceed $USER"
	LXDmenu
	else 
	lastmessage=$(echo -e "${RED}LXD not present, please run LXD install!${STD}")
	#  LXDsetup ;
	LXDmenu
	fi
}
LXDsetup(){
# Update, upgrade and autoremove
	sudo apt update
    sudo apt upgrade -y
	sudo apt autoremove -y
# Install lxd:
	$SAI lxd-client
# Incase you are upgrading from apt to snap 	
	sudo ln -s /var/snap/lxd/common/lxd /var/lib/lxd

# Required Software	
	$SAI build-essential
#	Install terminal konsole
	$SAI konsole
#	Browse servers with SSH using Nemo
	$SAI nemo
	$SAI zfsutils-linux
# install additional LXC tools
	$SAI lxc-utils
	$SAI criu
	$SAI lxd-tools
	$SAI libpam-cgfs
	sudo snap install -y opera
	$SAI software-properties-common

# Set git push default to stop that message
  git config --global push.default simple
# Initialise LXD (must make a config file for this......) 
	echo "**"
	echo "Starting the interactive configuration,"
        echo "please select the default options:"
	echo "**"
	sudo lxd init 
# Add the current user to the LXD Group:
	sudo usermod --append --groups lxd $USER
# Create AV passthrough, but it dont work!
#	GUIProfile
	
# notification message to display on the menu screen
	lastmessage="---LXD/LXC, and other things installed, 
	please RESTART YOUR MACHINE before you create containers.---"
	
	defaultLXDconfig="config: {}/home/ubuntu/.ssh/id_rsa.pub
networks:
- config:
    ipv4.address: auto
    ipv6.address: none
  description: ""
  managed: false
  name: lxdbr0
  type: ""
storage_pools:
- config:
    size: 17GB
  description: ""
  name: default
  driver: zfs
profiles:
- config: {}
  description: ""
  devices:
    eth0:
      name: eth0
      nictype: bridged
      parent: lxdbr0
      type: nicbash_profile or .
    root:
      path: /
      pool: default
      type: disk
  name: default
cluster: null
"
# Creates a file after install
    touch /home/$USER/development-management-tool/lxd0.txt

# Creates a mapping for the user ID of the host to the container (for non snap installs).
   echo "root:$UID:1" | sudo tee -a /etc/subuid /etc/subgid
}

GUIProfile(){
# Creates a profile for AV Passthrough.
    lxc profile create gui
# Copy the new profile data into the gui profile.
    cat /home/$USER/development-management-tool/lxdguiprofile.txt | lxd.lxc profile edit gui
# display the newly created profile.
    lxc profile list

}

# ---------- Share folder on host 2 container -----------

# Source: https://www.cyberciti.biz/faq/how-to-add-or-mount-directory-in-lxd-linux-container/
ShareHostFolder(){
# share a folder on the host
	lxc config device add $newcon $NewDeviceName disk source=$HOME/$HostFolder2Share path=/$MappedFolder2Share
}
# contname && DeviceName && HostFolder && ContainerPath && ShareHostFolder

HostFolder(){
# New Container name
	read -rp "Enter the name of the Host folder to share: " HostFolder2Share
	if [[ "$HostFolder2Share" == "" ]]
	then 
	echo "Enter a name Host folder to share please....."
	else
	echo "$HostFolder2Share"
	fi
}

ContainerPath(){
# New Container name
	read -rp "Enter the name of the new shared container folder : " MappedFolder2Share
	if [[ "$MappedFolder2Share" == "" ]]
	then 
	echo "Enter a  shared container folder device name please....."
	else
	echo "$MappedFolder2Share"
	fi
}

ShowDevices(){
# show devices attached to this container.
	lxc config device show $newcon
}

RemoveDevice(){
# Remove an unwanted device
	lxc config device remove $newcon $NewDeviceName
}

contname(){
# New Container name
	read -rp "Enter the name of the target Container: " newcon
	if [[ "$newcon" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo "$newcon"
	fi
}

DeviceName(){
# New Container name
	read -rp "Enter the name of the device: " NewDeviceName
	if [[ "$NewDeviceName" == "" ]]
	then 
	echo "Enter a device name please....."
	else
	echo "$NewDeviceName"
	fi
}


# 2b ********************* Creating Containers *******************


# Ephemeral container
Ephemcont(){
	read -rp "The name of this Ephemeral (temporary) Container is: " newcon
	lxd.lxc launch -e ubuntu:18.04 $newcon
	contip=$( lxd.lxc list $newcon --format csv -c 4 ) 
	wait4ip
	updupgre
	lastmessage=" The Epheremal container $newcon be ready ."
}

makecon(){
# Create a basic Ubuntu22.04 container	
#	contname
	lxc launch ubuntu:22.04 $newcon
#	wait4ip
#	updupgre
#	lxd.lxc exec $newcon -- sudo --login --user ubuntu sh -c "git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"
	lastmessage="The basic $newcon container is now ready for use. "
	
}

MakeCon(){
# Create a basic Ubuntu 20.04 container	
	contname
	lxc launch ubuntu:20.04 $newcon
	wait4ip
#	updupgre
#	lxd.lxc exec $newcon -- sudo --login --user ubuntu sh -c "git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"
	lastmessage="The basic $newcon container is now ready for use. "
}

# =========== Prep Application Server ============

MakeConAppServer(){
# Create a basic Ubuntu18.04 container	
	contname
	lxc launch ubuntu:20.04 $newcon
	wait4ip
#	updupgre
	SSHKey2Con
# Run the following in a new terminal
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "sudo apt update && sudo apt install -y xrdp firefox xterm nemo kate firefox-geckodriver firefoxdriver"
# Reboot container
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "sudo reboot"
# Give the container 4 seconds to reboot before continuing	
	sleep 4
# wait4ip?

# SSH login with X to the new server
	SSHKonsoleXLogin 
	lastmessage="The App-Server $newcon container is now ready for use. "
}

# display firefox from remote
# - ssh ubuntu@10.56.138.63 -X firefox & disown

#Update, upgrade and autoremove
updupgre(){
	lxc exec $newcon -- sudo apt update
	lxc exec $newcon -- sudo apt upgrade -y
	lxc exec $newcon -- sudo apt autoremove -y
}

dpkgfix(){
	lxc exec $newcon -- dpkg --configure -a
}

# Open the container in the opera browser
OpenInOpera(){
    opera $newcon
}

# Create a GitLab container
makeGitLab(){
	contname
	lxc launch ubuntu:18.04 $newcon
	wait4ip
	updupgre
	AutoSSHKey
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash"
	lxc exec $newcon -- sudo EXTERNAL_URL="http://$contip" apt-get install gitlab-ee
#	lxd.lxc exec $newcon -- RealIP=$( ip -4 addr show eth0 | grep inet | awk '{ print $4; }' | sed 's/\/.*$//' )
#	lxd.lxc exec $newcon -- /bin/bash -- sed -i "s/100.2.3.4/$RealIP/g" /etc/gitlab/gitlab.rb
#	lxd.lxc exec $newcon -- /bin/bash -- more /etc/gitlab/gitlab.rb
#    lxd.lxc exec $newcon -- /bin/bash -- gitlab-ctl reconfigure
	lastmessage="The GitLab container $newcon at $contip is now ready for use"
    opera $contip
    logubu
}

# Create an nginx container 
makenginx(){
	contname
	lxc launch --profile default --profile gui ubuntu:18.04 $newcon
	wait4ip
	updupgre
	AutoSSHKey
#	lxd.lxc exec $newcon -- sudo --login --user ubuntu sh -c "ssh-keygen"
	lxc exec $newcon -- apt install nginx -y
	lastmessage="The NGINX container $newcon at $contip is now ready for use"
	logubu
}

# Create a Jenkins container 
makeRuby(){
	contname
	lxc launch ubuntu:18.04 $newcon
	wait4ip
	updupgre
	AutoSSHKey

# Shotgun port:9393 & -o 0.0.0.0 to listen on IP, eg. shotgun myapp.rb -o 0.0.0.0


# import required keys and download ROR
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " cd /tmp && curl -sSL https://get.rvm.io -o rvm.sh && cat /tmp/rvm.sh | bash -s stable --ruby" 

# Install dependencies with apt
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " sudo apt update && sudo apt install -y build-essential rustc libssl-dev libyaml-dev zlib1g-dev libgmp-dev "
# Install Mise version manager
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " curl https://mise.run | sh && echo 'eval "$(~/.local/bin/mise activate bash)"' >> ~/.bashrc && source ~/.bashrc"

# Install Ruby globally with Mise
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " mise use -g ruby@3 "


# Prepare Ruby for use by users ubuntu & root
	lxd.lxc exec $newcon -- sh -c "echo "source /home/ubuntu/.rvm/scripts/rvm" >>~/.profile && source /home/ubuntu/.rvm/scripts/rvm && usermod --append --groups rvm ubuntu && usermod --append --groups rvm root"
# for development:	
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "sudo apt install build-essential -y "

# Login via SSH	
	PrepSSH
	SSHlogin
}

# Create a Gogs container 
makeGogs(){
	contname
	lxc launch ubuntu:18.04 $newcon
	wait4ip
	updupgre
	AutoSSHKey
	lxc exec $newcon -- sudo snap install gogs
	lastmessage="The Gogs container $newcon at $contip:3001 is now ready for use"
	sleep 2
	firefox $newcon:3001
}

# Create a Wordpress container 
makeGoLang(){
	contname
	lxc launch ubuntu:18.04 $newcon
	wait4ip
	updupgre
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "sudo snap install golang-go --classic"
    lastmessage="The GoLang container $newcon at $contip is now ready for use"
    logubu
}

# Create a ROR container 
makeROR(){
	contname
	lxc launch ubuntu:20.04 $newcon
	wait4ip
	updupgre
	InsBuiEss
#	lxc exec $newcon -- sudo --login --user root sh -c "sudo apt install -y ruby rails"
# Install dependencies with apt
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " sudo apt update && sudo apt install -y build-essential rustc libssl-dev libyaml-dev zlib1g-dev libgmp-dev "
# Install Mise version manager
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " curl https://mise.run | sh && echo 'eval "$(~/.local/bin/mise activate bash)"' >> ~/.bashrc && source ~/.bashrc"
# Install Ruby globally with Mise
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " mise use -g ruby@3 "
	RailsDemo

# Login via SSH	
	SSHlogin

# Start browser for Rails app
	firefox $contip:3000 && disown -h

# Login via SSH	
	SSHlogin
}

RORinstall(){
# import required keys and download ROR
	lxc exec $newcon -- sudo --login --user root sh -c "\curl -sSL https://get.rvm.io | bash -s stable --rails
" 

# Prepare Ruby for use by users ubuntu & root
	lxc exec $newcon -- sh -c "echo "source /home/ubuntu/.rvm/scripts/rvm" >>~/.profile && source /home/ubuntu/.rvm/scripts/rvm && usermod --append --groups rvm ubuntu && usermod --append --groups rvm root"
}

RailsDemo(){
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "rails new lxdrailsdemo && cd lxdrailsdemo && rails s --binding=0.0.0.0 -d && exit" 
}

# examine file:	less /tmp/rvm.sh


# 2c ******************** Container Commands ******************


InsBuiEss(){
# Install build essential for development:	
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "sudo apt install build-essential -y "
}

pause(){
# waits for user to hit the enter key...	
  read -p "Press [Enter] key to continue..." fackEnterKey
}

#List available containers
status(){
	lxc list --format csv -c ncs46tpaSP
}

#Display the contents of the Containers log file
conlog(){
	lxc info $newcon --show-log 
}

# Start Container(s)
start(){
	lxc start $newcon
	wait 2
	lastmessage=" $newcon has started. "
}
# Stop Container(s)
stop(){
	lxc stop $newcon
	lastmessage="The Container $newcon has stopped. "
}
# Restart Container(s)
resta(){
	lxc restart $newcon
	lastmessage=" $newcon has been restarted. "
}
# Delete a Container
delcon(){
	lxc stop -v $newcon 
	echo "Destroying the $newcon Container......"
	sleep 4
	lxc delete $newcon -v
	lastmessage="the Container $newcon is no longer with us... "
	clear
}
# *********************** Login ***********************

# Login to a Container as root
loginroot(){
    contname
	status
	lxc exec $newcon -- /bin/bash
}
# Login as the user Ubuntu (used after scripted install)
logubu(){
	status
	lxc exec $newcon  -- sudo --login --user ubuntu
}
# Login to a Container via xterm as root
loginrootinaterminal(){
	contname
	status
	konsole -e "lxd.lxc exec $newcon -- /bin/bash" & disown
}
# Login via xterm as the user Ubuntu
logubuinaterminal(){
	contname
	status
	konsole -e "lxd.lxc exec $newcon  -- sudo --login --user ubuntu" & disown -h
}

# ----------------SSH Options--------------


logubuSSH(){
# Login via SSH as the user Ubuntu (used after scripted installs).	
	read -rp "Enter the name of the target Container: " ssh2con
	sshcontip=$( lxc list $ssh2con --format csv -c 4 | awk '{ print $1; }')
	ssh ubuntu@$sshcontip && disown -h
}

# Login via SSH as the user Ubuntu via new Konsole
logubuSSHKon(){
	read -rp "Enter the name of the target Container: " ssh2con
	sshcontip=$( lxc list $ssh2con --format csv -c 4 | awk '{ print $1; }')
	konsole -e "ssh ubuntu@$sshcontip" & disown -h
}

AutoSSHKey(){
	lxc exec $newcon -- sudo --login --user ubuntu sh -c " printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa"
}

PrepSSH(){
	cp $HOME/.ssh/id_rsa.pub authorized_keys
	lxc file push authorized_keys $newcon/home/ubuntu/.ssh/
}

SSHlogin(){
# Login to a container with SSH.	
	sshcontip=$( lxc list $newcon --format csv -c 4 | awk '{ print $1; }')
	printf 'yes\n' | ssh ubuntu@$sshcontip 
}

SSHXLogin(){
# Login to a container with Xserver via SSH.	
	read -rp "Enter the name of the target Container: " ssh2con
	sshcontip=$( lxc list $ssh2con --format csv -c 4 | awk '{ print $1; }')
	ssh ubuntu@$sshcontip -X && disown -h
}

SSHKonsoleXLogin(){
	read -rp "Enter the name of the target Container: " ssh2con
	sshcontip=$( lxc list $ssh2con --format csv -c 4 | awk '{ print $1; }')
	konsole -e "ssh ubuntu@$sshcontip -X" & disown -h
}

SSHXtermXLogin(){
# Login to container with SSH -X via xterm.
	sshcontip=$( lxc list $newcon --format csv -c 4 | awk '{ print $1; }')
	xterm -e "ssh ubuntu@$sshcontip -X" & disown -h
}	

SSHKey2Con(){
# copy SSH key from Host to Container
	cp $HOME/.ssh/id_rsa.pub authorized_keys
	lxc file push authorized_keys $newcon/home/ubuntu/.ssh/
}

# 2d ************ Install Authorized Software *************

#Install Midnight Commander within a conatiner
insmcinacon(){
	lxc exec $newcon -- $SAI mc
}

# Display details of a container
contdetails(){
	contname
	lxc config show --expanded $newcon
}

# shutdown all containers
shtdwn(){
	lxc shutdown
	echo " All Containers Stopped "
}

wait4ip(){
# This should check when an ip address has been assigned to the container before continuing with the script.
findip4(){
# loop to handle when an IP address is provided.
	contip=$( lxc list $newcon --format csv -c 4  | awk '{ print $1; }') 
	if [ "$contip" == "" ]; then echo " Waiting for an IP address.... $contip " && sleep 1
	else
	echo "$newcon assigned $contip" 
	sleep 1
	fi
}

while [ "$contip" == "" ]
do
	findip4
done
}

# 2e --------------------start and stop containers----------------------


# 2f -------------------- LXD - menu --------------------

# set and display relevant menu information 
LXDmenu(){
	MenuTitle=" LXD - Options"
	Description=" Tools to manage Containers "

LXD_menu(){

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo "10. Install LXD"
	echo "20. List all containers. "
    echo -e "${ORANGE}30. Create an Ubuntu 22.04 Container.			31. Create an Ubuntu 20.04 Container. ${STD}"
	echo -e "${ORANGE}32. Create Application Server Container ${STD}"
#	echo "40. Create an NGINX container."
#	echo "41. Create a Gogs Git container				Port:3001"
	echo -e "${ORANGE}42. Create a Ruby On Rails container.			43. Install Ruby On Rails & NodeJS ${STD}" 
#	echo "44. Create a GoLang container "
#	echo "45. Create a GitLab Container and open in Opera "
	echo "46  Create a Ruby container and login"
	echo -e "${CYAN}50. Login to a Container as Root.			51. Login to a Container as ubuntu. ${STD}"
	echo "52. Login to a Container via Konsole as Root."
	echo "53. Login to a Container via Konsole as ubuntu. "
	echo "55. Login to a Container via Xterm as ubuntu."
	echo -e "${GREEN}56. Copy SSH key from Host to a Container.		57. Login via SSH ${STD}"
	echo "58. Login via SSH with X"
	echo "59. Login via SSH with X via konsole"
	echo "60. Start/Stop Containers. "
	echo "61. Make a host folder available to the container."
	echo "62. Show devices attached to a container"
	echo "63. Display and remove a containers device "
    echo "70. Create an Ephemeral container. "
#	echo "71. Save Container image"
#	echo "72. Load container image."
    echo "80. Detailed container information. "
	echo -e "${MAGENTA}90. Delete a Container.					91. Start a Container.${STD}"
#	echo "3.  Start all Containers."
	echo -e "${MAGENTA}95. Stop a Container.					96. Stop all Containers.	-Shutdown LXD${STD}"
	echo -e "${MAGENTA}97. Restart a Container.${STD}"
#	echo "8.  Restart all Containers."	
	echo "99. Exit this menu "
	echo "-------------------------------" 
    echo " $lastmessage " 
	echo " Existing Containers: " 
	# Display existing containers
	lxc list --format csv -c ns4 | awk '{ print $1; }'
    jobs -l

}

LXD_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		10) LXDsetup ;;
		20) clear && status ;;
		30) contname && makecon && wait4ip && SSHKey2Con && SSHKonsoleXLogin ;;
		31) MakeCon ;;
		32) MakeConAppServer ;;
		40) makenginx ;;
		41) makeGogs ;;
		42) makeROR ;;
		43) RORinstal ;;
		44) makeGoLang ;;
		45) makeGitLab ;;
		46) makeRuby && logubu ;;
		50) loginroot ;;
		51) contname && logubu ;;
		52) loginrootinaterminal ;;
		53) logubuinaterminal ;;
#		54)  ;;
		55) contname && SSHXtermXLogin ;;
		56) contname && SSHKey2Con ;;
		57) logubuSSH ;;
		58) SSHXLogin ;;
		59) SSHKonsoleXLogin ;;
		60) stasto ;;
		61) ShowDevices && contname && DeviceName && HostFolder && ContainerPath && ShareHostFolder && ShowDevices ;;
		62) contname && ShowDevices ;;
		63) contname && ShowDevices && DeviceName && RemoveDevice ;;
        70) Ephemcont && logubu ;;
        80) contdetails ;;
		90) contname && delcon ;;
		91) contname && start ;;
		93)  ;;
		95) status && contname && stop ;;
		96) shtdwn ;;
		97) status && contname && resta ;;
		99) clear && exit ;;
# Capture non listed inputs and send to BASH.
		*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
		esac

}

while true
do
	LXD_menu
	LXD_options
done
}

# check if this is the first run
 InstallCheck

# used to start this menu
# LXDmenu
