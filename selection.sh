#!/bin/bash

# clear

# ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

# -----------------------
SelectorMenu(){

selection(){ 
    local selection=$1
    if [[ ${opts[selection]} ]] # toggle
    then
        opts[selection]=
    else
        opts[selection]=Selected
    fi 
}

PS3='Enter your chosen options : '
while :
do
# Display Options
    clear
    options=("Option 1 ${opts[1]}" "Option 2 ${opts[2]}" "Option 3 ${opts[3]}" "Option 4 ${opts[4]}" "Done")

# Select options 
    select opt in "${options[@]}"
    do
        case $opt in
            "Option 1 ${opts[1]}")
                selection 1
                break
                ;;
            "Option 2 ${opts[2]}")
                selection 2
                break
                ;;
            "Option 3 ${opts[3]}")
                selection 3
                break
                ;;
            "Option 4 ${opts[4]}") 
                selection 4 
                break ;;
            "Done") break 2 ;;
            *) printf '%s\n' 'invalid option';;
        esac
    done
done

echo " "
printf '%s\n' #'Options chosen:'
for opt in "${!opts[@]}"
do
    if [[ ${opts[opt]} ]]
    then
     #printf '%s\n' "Option $opt"
        chosen="Option $opt"
    fi

echo $chosen

done
}

SelectorMenu