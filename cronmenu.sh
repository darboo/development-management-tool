#!/bin/bash

# usage:	crontab [-u user] file
# 	crontab [ -u user ] [ -i ] { -e | -l | -r }
#		(default operation is replace, per 1003.2)

    

#(edit user's crontab)
cronedit(){    
    crontab	-e
}

#(list user's crontab)
cronlist(){
    crontab	-l	
}

#(prompt before deleting user's crontab)
crondel(){
    crontab	-i -r
}

editfile(){
    select-editor
}


