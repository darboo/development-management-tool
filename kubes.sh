#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

export KUBE_EDITOR="/usr/bin/nano"
	NameSpace="default"


pause(){
# waits for user to hit the enter key...	
  read -p "Press [Enter] key to continue..." fackEnterKey
}

RunCheck(){
	if [[  -f !"home/$USER/development-management-tool/lxd0.txt" ]] 
	then lastmessage=$(echo -e "${RED} Install LXD  ${STD}")
	Microk8smenu 
	else 
	lastmessage="Hello $USER" 
	StartMicroK8s # Starts microk8s
	Microk8smenu
	fi


	export KUBE_EDITOR="/usr/bin/nano"
}

# 1 ------------------------------ Setup -----------------------------

SystemUUAA(){
    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get autoremove -y
    sudo apt-get autoclean
}

SSHkeyGenCopy(){
# will verify if a key exists before proceedings 
	printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
	HostSSHKey=$(cat $HOME/.ssh/id_rsa.pub)
	echo $HostSSHKey
	lastmessage="Your SSH Key has been created."
}

SetupPodman(){

	sudo apt install -y podman containers-storage 

}

SetupMicrok8s(){
    SystemUUAA
#     SSHkeyGenCopy
# Install microk8s snap
    sudo snap install microk8s --classic
# Install cURL
	sudo apt install curl
# Configure firewall
    sudo ufw allow in on cni0
    sudo ufw allow out on cni0
    sudo ufw default allow routed
# Add current user to microk8s group
    sudo usermod -a -G microk8s $USER
    sudo chown -f -R $USER ~/.kube
# Install Kompose
	curl -L https://github.com/kubernetes/kompose/releases/download/v1.26.0/kompose-linux-amd64 -o kompose
	chmod +x kompose
	sudo mv ./kompose /usr/local/bin/kompose
# Setup a working folder in the users home folder
	mkdir $HOME/mk8sprojects #CreateProjectFolder
}

SetupMicrok8sLXD(){
# Setup microK8s within an LXD container, source: https://microk8s.io/docs/lxd
# Upon the Host:
	echo "Install & Setup LXD" 
	sudo snap install lxd 
	sudo lxd init
# create and download the microk8s lxd profile for ext4
	lxc profile create microk8s
	wget https://raw.githubusercontent.com/ubuntu/microk8s/master/tests/lxc/microk8s.profile -O microk8s.profile
	cat microk8s.profile | lxc profile edit microk8s

# We can now create the container that MicroK8s will run in. this command uses the ‘default’ profile, for any existing system settings (networking, storage, etc.) before also applying the ‘microk8s’ profile - the order is important.
	lxc launch -p default -p microk8s ubuntu:22.04 microk8s
# First, we’ll need to install MicroK8s within the container.
	lxc exec microk8s -- sudo snap install microk8s --classic
}

SetupMiniKube(){
# https://minikube.sigs.k8s.io/docs/start/
# download
	curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
# install
	sudo install minikube-linux-amd64 /usr/local/bin/minikube
}

SetupBashCompletion(){
# enable BASH completion 
	echo "source <(kubectl completion bash)" >> ~/.bashrc
	echo "source <(kubeadm completion bash)" >> ~/.bashrc
# Reload bash without logging out
	source ~/.bashrc 
	echo "Added BaSH completion "
}


# -------------------------- Usage ---------------------------

StartMicroK8s(){
	echo "Starting microk8s"
	microk8s start
	lastmessage="MicroK8s started"
}

StopMicroK8s(){
	echo "Stopping microk8s ..."
	microk8s stop
	lastmessage="MicroK8s stopped"
}

RestartMicroK8s(){
	microk8s stop && microk8s start
	lastmessage="MicroK8s restarted"
}

ListStatus(){
# Display Addons
	microk8s status
}

AddOnEnable(){
	read -rp "Choose the desired Addons to enable: " EnableAddon
	microk8s enable $EnableAddon
}

AddOnDisable(){
	read -rp "Choose the desired Addons to remove: " DisableAddon
	microk8s disable $DisableAddon
}

DisplaySecretToken(){
# Display Secret Token
	token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
	microk8s kubectl -n kube-system describe secret $token
}

EditIngressConfigmaps(){

	microk8s.kubectl -n ingress edit configmaps nginx-load-balancer-microk8s-conf
# View Ingress pod status
	microk8s.kubectl -n ingress get pods
# Copy above output to below code to view logs
	microk8s.kubectl -n ingress logs nginx-ingress-microk8s-controller-jfcbz | grep reload		
}

NewMicrobot(){
# Create and expose a microbot service
	microk8s kubectl create deployment microbot --image=dontrebootme/microbot:v1
#	microk8s kubectl scale deployment microbot --replicas=2
	microk8s kubectl expose deployment microbot --type=NodePort --port=80 --name=microbot-service 
}

AddTag2Image(){
# Add the local tag to an image	
	docker image tag bitnami/kafka:2.8.1 bitnami_kafka_2.8.1:local
# Save the tagged image as a tarball
	docker save testmodeller_api_1.16.33:local > api.tar
# Import the created image into Kubernetes
	microk8s.ctr image import api.tar
}

DeleteInNamespace(){

	read -rp "Enter the name of the Service, Pod etc./Name: " servpod
	if [[ "$servpod" == "" ]]
	then 
	echo "Enter a name please....."
	else
	microk8s kubectl delete -n $NameSpace $servpod
	fi
}

SelectPod(){

	microk8s kubectl get pods -A 
	read -rp "Enter the name of the Pod: " thispod
	if [[ "$thispod" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo $thispod
	fi
}

DeletePod(){

	microk8s kubectl delete -n $NameSpace pod $thispod
}

SelectLogs(){
# Display pods in namespace
	microk8s kubectl get pods --namespace $NameSpace
# Enter chosen log
	read -rp "Enter the desired Pod/Container: " whatpodcon
	if [[ "$whatpodcon" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo $whatpodcon
	fi
}

ViewLogs(){
#https://medium.com/manikkothu/build-and-deploy-apps-on-microk8s-1df26d1ddd3c
	microk8s kubectl logs -n $NameSpace $whatpodcon
}

DesribePod(){
# https://jamesdefabia.github.io/docs/user-guide/pods/multi-container/#YAML
	microk8s kubectl describe pod -n $NameSpace $whatpodcon
}

BaSH2Container(){

	gnome-terminal --tab -- microk8s kubectl exec -it -n $NameSpace $whatpodcon -- bash
#	meta container
# 	cat user/utilities/migrate-from-graph.sh
#	sed -i 's/postgres_db/postgresdb/g' user/utilities/migrate-from-graph.sh
#	sed -i 's/META_DB/METADB/g' user/utilities/migrate-from-tdm.sh
#	sed -i 's/TDM_DB/TDMDB/g' user/utilities/migrate-from-tdm.sh  
}

CreatePodFromManifest(){
# Display yaml files in project
	ls $HOME/mk8sprojects/$loadproject/
# Enter chosen log
	read -rp "Enter the name of the manifest to use: " thisyaml
	if [[ "$thisyaml" == "" ]]
	then 
	echo "Enter a name please....."
	else
	sudo microk8s kubectl apply -n $NameSpace -f $thisyaml
	fi
}

ImportPackages(){

	cd $HOME/mk8sprojects/$loadproject/saved
# Import the required images for use
	microk8s.ctr image import modeller_api.tar
	microk8s.ctr image import modeller_db.tar
	microk8s.ctr image import modeller_job_engine.tar
	microk8s.ctr image import modeller_kafka.tar
	microk8s.ctr image import modeller_meta.tar
	microk8s.ctr image import modeller_neo4j.tar
	microk8s.ctr image import modeller_nginx.tar
	microk8s.ctr image import modeller_tdm.tar
	microk8s.ctr image import modeller_web.tar
	microk8s.ctr image import modeller_zookeeper.tar

}

DeploymentName(){

# Capture desired deployment name
	read -rp "Enter the desired name for this deployment: " WPname
	if [[ "$WPname" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo $WPname
	fi
}

WPDeploy(){
# https://github.com/bitnami/charts/tree/main/bitnami/wordpress
	cd $HOME/mk8sprojects/$loadproject/
	ls
	microk8s helm repo add my-repo https://charts.bitnami.com/bitnami
	microk8s helm install $WPname my-repo/wordpress
	microk8s kubectl get svc --namespace default -w $WPname-wordpress
	echo Password: $(microk8s kubectl get secret --namespace default my-release-wordpress -o jsonpath="{.data.wordpress-password}" | base64 -d)
}

WPRemove(){
# To uninstall/delete the my-release deployment:

	microk8s helm delete my-release
	microk8s helm delete $WPname

}
# -------------------------- Konversion ----------------------

CreateNewProject(){
# New Project name
	read -rp "Enter the name of the new Project: " newproject
	if [[ "$newproject" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo "$newproject"
	fi
# Set the namespace to equal the project name.
	NameSpace="$newproject"
	mkdir $HOME/mk8sprojects/$newproject
}

LoadProject(){
# Display existing projects in the folder
	ls $HOME/mk8sprojects
# Load Project 
	read -rp "Enter the name of the desired Project: " loadproject
	if [[ "$loadproject" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo "$loadproject"
	fi
	cd $HOME/mk8sprojects/$loadproject
# Set the namespace to equal the project name.
#	NameSpace="$loadproject"
	lastmessage="Current Project: $loadproject"
}

#LoadComposeAndEnvFiles(){}

ProvideNameSpace(){

	read -rp "Enter the name of the desired Name Space: " NameSpace
	if [[ "$NameSpace" == "" ]]
	then 
	echo "Nothing entered, using the Project Name as the Name Space"
	NameSpace="$loadproject"
	else
	NameSpace="$NameSpace"
	fi
# Create a namespace 
	microk8s kubectl create namespace $NameSpace

}

#KonvertToManifestFiles(){}

ApplyToKubern(){

	cd $HOME/mk8sprojects/$loadproject/kubegenop
	sudo microk8s kubectl apply -f testmodeller_meta.yaml
	sudo microk8s kubectl apply -f testmodeller_bitnami_kafka.yaml
	sudo microk8s kubectl apply -f testmodeller_nginx.yaml
	sudo microk8s kubectl apply -f testmodeller_tdm.yaml
	sudo microk8s kubectl apply -f testmodeller_zookeeper.yaml
	sudo microk8s kubectl apply -f testmodeller_api.yaml
	sudo microk8s kubectl apply -f testmodeller_job_engine.yaml
	sudo microk8s kubectl apply -f testmodeller_neo4j.yaml
	sudo microk8s kubectl apply -f testmodeller_postgres_db.yaml
	sudo microk8s kubectl apply -f testmodeller_web.yaml

}

#ViewPodLogs(){}




# -------------------------- Main Menu -----------------------

Microk8smenu(){

Microk8s_menus(){

MenuTitle=" Microk8s Menu"
MenuDescription=" Perform local Kubernetes operations. "

	echo " "	
	echo " $MenuTitle " 
	echo " $MenuDescription " 
	echo " -------------------------------- "
	echo "1. Update, Upgrade, Autoremove and Autoclean == 2. Install Microk8s "
#	echo "3.  Create an LXD container to install Microk8s"
	echo "4. Setup BASH autocompletion"
	echo "5. Enable Registry, DNS & Storage Management"
#	echo "6.  Install Minikube "
	echo "7. Reset Microk8s"
	echo "10. Start MicroK8s = 11. Stop MicroK8s = 12. Restart MicroK8s"
#	echo "20.  "
	echo "21. Microk8s addon status == 22. Enable Addons == 23. Disable Addons"
	echo "24.  Display Config"
	echo "25.  perform inspect and create a tarball"
	echo "30.  Display all $NameSpace pods "
	echo "31. Display all nodes | 32. Display all services | 33. Display all namespaces | 34: Display all data "
	echo "35. Display Endpoints. == 36. Display all pods."
#	echo "40.  Generate an SSH Key and copy to the HostSSHKey variable"
	echo "41.  Tag, Save and Import Tarballs"
	echo "42.  Import saved Modeller Tarballs"
	echo "43.  List All Imported Images"
#	echo "44.  "
	echo "45.  Attach via Bash to a Container"
	echo "46.  Create desired Name Space"
	echo "47.  Delete Pod, service etc. in $NameSpace "
	echo "48.  Delete $NameSpace pods"
	echo "49.  Delete the $NameSpace Name Space"
	echo "50.  select and view logs in $NameSpace == 51. Describe a Pod."
	echo "51.  Describe Pods"
	echo "54.  Create WordPress deployment."
	echo "55.  Delete WordPress deployment."
	echo "60.  Create a Pod From a Manifest."
	echo "61.  Apply modified modeller yaml's."
	echo "70. Delete a Pod."
    echo "80.  Create a new project "
	echo "81.  Load an existing project "
	echo "98. Stop microk8s and exit  "
	echo "99. Exit this tool. "
	echo "Current NameSpace= $NameSpace"
	echo "$lastmessage "
}

Microk8s_options(){

local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) SystemUUAA ;;
		2) SetupMicrok8s ;;
		3) SetupMicrok8sLXD ;;
		4) SetupBashCompletion ;;
		5) microk8s enable dns registry storage ;;
		6) SetupMiniKube ;;
		7) sudo microk8s reset --destroy-storage ;;
		10) StartMicroK8s ;;
		11) StopMicroK8s ;;
		12) RestartMicroK8s ;;
		20)  ;;
		21) ListStatus ;;
		22) ListStatus && AddOnEnable ;;
		23) ListStatus && AddOnDisable ;;
		24) microk8s config ;;
		25) microk8s inspect ;;
		30) microk8s kubectl get pods --namespace $NameSpace ;;
		31) microk8s kubectl get nodes ;;
		32) microk8s kubectl get services ;;
		33) microk8s kubectl get namespaces ;;
		34) microk8s kubectl get all -A ;;
		35) microk8s kubectl get endpoints ;;
		36) microk8s kubectl get pods -A ;;
		40) SSHkeyGenCopy ;;
		41) AddTag2Image ;;
        42) ImportPackages ;;
		43) microk8s ctr images list -q ;;
		44) ;;
		45) SelectLogs && BaSH2Container ;;
		46) ProvideNameSpace ;;
		47) microk8s kubectl get all -A && DeleteInNamespace ;;
		48) microk8s kubectl get pods && DeletePod ;;
		49) microk8s kubectl delete namespaces $NameSpace ;;
		50) SelectLogs && ViewLogs ;;
		51) SelectPod && DesribePod ;;
		54) microk8s kubectl get endpoints && DeploymentName && WPDeploy ;;
		55) microk8s kubectl get endpoints && DeploymentName && WPRemove ;;
		60) CreatePodFromManifest ;;
		61) ApplyToKubern ;;
		70) SelectPod && DeletePod ;;
		80) CreateNewProject ;;
		81) LoadProject ;;
		98) StopMicroK8s && exit ;;
		99) exit ;;
		*) echo -e "${RED} sending $choice to bin/bash.....${STD}" && echo -e $choice | /bin/bash
	esac

}
    while true
    do
	Microk8s_menus
	Microk8s_options
    done
}


 RunCheck # Checks if this is the first run of this application

 Microk8smenu # Displays the main menu
 StartMicroK8s # Starts microk8s


# https://medium.com/manikkothu/build-and-deploy-apps-on-microk8s-1df26d1ddd3c
