#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

REPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"
clear
SAI="sudo apt install -y "

# 6 ----------------------------- UVT-KVM -----------------------------

# 1 ----------------- UVT-KVM INSTALL --------------------

InstallKVM(){
# check the CPU supports virtualization
	cat /proc/cpuinfo
# System update
	sudo apt update && sudo apt autoremove
	$SAI qemu-kvm #- The main package
	$SAI uuid # Generate UUID's
	$SAI libvirt0 #- Includes the libvirtd server exporting the virtualization support
	$SAI libvirt-bin libvirt-doc #- contains virsh 
	$SAI virtinst #- Utility to install virtual machines
	$SAI virt-viewer #- Utility to display graphical console for a virtual machine
	$SAI libosinfo-bin #VM OS Support
	$SAI virt-manager # GUI interface
	$SAI xterm # install xterminal
# the following is an easy way to create VM's
	$SAI uvtool # To download cloud images
#ssh-keygen # create a key to login to containers securely
	uvt-simplestreams-libvirt --verbose sync release=bionic arch=amd64 #To just update/grab Ubuntu 16.04 LTS (xenial/amd64) image
# Source: https://www.cyberciti.biz/faq/how-to-use-kvm-cloud-images-on-ubuntu-linux/
# systemctl status libvirt-bin

# ensure the needed kernel modules have been loaded
	sudo lsmod | grep kvm

# load the modules if not already
	sudo modprobe kvm_intel/kvm_amd

# start the libvirtd daemon at boot time and immediately
	sudo systemctl enable --now libvirtd
# add user to kvm group
	sudo usermod -aG kvm $(whoami)

# Require user action
LogMessage="Please Logout & Login or Restart this host before use."
}

# Convert IMG's to ISO's
IMG2ISO(){
	$SAI ccd2iso
}


# 4 ---------------- KVM USER INPUT --------------------

nameVM(){
    read -rp "Enter the name of the target VM: " VMname
}

# captures user data to create a customized VM
customVM(){
# --------- Custom VM data capture -----------
    read -rp "Enter the name of the target VM: " VMname
    read -rp "Enter the number of Virtual CPU's: " VCPUs
    read -rp "Enter the amount of Virtual RAM required: " VRAM
    read -rp "Size of the virtual HDD in GB's: " VHDD
    read -rp "Enter the OS release version (ie. bionic): " OStype
    read -rp "Enter the required architecture (ie. amd64): " ArchType
    read -rp "Enter the SSH key path or blank for default" SSHKEYPATH
#    read -rp "Enter the software to be installed comma space seperated values: " SUAPIN  #-or read a pre made list

# If keypath not set use default location
	if [ -z "$SSHKEYPATH" ]; then SSHKEYPATH=/home/$USER/.ssh/id_rsa.pub;
	else 
	echo " looking in $SSHKEYPATH "
	fi

# require user validation of inputed data
cVMarray=("$VMname" "$VCPUs" "$VRAM" "$VHDD" "$OStype" "$ArchType" "$SSHKEYPATH" "$SUAPIN" )
# for loop that iterates over each element in cVMarray
	for i in "${cVMarray[@]}"
	do
    echo $i
	done 
	read -rp "Happy with your choices? hit y to continue or any key to re-enter data." Happy

	if [ "$Happy" != "y" ]; then customVM
	else
	echo "Then lets continue "
	
	fi
# update chosen release and arhitecture
	OSpoolupdate
# put the above data into the below function
	uvt-kvm create $VMname --cpu $VCPUs --memory $VRAM --disk $VHDD --ssh-public-key-file $SSHKEYPATH # --release $OStype --arch $ArchType --packages $SUAPIN
}


# 5 ---------------- UVT-KVM USE --------------------

getVMOS(){
# Download the desired OS	
	read -rp "Enter the OS release version: (ie. 18.04) " OStype 
    read -rp "Enter the required architecture: (ie. amd64) " ArchType
	LogMessage="Dowloading $OStype $ArchType image....."
	OSpoolupdate
}
OSpoolupdate(){
	uvt-simplestreams-libvirt --verbose sync release=$OStype arch=$ArchType
	LogMessage="Download complete."
}

ImageUpdate(){
# Update an existing image
	read -rp "Enter the OS release version: (ie. bionic) " OStype 
	uvt-simplestreams-libvirt --verbose sync release=$OStype arch=amd64
}
AddSource(){
	uvt-simplestreams-libvirt --verbose sync --source http://cloud-images.ubuntu.com/daily release=bionic arch=amd64
}

# Display downloaded local OS list
OSImages(){
	uvt-simplestreams-libvirt query
}

SSHSetup(){

	IPofVM=$(uvt-kvm ip $VMname)
	
	ssh ubuntu@$IPofVM
}

SSHinto(){
	uvt-kvm ssh $VMname
}
SSHlxterm(){
	xterm -e "uvt-kvm ssh $VMname" & disown -h
}

VMip(){
	uvt-kvm ip $VMname
}

basicVM(){
# UVTool command to create a virtual machine
	uvt-kvm create $VMname-basic release=bionic arch=amd64
sleep 3
	uvt-kvm ssh $VMname-basic
}

minimalVM(){
# Create a miinimal spec VM for ubuntu
    sudo virt-install --name $VMname --memory 512 --vcpus 1 --disk size=20 --cdrom $UBSE_ISO 
}

M16VM(){
    sudo virt-install --name $VMname --memory 512 --vcpus 1 --disk size=10 --import
#    nohup sudo virt-install --name $VMname --memory 512 --vcpus 1 --disk size=20 --cdrom $UM16_ISO &>/dev/null &
}
#List VM's

detailVM=( "--all" "--transient" "--inactive" "--persistent" "--with-snapshot" "--without-snapshot" "--state-running" "--state-paused" "--state-shutoff" "--state-other" "--autostart" "--no-autostart" "--with-managed-save" "--without-managed-save" "--managed-save" )
# List all VM's
listVMs(){
    virsh list ${detailVM[0]} 
}
showtransientVMs(){
    echo " Transient Virtual Machines "
    virsh list ${detailVM[1]}
}
# List all types of VM

# require an if else do for this bit
autostartVM(){
    virsh autostart $VMname
#    virsh autostart --disable $VMname
Edit
}
startVM(){
    virsh start $VMname
}

editVM(){
    virsh edit $VMname
}
rebootVM(){
    virsh reboot $VMname
}
# this stops a VM
destroyVM(){
    virsh destroy $VMname --graceful
}
startVM(){
    virsh start $VMname
}
pauseVM(){
    virsh suspend $VMname
}
resumeVM(){
    virsh resume $VMname
}
# delete a vm
deleteVM(){
    virsh undefine --domain $VMname
    destroyVM
    showtransientVMs
}
cloneVM(){
    clonedVM="cloned-"$VMname
    sudo virt-clone --original $VMname --name $clonedVM --file /var/lib/libvirt/images/$clonedVM.qcow2
}
UVTKVMOptions(){

uvt_kvm_menu(){
    MenuTitle=" - Virtualisation Options - "
    Description=" Virtual Machine software utilising KVM & UVTools to build and manage virtualised environments. "

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"	
	echo "1.  Display the downloaded Virtualisation OS list "
	echo "2.  Create an ubuntu VM			18.04 1VCPU, 512Mb RAM & 20Gb Storage"
	echo "3.  Create a customised VM		Interactive VM creation"
	echo "4.  Edit a VM "
	echo "5.  Reboot a VM "
	echo "6.  Pause a VM "
	echo "7.  Resume a VM "
	echo "8.  Stop a VM "
	echo "9.  Start a VM "
#	echo "10. Clone a VM * almost there *"
	echo "11. Set a VM to Autostart "
	echo "12. Delete a VM "
	echo "13. Display transient VM's "
#	echo "14  Save a VM "
#	echo "15  Create a Snapshot "
#	echo "16  Create a VM Template"
	echo "17  Configure SSH "
	echo "18  Login to a VM via SSH. "
	echo "30. Add an OS image to download and Virtualise " # do this first
	echo "20. View the IP address of a VM "
	echo "21  Login to a VM via LXTerminal. "
	echo "91  Install KVM & UVT "
	echo "99. Return to Main Menu "
    echo " =========================== "
	echo "Existing Virtual Machines:" && listVMs
	echo "$LogMessage"
}

uvt_kvm_options(){
	local choice
	read -p "Enter choice: [1 - 99] or command: " choice
	case $choice in
		1) OSImages ;;
		2) nameVM && basicVM ;;
		3) customVM ;;
		4) nameVM && editVM ;;
		5) nameVM && rebootVM ;;
		6) nameVM && pauseVM ;;
		7) nameVM && resumeVM ;;
        8) nameVM && destroyVM ;;
		9) nameVM && startVM ;;
		10) nameVM && pauseVM && cloneVM ;;
		11) nameVM && autostartVM ;;
		12) nameVM && deleteVM ;;
		13) clear && showtransientVMs ;;
		17) nameVM && SSHSetup ;;
		18) nameVM && SSHinto ;;
		30) nameVM && getVMOS ;;
		20) nameVM && VMip ;;
		21) nameVM && SSHlxterm ;;
		91) InstallKVM ;;
		99) clear && exit ;;
	*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

while true
do
	uvt_kvm_menu
	uvt_kvm_options
done
}

UVTKVMOptions
# /var/lib/uvtool/libvirt/images


