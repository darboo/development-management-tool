
# ------------------ Securiity --------------------
    SAI="sudo apt install -y "
    ssh-keygen # create a key to login to containers securely
    $SAI uuid # Generate UUID's


# ------------------ User Managment -----------------

UserSetup(){
# Create a new User
    adduser devtwo

# Add user to a group
    usermod -aG sudo devtwo

# To disable the root account, simply use the -l option.
    sudo passwd -l root

# If for some valid reason you need to re-enable the account, simply use the -u option.
#    sudo passwd -u root
}

# ------------------- Port Control & Monitoring ----------------------

Firewall(){
    ufw app list
    ufw allow 22
    ufw enable
    ufw status
}

PortLog(){
    $SAI fail2ban
    sudo nano /etc/fail2ban/jail.local
# In this new file add the following contents:
[sshd]
enabled = true
port = 22
filter = sshd
logpath = /var/log/auth.log
maxretry = 3

    sudo systemctl restart fail2ban

}

SecSharedMem(){
# Shared memory can be used in an attack against a running service, apache2 or httpd for example.
    sudo nano /etc/fstab
    tmpfs /run/shm tmpfs ro, noexec,nosuid 0 0
    tmpfs /run/shm tmpfs defaults,noexec,nosuid 0 0
    sudo reboot
}

# -------------------------

   


# ---------------- Creating a swap file -------------- 
SwapFile(){
# Let's check if a SWAP file exists and it's enabled before we create one.
sudo swapon -s

# To create the SWAP file, you will need to use this.
sudo fallocate -l 4G /swapfile	# same as "sudo dd if=/dev/zero of=/swapfile bs=1G count=4"

# Secure swap.
sudo chown root:root /swapfile
sudo chmod 0600 /swapfile

# Prepare the swap file by creating a Linux swap area.
sudo mkswap /swapfile

# Activate the swap file.
sudo swapon /swapfile

# Confirm that the swap partition exists.
sudo swapon -s

# This will last until the server reboots. Let's create the entry in the fstab.
sudo nano /etc/fstab
: /swapfile	none	swap	sw	0 0

# Swappiness in the file should be set to 0. Skipping this step may cause both poor performance,
# whereas setting it to 0 will cause swap to act as an emergency buffer, preventing out-of-memory crashes.
echo 0 | sudo tee /proc/sys/vm/swappiness
echo vm.swappiness = 0 | sudo tee -a /etc/sysctl.conf
}

# -------------------- Command Execution -----------------

UserSetup
Firewall
#	SecSharedMem
#	PortLog
#	SwapFile




