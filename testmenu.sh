#!/bin/bash  

# Ensures application starts in the users home folder.     
    cd $HOME
    D=$(date +"%T")
# ==== Prepare environments ====
STD='\033[0;0;39m'
RED='\033[0;41;30m'
BLU='\033[[38;5;61m' 
RequestFolder="$HOME/TestData/$JobID-$USER"
TimeDateStamp=$(date)
clear

DevManTooREPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"    

pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}


# ==================== Data Handling ======================

NewProject(){
# Capture the new request ID data
    read -rp "Enter the given Request ID (leave blank to quit): " JobID 
# validate data was entered.  
    if [ "$JobID" = "" ]; 
    then 
    lastmessage="Please enter a Request ID"
    TestOptions
	else 
# Add JobID to Username to Prepare project name.
    ProjectName="$JobID-$USER"
# Prepare test folder.
    RequestFolder="$HOME/TestData/$ProjectName"
	fi
# Validate folder does not already exist and inform user if so.
if [ ! -f "$RequestFolder" ]; 
	then 
    # create and enter the new test folder.     
    mkdir -pv $RequestFolder
    touch $RequestFolder/logs.txt
	lastmessage="Created folder in: $RequestFolder, Copy exported SQL ( TestData.txt ) into this folder." 
    echo "A1- New Project- $D $lastmessage" >> $RequestFolder/logs.txt
    cd $RequestFolder
	else 
	lastmessage="Sorry $USER, That name already exists, try again."
	fi
}

LoadProject(){
# require selector menu for displayed folders.
# Display existing projects (list folders). 
    ls /$HOME/TestData/

# Capture the request ID data
    read -rp "Enter the first part of your Request ID (leave blank to quit): " JobID 
# Create folder based on Request ID.  
    if [ "$JobID" = "" ]; 
    then 
    lastmessage="Please enter a Request ID"
# Prepare test folder.
	else 
# Add JobID to Username to Prepare project name.
    ProjectName="$JobID-$USER"
# Prepare test folder.
    RequestFolder="$HOME/TestData/$ProjectName"
    cd $RequestFolder
    echo "Existing files within the $ProjectName project" 
    ls
    lastmessage="- Loaded project: $ProjectName. -"
    cd $RequestFolder
    echo "B1- Load Project- $D $lastmessage" >> $RequestFolder/logs.txt
	fi
}

#-------------------------------------------------------------------------

CaptureSQLheader(){
# copies the returned header data into lines in a file.
# Delete existing file
    rm $RequestFolder/SQLheads.txt
# Set file location as a variable 
    SQLreturn="$RequestFolder/TestData.txt"
# Make an array
    Header=( )
# copy the returned header data into lines in a file and replace double commas with double spaces (sed). 
    head -1 $SQLreturn | sed 's/"//g' | sed 's/,/,¬/g' | while IFS=¬ read -a SQLheader; 
    do  
# Create file & Display returned data
# for each entry in array...
    for i in "${SQLheader[@]}"
# perform the actions:
    do 
    # Assign C as a counter:
    C=$((C+1))
    # print and append the counter number and array entry data to a file
    echo -e "$C, $i" >> $RequestFolder/SQLheads.txt
    # Display results
    echo "Captured Headers $C $i"
    # Copy data to the array
    Header+=("$C $i")
# and complete this for loop.    
    done

# Display number of elements in the array  
#  for index in "${!SQLheader[@]}"; do echo "$index"; done
# Display the number of fields returned    
    echo -e "C1- Capture Headers- $D - Found ${#Header[@]} Headers -" >> $RequestFolder/logs.txt
    lastmessage="-- ${#Header[@]} SQL Headers Found --"
    done
}

CaptureSQLdata(){
# 6 add test ID's to the returned data and create a file for Soap usage.
# Set working location of files
    DataFile="$RequestFolder/DataLines.txt" # Output File
    SQLreturn="$RequestFolder/TestData.txt" # Source File
    SoapOutput="$RequestFolder/SoapUI.txt"

# delete existing output file.
    rm $DataFile
    rm $RequestFolder/SoapUi.txt
# Declare array
#    declare -a SQLdata=( ) 
# Set start number for the counter.
    C=0
# pass all lines into Arrays except the first (header), remove empty lines (| sed '/^$/d'), add delimter ¬ to preserve commas and replace empty fields with NO_VALUE.  
    tail -n +1 $SQLreturn | sed '/^$/d' |sed 's/,,/,NO_NUMVAL, /g' | sed 's/""/NO_VALUE /g' | sed 's/,/¬,/' | while IFS=¬ read -a SQLdata;
# | sed 's/"NO_NUMVAL",,/"NO_NUMVAL",/g' | sed 's/"NO_VALUE",,/"NO_VALUE",/g' 
    
# Append each returned line with a testID as a CSV's.
    do 
    if [[ "$ProjectName-$C"  == "$ProjectName-0" ]]
    then
    echo "'TEST_ID',${SQLdata[@]}" >> $DataFile
 
    else
    echo "$ProjectName-$C, ${SQLdata[@]}" >> $DataFile
# Create a file for SoapUI 
    echo "$ProjectName/Test_$C, ${SQLdata[0]}" | sed 's/"//g' >> $RequestFolder/SoapUi.txt
    fi
# Increment the counter on each pass  
    C=$((C+1))
# Display number of elements in the array  
    echo "D1-  Capture Data $D - ${#SQLdata[@]} elements captured." >> $RequestFolder/logs.txt
    done
#    cat /home/$USER/TestData/DataLine.txt
}

#---------------------------------------------------------------------

SoapData(){
# Copy SoapUI output to a reduced, simplified file.
    tail -n +1 $RequestFolder/SoapReturn.txt | sed '/^$/d' | sed 's/,/,¬/g' | awk -F¬ '{ print $1,$2,$3,$4 }' | tee $RequestFolder/FilteredSoapReturn.txt
# Send message to user    
    lastmessage="Created: $RequestFolder/FilteredSoapReturn.txt "
}

FilterSoap(){
# Run SoapData to simplify output if not done already.
    if [ -f "$RequestFolder/FilteredSoapReturn.txt" ]
    then echo " - Returned SoapUI already filtered -"
    else SoapData
    fi 
# -- Filter returned SoapUI results, display and save to file.

# Capture data filter requirement
    read -rp "Enter Soap Filter : " FilterSoapData  
# Count matched results
    FilterTheData=$(grep -ciw --color "$FilterSoapData" $RequestFolder/FilteredSoapReturn.txt)
# Display and add applied filters to 1st line of file   
    echo  "-- Found $FilterTheData lines matching filters $FilterSoapData --" | tee $RequestFolder/SoapDataFilter-$FilterSoapData.txt  
# Copy results to file 
    egrep -iw "$FilterSoapData" $RequestFolder/FilteredSoapReturn.txt | tail -n +1 >> $RequestFolder/SoapDataFilter-$FilterSoapData.txt

# Send message to user    
    lastmessage="View Results in: $RequestFolder/SoapDataFilter-$FilterSoapData.txt "
}

# =============================================================

RequestCapture(){
# Copy headers to an array and creates a data capture form from returned headers
   
# Set file location as a variable 
    SQLreturn="$RequestFolder/DataLines.txt"
# Prepare data Capture File   
    echo -e "Column Number,; Header Name,; 1st Desired Match,; Secondary Match,; " &> $RequestFolder/Capture1Form.txt
# Store each request in its own filein its own filein its own file

# copy the returned header data into lines in a file and remove double commas from each line with sed. 
    head -1 $SQLreturn | sed 's/"//g' | while IFS=, read -a SQLheader; 
    do  
# Create file & Display returned data

# for each entry in array...
    for i in "${SQLheader[@]}"
# perform the actions:
    do
    C=$((C+1))
# Prepare file for editing
    echo -e "$C,;¬ $i,;¬              ,;¬ " >> $RequestFolder/Capture1Form.txt
# and complete this FOR loop.    
    done
# Completes the WHILE loop.    
    done
# creates a data capture form from returned headers
    column $RequestFolder/Capture1Form.txt -t -s ";" &> $RequestFolder/CaptureForm.txt
    echo "F1- $D Created Request Form from $SQLreturn" >> $RequestFolder/logs.txt

}

EditCaptureForm(){
# Allow user to edit requirements
    nano $RequestFolder/CaptureForm.txt
# Display output and create completed capture form for filtering.
    cat $RequestFolder/CaptureForm.txt |tee $RequestFolder/CaptureForm.txt
# Backup each request in its own file
    D=$(date +"%T")
#    cat $RequestFolder/CaptureForm.txt &> $RequestFolder/Captured$D.txt
    echo "G1- $D Edited the Request Form" >> $RequestFolder/logs.txt
# display output to user
    lastmessage="happy with the contents of: $RequestFolder/CaptureForm.txt ?"
}

Filter1Numerical(){
# pass data through each filter, reducing results untill left with only matches. 
# Remove last field from array reset and set array  
    unset 'Header[-1]'
    Header+=(" { if (")

#    echo "" | tee $RequestFolder/CaptureData.txt 
    RequFile="$RequestFolder/CaptureForm.txt"
#1    sed 's/ /,/g' $RequFile | tee $RequestFolder/Capture1Form.txt
# Print lines greater than 1 from file to array 
    awk 'FNR > 1 {print $0}' $RequFile | while IFS=¬ read -a FileRequ
    do     
# Remove spaces, commas and set variables     
    COLNUM=$( echo ${FileRequ[0]} | sed 's/,//g')
    REQ=$( echo ${FileRequ[2]} | sed 's/ //' | sed 's/,//')
    LTETMT=$( echo ${FileRequ[3]} | sed 's/ //' | sed 's/,//')
# If no requirement, skip to next requiremnt, otherwise display and store data in array.
if  [ "$REQ" == "" ]
then
    echo "No Requirement specified for $COLNUM - ${FileRequ[1]}"
else

#Perform filtering
    echo "1 - $D vars after ELSE, before if: $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt

# Prepare numerical queries for awk
        if [[ "$REQ" =~ "<" || "$REQ" =~ "=" || "$REQ" =~ ">" ]]
        then
        echo "2- DEV $D caught numerical  $COLNUM $REQ " >> $RequestFolder/logs.txt
        Dollar='$'
        Header+=("($Dollar$COLNUM $REQ )")
        Header+=("&&")
        else
        echo " found nothing, moving on to next test"

    if [[ "$REQ" =~ "neither" || "$REQ" =~ "NEITHER" || "$REQ" =~ "or" || "$REQ" =~ "OR" ||  "$REQ" =~ "not" || "$REQ" =~ "NOT" || "$REQ" =~ "between" ]]
    then
    echo "Q6- $D Ignoring Previous filters...." >> $RequestFolder/logs.txt
    else
    echo
    fi
# Prepare for awk
    fi # close last IF statement
    fi # close 1st IF statement
    echo "-DEV 1- numerical content- $D content of array between FI & DONE ${Header[@]}" >> $RequestFolder/logs.txt
    echo -e "${Header[@]}" &> $RequestFolder/awkInputFile 
    done

    OPFile='1-ETGTLT'
# sed to replace last && with awk end, by reversing input
    cat "$RequestFolder/awkInputFile" | rev | sed 's/&&/} 0$ tnirp )/' | rev | tee $RequestFolder/$OPFile.txt

    LastFile=$(cat $RequestFolder/awkInputFile)
    NoData=' { if ('
    if [[ "$LastFile" == "$NoData" ]]
    then 
    echo "No data for BETWEEN, moving on...."
    echo "$LastFile matches $NoData"
    cp $RequestFolder/DataLines.txt $RequestFolder/Awk1-ETGTLT-out.txt
    else

# Create a log of this event
    cat $RequestFolder/$OPFile.txt >> $RequestFolder/logs.txt
# Start with filtering request from source file
    awk -F, -f $RequestFolder/$OPFile.txt $RequestFolder/DataLines.txt | tee $RequestFolder/Awk$OPFile-out.txt 
# Count the number of columns returned
    FoundTotal=$(awk 'END { print NR }' $RequestFolder/Awk$OPFile-out.txt)
# more looging
    echo "-DEV 2- $OPFile $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
    fi   
}

Filter2Between(){
# pass data through each filter, reducing results untill left with only matches. 
# Remove last field from array reset and set array  
    unset 'Header[-1]'
    Header+=(" { if (")

#    echo "" | tee $RequestFolder/CaptureData.txt 
    RequFile="$RequestFolder/CaptureForm.txt"
#1    sed 's/ /,/g' $RequFile | tee $RequestFolder/Capture1Form.txt
# Print lines greater than 1 from file to array 
    awk 'FNR > 1 {print $0}' $RequFile | while IFS=¬ read -a FileRequ
    do     
# Remove spaces, commas and set variables     
    COLNUM=$( echo ${FileRequ[0]} | sed 's/,//g')
    REQ=$( echo ${FileRequ[2]} | sed 's/ //' | sed 's/,//')
    LTETMT=$( echo ${FileRequ[3]} | sed 's/ //' | sed 's/,//')
# If no requirement, skip to next requiremnt, otherwise display and store data in array.
if  [ "$REQ" == "" ]
then
    echo "No Requirement specified for $COLNUM - ${FileRequ[1]}"
else
#Perform filtering
    echo "1 - $D vars after ELSE, before if: $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt

# Matching within a range of numbers         
        if [[ "$REQ" =~ "between" ]]
        then
        REQ1=$(echo $REQ | sed 's/between//')
        echo $REQ1
        Dollar='$'
        Header+=("($Dollar$COLNUM >= $REQ1 && $Dollar$COLNUM <= $LTETMT)")
        Header+=("&&")
        else
        echo " found nothing, moving on to next test"

    if [[ "$REQ" =~ "neither" || "$REQ" =~ "NEITHER" || "$REQ" =~ "or" || "$REQ" =~ "OR" ||  "$REQ" =~ "not" || "$REQ" =~ "NOT" || "$REQ" =~ "<" || "$REQ" =~ "=" || "$REQ" =~ ">" ]]
    then
    echo "Q2- BETWEEN $D Ignoring Previous filters...." >> $RequestFolder/logs.txt
    else
   echo
    fi
# Prepare for awk
    fi # close last IF statement
    fi # close 1st IF statement
    echo "-DEV 1- numerical content- $D content of array between FI & DONE ${Header[@]}" >> $RequestFolder/logs.txt
    echo -e "${Header[@]}" &> $RequestFolder/awkInputFile 
    done

    OPFile='2-BETWEEN'
# sed to replace last && with awk end, by reversing input
    cat "$RequestFolder/awkInputFile" | rev | sed 's/&&/} 0$ tnirp )/' | rev | tee $RequestFolder/$OPFile.txt

# Validate data was capture creates file for next query if not
    LastFile=$(cat $RequestFolder/awkInputFile)
    NoData=' { if ('
    if [[ "$LastFile" == "$NoData" ]]
    then 
    echo "No data for BETWEEN, moving on...."
    echo "$LastFile matches $NoData"
    cp $RequestFolder/Awk1-ETGTLT-out.txt $RequestFolder/Awk2-BETWEEN-out.txt
    else

# Create a log of this event
    cat $RequestFolder/$OPFile.txt >> $RequestFolder/logs.txt
# Continue filtering request from previous query file
    awk -F, -f $RequestFolder/$OPFile.txt $RequestFolder/Awk1-ETGTLT-out.txt | tee $RequestFolder/Awk$OPFile-out.txt 
# Count the number of columns returned
    FoundTotal=$(awk 'END { print NR }' $RequestFolder/Awk$OPFile-out.txt)
# more looging
    echo "-DEV 2- $OPFile $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
    fi
}

Filter3Not(){
# pass data through each filter, reducing results untill left with only matches. 
# Remove last field from array reset and set array  
    unset 'Header[-1]'
    Header+=(" { if (")

#    echo "" | tee $RequestFolder/CaptureData.txt 
    RequFile="$RequestFolder/CaptureForm.txt"
#1    sed 's/ /,/g' $RequFile | tee $RequestFolder/Capture1Form.txt
# Print lines greater than 1 from file to array 
    awk 'FNR > 1 {print $0}' $RequFile | while IFS=¬ read -a FileRequ
    do     
# Remove spaces, commas and set variables     
    COLNUM=$( echo ${FileRequ[0]} | sed 's/,//g')
    REQ=$( echo ${FileRequ[2]} | sed 's/  //' | sed 's/,//')
    LTETMT=$( echo ${FileRequ[3]} | sed 's/ //' | sed 's/,//')
# If no requirement, skip to next requiremnt, otherwise display and store data in array.
if  [ "$REQ" == "" ]
then
    echo "No Requirement specified for $COLNUM - ${FileRequ[1]}"
else
#Perform filtering
    echo "1 - $D vars after ELSE, before if: $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt

if [[ "$REQ" =~ "neither" || "$REQ" =~ "NEITHER" || "$REQ" =~ "or" || "$REQ" =~ "OR" || "$REQ" =~ "between" ||  "$REQ" =~ "<" || "$REQ" =~ "=" || "$REQ" =~ ">" ]]
    then
    echo "Q3- NOT $D Ignoring Previous filters...." >> $RequestFolder/logs.txt
    else

# Prepare NOT filter
        if [[ "$REQ" =~ "not" || "$REQ" =~ "NOT" ]]
        then
        REQ2=$(echo $REQ | sed 's/not//' | sed 's/NOT//')
        echo $REQ $REQ2
        Dollar='$'
        Header+=("!($Dollar$COLNUM ~ /$REQ2/)")
        Header+=("&&")
        Header+=("!($Dollar$COLNUM == "$REQ2")")
        Header+=("&&")
        echo "3- DEV- NOT Text Filter $D content of variables between ELSE & FI $COLNUM $REQ " >> $RequestFolder/logs.txt
        else
        echo " found nothing, moving on to next test"

    
   echo
    fi
# Prepare for awk
    fi # close last IF statement
    fi # close 1st IF statement
    echo "-DEV 1- numerical content- $D content of array between FI & DONE ${Header[@]}" >> $RequestFolder/logs.txt
    echo -e "${Header[@]}" &> $RequestFolder/awkInputFile 
    done

    OPFile='3-NOT'
# sed to replace last && with awk end, by reversing input
    cat "$RequestFolder/awkInputFile" | rev | sed 's/&&/} 0$ tnirp )/' | rev | tee $RequestFolder/$OPFile.txt

# Validate data was capture creates file for next query if not
    LastFile=$(cat $RequestFolder/awkInputFile)
    NoData=' { if ('
    if [[ "$LastFile" == "$NoData" ]]
    then 
    echo "No data for NOT, moving on...."
    echo "$LastFile matches $NoData"
    cp $RequestFolder/Awk2-BETWEEN-out.txt $RequestFolder/Awk3-NOT-out.txt
    else

# Create a log of this event
    cat $RequestFolder/$OPFile.txt >> $RequestFolder/logs.txt
# Continue filtering request from previous query file
    awk -F, -f $RequestFolder/$OPFile.txt $RequestFolder/Awk2-BETWEEN-out.txt | tee $RequestFolder/Awk$OPFile-out.txt 
# Count the number of columns returned
    FoundTotal=$(awk 'END { print NR }' $RequestFolder/Awk$OPFile-out.txt)
# more looging
    echo "-DEV 2- $OPFile $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
    fi
}

Filter4Or(){
# pass data through each filter, reducing results untill left with only matches. 
# Remove last field from array reset and set array  
    unset 'Header[-1]'
    Header+=(" { if (")

#    echo "" | tee $RequestFolder/CaptureData.txt 
    RequFile="$RequestFolder/CaptureForm.txt"
#1    sed 's/ /,/g' $RequFile | tee $RequestFolder/Capture1Form.txt
# Print lines greater than 1 from file to array 
    awk 'FNR > 1 {print $0}' $RequFile | while IFS=¬ read -a FileRequ
    do     
# Remove spaces, commas and set variables     
    COLNUM=$( echo ${FileRequ[0]} | sed 's/,//g')
    REQ=$( echo ${FileRequ[2]} | sed 's/  //' | sed 's/,//')
    LTETMT=$( echo ${FileRequ[3]} | sed 's/ //' | sed 's/,//')
# If no requirement, skip to next requiremnt, otherwise display and store data in array.
if  [ "$REQ" == "" ]
then
    echo "No Requirement specified for $COLNUM - ${FileRequ[1]}"
else

#Perform filtering
    echo "1 - $D vars after ELSE, before if: $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt

     if [[ "$REQ" =~ "neither" || "$REQ" =~ "NEITHER" || "$REQ" =~ "not" || "$REQ" =~ "NOT" || "$REQ" =~ "between" ||  "$REQ" =~ "<" || "$REQ" =~ "=" || "$REQ" =~ ">" ]]
    then
    echo "Q4- OR $D Ignoring Previous filters...." >> $RequestFolder/logs.txt
    else      

# Handle OR query !!MODIFY SQL OUTPUT TO BE LESS SIMILAR (EOOC, OOC, LOOC, MOOC) FOR THIS TO WORK correctly!!
        if [[ "$REQ" =~ "or" || "$REQ" =~ "OR" ]]
        then
        
        echo "2- $D OR query $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt
        REQ3=$(echo $REQ | sed 's/or//' | sed 's/OR//')
        Dollar='$'
        Header+=("($Dollar$COLNUM ~ /$REQ3/ || $Dollar$COLNUM ~ /$LTETMT/)")
        Header+=("&&")
        else
        echo " found nothing matching OR, moving on to next test"
   echo
    fi
# Prepare for awk
    fi # close last IF statement
    fi # close 1st IF statement
    echo "-DEV 1- numerical content- $D content of array between FI & DONE ${Header[@]}" >> $RequestFolder/logs.txt
    echo -e "${Header[@]}" &> $RequestFolder/awkInputFile 
    done

    OPFile='4-OR'
# sed to replace last && with awk end, by reversing input
    cat "$RequestFolder/awkInputFile" | rev | sed 's/&&/} 0$ tnirp )/' | rev | tee $RequestFolder/$OPFile.txt

# Validate data was capture creates file for next query if not
    LastFile=$(cat $RequestFolder/awkInputFile)
    NoData=' { if ('
    if [[ "$LastFile" == "$NoData" ]]
    then 
    echo "No data for OR, moving on...."
    echo "$LastFile matches $NoData"
    cp $RequestFolder/Awk3-NOT-out.txt $RequestFolder/Awk4-OR-out.txt
    else

# Create a log of this event
    cat $RequestFolder/$OPFile.txt >> $RequestFolder/logs.txt
# Continue filtering request from previous query file
    awk -F, -f $RequestFolder/$OPFile.txt $RequestFolder/Awk3-NOT-out.txt | tee $RequestFolder/Awk$OPFile-out.txt 
# Count the number of columns returned
    FoundTotal=$(awk 'END { print NR }' $RequestFolder/Awk$OPFile-out.txt)
# more loging
    echo "-DEV 2- $OPFile $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
    fi
}

Filter5Neither(){
# pass data through each filter, reducing results untill left with only matches. 
# Remove last field from array reset and set array  
    unset 'Header[-1]'
    Header+=(" { if (")

#    echo "" | tee $RequestFolder/CaptureData.txt 
    RequFile="$RequestFolder/CaptureForm.txt"
#1    sed 's/ /,/g' $RequFile | tee $RequestFolder/Capture1Form.txt
# Print lines greater than 1 from file to array 
    awk 'FNR > 1 {print $0}' $RequFile | while IFS=¬ read -a FileRequ
    do     
# Remove spaces, commas and set variables     
    COLNUM=$( echo ${FileRequ[0]} | sed 's/,//g')
    REQ=$( echo ${FileRequ[2]} | sed 's/  //' | sed 's/,//')
    LTETMT=$( echo ${FileRequ[3]} | sed 's/  //' | sed 's/,//')
# If no requirement, skip to next requiremnt, otherwise display and store data in array.
if  [ "$REQ" == "" ]
then
    echo "No Requirement specified for $COLNUM - ${FileRequ[1]}"
else
    if [[  "$REQ" =~ "or" || "$REQ" =~ "OR" ||  "$REQ" =~ "not" || "$REQ" =~ "NOT" || "$REQ" =~ "between" ||  "$REQ" =~ "<" || "$REQ" =~ "=" || "$REQ" =~ ">" ]]
    then
    sed
    echo "Q5- NEITHER $D Ignoring Previous filters...." >> $RequestFolder/logs.txt
    else
#Perform filtering
    echo "1 - $D vars after ELSE, before if: $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt

# Handle NEITHER (NOR) query
        if [[ "$REQ" =~ "neither" || "$REQ" =~ "NEITHER" ]]
        then
        echo "2-dev $D NEITHER query $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt
        REQ3=$(echo $REQ | sed 's/neither//' | sed 's/NEITHER//')
        Dollar='$'
        Header+=("!($Dollar$COLNUM == "$REQ3") || !($Dollar$COLNUM == "$LTETMT")")
        Header+=("&&")
        Header+=("!($Dollar$COLNUM ~ /$REQ3/) || !($Dollar$COLNUM ~ /$LTETMT/)")
        Header+=("&&")
        else
        echo " found nothing matching NEITHER, moving on to next test"

    
   echo
    fi
# Prepare for awk
    fi # close last IF statement
    fi # close 1st IF statement
    echo "-DEV NEITHER 1- numerical content- $D content of array between FI & DONE ${Header[@]}" >> $RequestFolder/logs.txt
    echo -e "${Header[@]}" &> $RequestFolder/awkInputFile 
    done

    OPFile='5-NEITHER'
# sed to replace last && with awk end, by reversing input
    cat "$RequestFolder/awkInputFile" | rev | sed 's/&&/} 0$ tnirp )/' | rev | tee $RequestFolder/$OPFile.txt

# Validate data was capture creates file for next query if not
    LastFile=$(cat $RequestFolder/awkInputFile)
    NoData=' { if ('
    if [[ "$LastFile" == "$NoData" ]]
    then 
    echo "No data for NEITHER, moving on...."
    echo "$LastFile matches $NoData"
    cp $RequestFolder/Awk4-OR-out.txt $RequestFolder/Awk5-NEITHER-out.txt
    else

# Create a log of this event
    cat $RequestFolder/$OPFile.txt >> $RequestFolder/logs.txt
# Continue filtering request from previous query file
    awk -F, -f $RequestFolder/$OPFile.txt $RequestFolder/Awk4-OR-out.txt | tee $RequestFolder/Awk$OPFile-out.txt 
# Count the number of columns returned
    FoundTotal=$(awk 'END { print NR }' $RequestFolder/Awk$OPFile-out.txt)
# more loging
    echo "5- NEITHER -DEV  $OPFile $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
    fi
}

Filter6Match(){        
# Final query catches text matches but cant match previous searches so fails..
# Remove last field from array reset and set array  
    unset 'Header[-1]'
    Header+=(" { if (")

#    echo "" | tee $RequestFolder/CaptureData.txt 
    RequFile="$RequestFolder/CaptureForm.txt"
#1    sed 's/ /,/g' $RequFile | tee $RequestFolder/Capture1Form.txt
# Print lines greater than 1 from file to array 
    awk 'FNR > 1 {print $0}' $RequFile | while IFS=¬ read -a FileRequ
    do     
# Remove spaces, commas and set variables     
    COLNUM=$( echo ${FileRequ[0]} | sed 's/,//g')
    REQ=$( echo ${FileRequ[2]} | sed 's/ //' | sed 's/,//')
    LTETMT=$( echo ${FileRequ[3]} | sed 's/ //' | sed 's/,//')
# If no requirement, skip to next requiremnt, otherwise display and store data in array.
if  [ "$REQ" == "" ]
then
    echo "No Requirement specified for $COLNUM - ${FileRequ[1]}"
else
    echo "-Dev Output - $D vars after ELSE, before if: $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt
# Prepare to neutralise the above filters for final pass.

    if [[ "$REQ" =~ "neither" || "$REQ" =~ "NEITHER" || "$REQ" =~ "or" || "$REQ" =~ "OR" ||  "$REQ" =~ "not" || "$REQ" =~ "NOT" || "$REQ" =~ "between" ||  "$REQ" =~ "<" || "$REQ" =~ "=" || "$REQ" =~ ">" ]]
    then
    echo "Q6- $D Ignoring Previous filters...." >> $RequestFolder/logs.txt
    else
# Prepare text data for awk
        Dollar='$'
        Header+=("($Dollar$COLNUM ~ /$REQ/)")
        Header+=("&&")
        echo "3- DEV- Text Filter $D content of variables between ELSE & FI $COLNUM $REQ $LTETMT" >> $RequestFolder/logs.txt
        fi
#    echo "3- content of array after empty filter ${Header[@]}"
    echo "4- $D content of array between FI & DONE ${Header[@]}" >> $RequestFolder/logs.txt
    fi
    echo -e "${Header[@]}" &> $RequestFolder/awkInputFile 
done

# sed to replace last && with awk end, 
    cat "$RequestFolder/awkInputFile" | rev | sed 's/&&/} 0$ tnirp )/' | rev | tee $RequestFolder/awkInputFile

    cat $RequestFolder/awkInputFile >> $RequestFolder/logs.txt

    awk -F, -f $RequestFolder/awkInputFile $RequestFolder/Awk5-NEITHER-out.txt | tee $RequestFolder/AwkFilterOut.txt
    
# Count the number of columns returned
    FoundTotal=$(awk 'END { print NR }' $RequestFolder/AwkFilterOut.txt)

    echo "6- DEV- FT&ND $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
    lastmessage="-- $FoundTotal results for queries"

#    awk -F "\t" '{ if(($7 == 6) && ($8 >= 11000000 && $8 <= 25000000)) { print } }' pos_cut.txt
}

# ========================================================================

SelectorMenu(){
# Copy SoapUI output to a reduced, simplified file.

#    InData=$(cat $RequestFolder/DataLines.txt | awk -F, '{ print $2 }' )
    sed 's/\"//g' $RequestFolder/DataLines.txt |tee $RequestFolder/DataLinesNodc.txt

    cat $RequestFolder/SoapReturn.txt | sed '/^$/d' | sed 's/,/¬,/g' | while IFS=¬ read -a SoapFilter; 
    do
    CustID=$(echo "${SoapFilter[1]}" | sed 's/,//' | sed 's/ //')
    BannerID=$(echo "${SoapFilter[2]}" | sed 's/,//' | sed 's/"//g' | sed 's/ //')
    AccountID=($(awk -F, '{ print $2 }' $RequestFolder/DataLines.txt))

    ColumnHeader=($(head -1 $RequestFolder/DataLines.txt | awk -F, '{ print $2 }'))
# awk 'FNR > 1 {print $0}'
    for i in "${AccountID[@]}"
    do 
    C=$((C+1))
    echo " Evaluated $C files.."
    if [[ "$CustID" == "$i" ]]
    then
    echo "-- Found $BannerID match for $CustID & $i, -- $C completed. "
    echo " Banner $BannerID Found for $CustID & $i " >> $RequestFolder/BannCustMatch.txt

    sed "s/$/,$BannerID/" $RequestFolder/DataLines.txt >> $RequestFolder/TestLines.txt


    fi
    done
    done
#    | awk -F¬ '{ print $1,$2,$3,$4 }' &> $RequestFolder/FilteredSoapReturn.txt

    echo "6- DEV2- Soapmath $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
#    SoapFilter=$"cat $RequestFolder/SoapReturn.txt | sed 's/,/¬,/g' | awk -F¬ '{ print $2,$3,$4 }'"

# Send message to user    
    lastmessage="Created: $RequestFolder/FilteredSoapReturn.txt "

       
# Set file location as a variable 
    SQLreturn="$RequestFolder/TestLines.txt"
# Prepare data Capture File   
    echo -e "Column Number,; Header Name,; 1st Desired Match,; Secondary Match,; " &> $RequestFolder/Capture1Form.txt
# Store each request in its own file

# copy the returned header data into lines in a file and remove double commas from each line with sed. 
    head -1 $SQLreturn | sed 's/"//g' | while IFS=, read -a SQLheader; 
    do  
# Create file & Display returned data

# for each entry in array...
    for i in "${SQLheader[@]}"
# perform the actions:
    do
    C=$((C+1))
# Prepare file for editing
    echo -e "$C,;¬ $i,;¬              ,;¬ " >> $RequestFolder/Capture1Form.txt
# and complete this FOR loop.    
    done
# Completes the WHILE loop.    
    done
# creates a data capture form from returned headers
    column $RequestFolder/Capture1Form.txt -t -s ";" &> $RequestFolder/CaptureForm-$D.txt
    echo "F1- $D Created Request Form from $SQLreturn" >> $RequestFolder/logs.txt

    nano $RequestFolder/CaptureForm-$D.txt
}

ReturnedSoap(){
# Display all column data from the 1st line
    head -2 $RequestFolder/SoapReturn.txt | awk -F, '{ print $0 }' | tee $RequestFolder/SoapReturnHeaders.txt
    
    SoapHeaders="$( cat $RequestFolder/SoapReturnHeaders.txt )"
    echo "J1- $D Created $RequestFolder/SoapReturnHeaders.txt" >> $RequestFolder/logs.txt    

}

ReviewCaptureForm(){
# Display     
    echo "--Current Captue Form:"
    cat $RequestFolder/CaptureForm.txt
}

Developing(){
# Works in progress...
    rm $RequestFolder/BannCustMatch.txt
#    InData=$(cat $RequestFolder/DataLines.txt | awk -F, '{ print $2 }' )
#    sed 's/\"//g' $RequestFolder/DataLines.txt |tee $RequestFolder/DataLinesNodc.txt
# Add CC_STRATEGY to last line of headers for TestLines 

    head -1 $RequestFolder/DataLines.txt | sed -e '1s/$/,"CC_STRATEGY"/' | tee $RequestFolder/TestLines.txt

sleep 1

# Read data from SoapReturn to array (no headers)
    awk 'FNR > 0 {print $0}' $RequestFolder/SoapReturn.txt | sed '/^$/d' | sed 's/,/¬,/g' | while IFS=¬ read -a SoapFilter; 
    do
# Display required columns and assign to variables.
    CustID=$(echo "${SoapFilter[1]}" | sed 's/,//' | sed 's/ //')
    BannerID=$(echo "${SoapFilter[2]}" | sed 's/,//' | sed 's/"//g' | sed 's/ //')
    
    AccountID=($(awk -F, 'FNR > 1 { print $2 }' $RequestFolder/DataLines.txt))
#    AccountID=$(awk -F, 'FNR > 1 {print $2}' $RequestFolder/DataLines.txt )

# Pass each account through a loop
    for i in "${AccountID[@]}"
    do 
# Setup a counter for each pass of this loop    
    Count1=$((Count1+1))
    echo " Evaluated $Count1 lines, checked $i from ${#AccountID[@]} lines in file.."
#    echo "  -${AccountID[0]} --$i ---${AccountID[1]}"
# Compare accounts from SoapReturn & DataLines 
    if [ "$CustID" == "$i" ]
    then
# Display matching Banners and copy to logs.txt   
    echo "-- Found $BannerID match for $CustID & $i, -- $Count1 completed. "
    echo " Matching $CustID & $i for $BannerID" >> $RequestFolder/BannCustMatch.txt

# Add matching Banner to last column of TestLines
# | sed -e "s/$/,$BannerID/" |
    FilterSoapData=$(egrep -iw --color "$CustID" $RequestFolder/DataLines.txt)
    echo "$FilterSoapData,$BannerID" | sed 's/"//g' >> $RequestFolder/TestLines.txt

    
# End this IF filter    
    fi
# Close the FOR loop.    
    done
# Close the WHILE loop.    
    done

    echo " DEV- Soapmatch $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
#    SoapFilter=$"cat $RequestFolder/SoapReturn.txt | sed 's/,/¬,/g' | awk -F¬ '{ print $2,$3,$4 }'"

# Send message to user    
    lastmessage="Created: $RequestFolder/FilteredSoapReturn.txt "

# Columnize 
     column -t -s "," $RequestFolder/TestLines.txt &> $RequestFolder/TestLinesColumns.txt
}

Duplicates(){

# Find Duplicates
#    sort TestLines.txt | awk -F, '{print $1,$2,$3,$4,$22,$23,$24,$25,$26}' | uniq -cd
    sort TestLines.txt | awk -F, '{print $1,$2,$26}' | uniq -cd

    head -1 $RequestFolder/TestLines.txt | sed 's/,/¬,/g' | awk -F, '{print $1,$2,$3,$4,$22,$23,$24,$26}' | column -t -s "¬" | tee $RequestFolder/Duplicates.txt

    DuplicateEntries=$(awk -F, '{print $2}' $RequestFolder/TestLines.txt | uniq -d)
    for i in "${DuplicateEntries[@]}"
    do egrep -iw "$i" $RequestFolder/TestLines.txt | sed 's/,/¬,/g' | awk -F, '{print $1,$2,$3,$4,$22,$23,$24,$26}' | column -t -s "¬" >> $RequestFolder/Duplicates.txt
    done

}

Developing2(){
# Works in progress...
    rm $RequestFolder/BannCustMatch.txt
#    InData=$(cat $RequestFolder/DataLines.txt | awk -F, '{ print $2 }' )
#    sed 's/\"//g' $RequestFolder/DataLines.txt |tee $RequestFolder/DataLinesNodc.txt
# Add CC_STRATEGY to last line of headers for TestLines 

    head -1 $RequestFolder/DataLines.txt | sed -e '1s/$/,"CC_STRATEGY"/' | tee $RequestFolder/TestLines.txt

sleep 1

# Read data from SoapReturn to array (no headers)
    awk 'FNR > 0 {print $0}' $RequestFolder/SoapReturn.txt | sed '/^$/d' | sed 's/,/¬,/g' | while IFS=¬ read -a SoapFilter; 
    do
# Display required columns and assign to variables.
    CustID=$(echo "${SoapFilter[1]}" | sed 's/,//' | sed 's/ //')
    BannerID=$(echo "${SoapFilter[2]}" | sed 's/,//' | sed 's/"//g' | sed 's/ //')
    
    AccountID=($(awk -F, 'FNR > 1 { print $2 }' $RequestFolder/DataLines.txt))
#    AccountID=$(awk -F, 'FNR > 1 {print $2}' $RequestFolder/DataLines.txt )
    DataLinesID=($(awk -F, 'FNR > 1 {print $2}' $RequestFolder/TestLines.txt))
    RunLinesID=($(for i in "${DataLinesID[@]}"; do echo "$i"; done;))

# Pass each account through a loop
    for i in "${AccountID[@]}"
    do 
#    for j in "${DataLinesID[@]}"
#    do

# Setup a counter for each pass of this loop    
    Count1=$((Count1+1))
    echo " Evaluated $Count1 lines, $i, $RunLinesID from ${#AccountID[@]} lines in file.."

    # Compare accounts from SoapReturn & DataLines 
    if [[ "$CustID" == "$i" ]] #&& "$i" != "$RunLinesID" ]]
    then



#    if [[ "$i" != "$DataLinesID" ]]
#    then


# Display matching Banners and copy to logs.txt   
    echo "-- Found $BannerID match for $CustID & $i, -- $Count1 completed. "
    echo " Matching $CustID & $i for $BannerID" >> $RequestFolder/BannCustMatch.txt

# Add matching Banner to last column of TestLines
# | sed -e "s/$/,$BannerID/" |
    FilterSoapData=$(egrep -iw --color "$CustID" $RequestFolder/DataLines.txt)
    echo "$FilterSoapData,$BannerID" | sed 's/"//g' >> $RequestFolder/TestLines.txt

# End this IF filter    
    fi
#   done 

# Close the FOR loop.    
    done
# Close the WHILE loop.    
    done

# remove duplicates
    sort $RequestFolder/TestLines.txt | uniq &> $RequestFolder/UniqLines.txt 
    sort $RequestFolder/BannCustMatch.txt | uniq &> $RequestFolder/UniqBannCustMatch.txt 

    echo " DEV- Soapmatch $D Matching records: $FoundTotal" >> $RequestFolder/logs.txt
#    SoapFilter=$"cat $RequestFolder/SoapReturn.txt | sed 's/,/¬,/g' | awk -F¬ '{ print $2,$3,$4 }'"

# Send message to user    
    lastmessage="Created: $RequestFolder/FilteredSoapReturn.txt "

# Columnize 
     column -t -s "," $RequestFolder/TestLines.txt &> $RequestFolder/TestLinesColumns.txt
}


Developing3(){
# Continuation of Developing
# Set file location as a variable 
    SQLreturn="$RequestFolder/TestLines.txt" 
# Prepare data Capture File   
    echo -e "Column Number,; Header Name,; 1st Desired Match,; Secondary Match,; " &> $RequestFolder/Capture1Form.txt
# Store each request in its own file

# copy the returned header data into lines in a file and remove double commas from each line with sed. 
    head -1 $SQLreturn | sed 's/"//g' | while IFS=, read -a SQLheader; 
    do  
# Create file & Display returned data

# for each entry in array...
    for i in "${SQLheader[@]}"
# perform the actions:
    do
    C=$((C+1))
# Prepare file for editing
    echo -e "$C,;¬ $i,;¬              ,;¬ " >> $RequestFolder/Capture1Form.txt
# and complete this FOR loop.    
    done
# Completes the WHILE loop.    
    done
# creates a data capture form from returned headers
    column $RequestFolder/Capture1Form.txt -t -s ";" &> $RequestFolder/CaptureForm-$D.txt
    echo "F1- $D Created Request Form from $SQLreturn" >> $RequestFolder/logs.txt

    nano $RequestFolder/CaptureForm-$D.txt
}

# Display all column data for each line

# ========================= Main Menu ========================

TestOptions(){

MenuTitle=" Data Handling "
Description=" Data aquisition, management and filtering tools "

    test_menu(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "
    echo " =========================================== "
	echo " -- Current Project: $ProjectName -- "   
    echo " =========================================== "
	echo ""
	echo "1.  Run Complete Process"
	echo "2.   "
	echo "3.  Create New Project "
	echo "4.  Load Existing Project: "
	echo "5.  Returned SQL Column Headers: "  
	echo "6.  Create SoapUI query and add testID's to Data " 
    echo "7.  Filter SoapUI results to a file  "
    echo "8.  Search for desired field in SoapUI Return. "
	echo "9.   "
	echo "10. Request Statement Capture"
	echo "11. Edit the request form "
	echo "12. Filter Text and Number Data N/A"
    echo "13. Capture and run queries N/A"
    echo "14. Soap Headers " 
    echo "15.   "
    echo "16. Selection  "
	echo "20. Run All Filters "
	echo "21. Filter >, < or ="
    echo "22. Filter Range"
    echo "23. Filter Not"
    echo "24. Filter Or"
    echo "25. Filter Neither"
    echo "26. Filter Contains"
    echo "27. Review Capture Form "
    echo "30. Developing "
    echo "31. Developing2 "
    echo "40. Duplicates"
    echo "63. View Logs "
	echo "99. Exit " 
    echo "------------------------"
    echo "-- $lastmessage "
    echo "--The last ten logs:"
    tail $RequestFolder/logs.txt
	echo "---------------------------"
}
     test_options(){
	local choose
	read -p "Enter choice [ 1 - 99] " choose
	case $choose in
		1)   CaptureSQLheader && CaptureSQLdata && RequestCapture && EditCaptureForm && FilterTextNumberOrQuery ;;
		2)    ;;
		3)   NewProject ;;
		4)   LoadProject ;;
		5)   CaptureSQLheader ;;
        6)   CaptureSQLdata ;;
        7)   SoapData ;;
        8)   FilterSoap ;;
		9)    ;;
		10)  RequestCapture ;;
        11)  EditCaptureForm ;;
		12)  NumericalQuery && TextQuery ;;
        13)  RequestCapture && EditCaptureForm && FilterTextNumberOrQuery ;;
        14)  ReturnedSoap ;;
        15)   ;;
		16)  SelectorMenu ;;
		20)  Filter1Numerical && Filter2Between && Filter3Not && Filter4Or && Filter5Neither && Filter6Match ;;
		21)  Filter1Numerical ;;
        22)  Filter2Between ;;
        23)  Filter3Not ;;
        24)  Filter4Or ;;
        25)  Filter5Neither ;;
        26)  Filter6Match ;;
        27)  ReviewCaptureForm ;;
        30)  Developing ;;
        31)  Developing2 ;;
        40)  Duplicates ;;
        63)  more $RequestFolder/logs.txt ;;
		99)  clear && exit ;;
		*) echo -e "${RED} $choose is not a displayed option.....${STD}" && sleep 2
        
	esac
}

while true
do
	test_menu
	test_options
done
}   

# required to start the menu
TestOptions
# NewProject
# LoadProject
# RequestCapture
# CaptureSQLheader

# Filter1Numerical
# Filter2Between 
# Filter3Not 
# Filter4Or
# Filter5Neither 
# Filter6Match 

# SoapData