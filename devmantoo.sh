#!/bin/bash

# V ------------------ Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'
DateTime=$(date)
RunB4=$(egrep -iw "alias dmt='~/development-management-tool/devmantoo.sh'" ~/.bash_aliases)

# -------------------------- Setup -------------------

RunCheck(){
# Validates if this is a fresh install or not.
# Looks for dmt0.txt and creates a shortcut in ~/.bash_aliases if it does not exists.
if [ $RunB4=="" ]
	then
# Add dmt as a CLI shortcut in bash_aliases	
 	echo -e "alias dmt='~/development-management-tool/devmantoo.sh' # $DateTime" >> ~/.bash_aliases
# Activate file changes by sourcing	
	source ~/.bash_aliases
 	lastmessage="Created $RunB4 in .bash_aliases.  "
	else
	lastmessage="Welcome back $USER "
#	MainMenu
	fi
}

CreateMarker(){
# Creates a marker file in the DMT folder. 
if [ -f !"~/development-management-tool/dmt0.txt" ]
	then
#Create the marker (dmt0.txt) file.	
	touch ~/development-management-tool/dmt0.txt
# Copy install time to the marker.	
	echo "Installed and run on $DateTime" >> ~/development-management-tool/dmt0.txt

	lastmessage="Created a shortcut in .bash_aliases"
	else 
	lastmessage="Proceed $USER"
	fi
}

GitBranch(){
	cd ~/development-management-tool/
	BranchName=$(git branch)
	cd ..
	echo "Working on the Git Branch: $BranchName "
}

DMTupdate(){
	cd ~/development-management-tool/
	echo "Checking for an update"
    git pull
    cd ..
	echo "Quit this menu to apply any updates"
}

DMTcommit(){
	cd ~/development-management-tool/
	read -rp "Enter your commit message: " GitMessage
    git add . && git commit -m " $GitMessage "
    git push git@bitbucket.org:darboo/development-management-tool.git
    cd ..
}


# 0 ----------------------- Main Menu -----------------------

MainMenu(){
#clear
MenuTitle=" - Development Management Tools - "
Description=" Menu for software development, environment creation and managment. "

# Display menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "
GitBranch 
	echo "-----------------------------------"
	echo "0.  Update Development Management Tool "
	echo "1.  System configuration, maintenance & management. "
	echo "2.  LXD			Container Virtualisation "
#	echo "3.  GIT			Version control options"
	echo "4.  Pod Manager 		Podman Menu"
	echo "5.  Microk8s		Kuberenetes Options"
	echo "6.  KVM/UVT		Virtual Machine tools "
	echo "7.  Docker		More Container Virtualisation " 
#    echo "8.  Testing		Test web & API services "
    echo "9.  RoR Maker		Menu to create Rails apps "
	echo "10. Ansible"
	echo "Q.  Quit			Exit this Menu"

#   echo "ACP                      Shortcut to git Add, Commit & Push"
	echo " "
	echo " $lastmessage " 
	echo " " 
}

read_options(){
	local choice
	read -p "Enter the desired item number: " choice
	case $choice in

    0)  clear && DMTupdate ;;
	1)  clear && ~/development-management-tool/adminmenu.sh ;;
	2)  clear && ~/development-management-tool/lxdmenu.sh ;;
	3)  clear && ~/development-management-tool/gitmenu.sh ;;
	4)  clear && ~/development-management-tool/Podmanager.sh ;;
	5)  clear && ~/development-management-tool/kubes.sh ;;
	6)  clear && ~/development-management-tool/uvtmenu.sh ;;
	7)  clear && ~/development-management-tool/dockermenu.sh ;;
	8)  clear && ~/development-management-tool/testmenu.sh;;
    9)  clear && ~/development-management-tool/railsmenumaker.sh ;;
    10) clear && ~/development-management-tool/AnsibleMenu.sh ;;
	ACP)  DMTcommit ;;
    q|Q) clear && echo " See you later $USER! " && exit 0;;
#	fackEnterKey) echo -e "${RED} Please choose an option then hit enter.${STD}" && sleep 2
	*) echo -e "${RED} $choice is not a displayed option.....${STD}" && sleep 2 && clear
	esac
}

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

#Start the Main Menu
DMTupdate
RunCheck
MainMenu

