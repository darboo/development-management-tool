#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

REPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"

clear

SAI="sudo apt install -y "

# clear the screen exit the sub menu
quitsub(){
	clear && MainMenu
}

# Present_containers=()

default_containers=( NGINX GOGS MARIADB ROR 3DFX AUDIO )

# 7 ----------------------------- Ansible Menu -----------------------------


# 6 ----------------------------- UVT-KVM -----------------------------

# 1 ----------------- UVT-KVM INSTALL --------------------

installKVM(){
# check the CPU supports virtualization
cat /proc/cpuinfo

$SAI qemu-kvm #- The main package
$SAI uuid # Generate UUID's
$SAI libvirt0 #- Includes the libvirtd server exporting the virtualization support
$SAI libvirt-bin libvirt-doc #- contains virsh 
$SAI virtinst #- Utility to install virtual machines
$SAI virt-viewer #- Utility to display graphical console for a virtual machine
$SAI libosinfo-bin #VM OS Support
# the following is an easy way to create VM's
$SAI uvtool # To download cloud images
ssh-keygen # create a key to login to containers securely
uvt-simplestreams-libvirt --verbose sync release=xenial arch=amd64 #To just update/grab Ubuntu 16.04 LTS (xenial/amd64) image
# Source: https://www.cyberciti.biz/faq/how-to-use-kvm-cloud-images-on-ubuntu-linux/
# systemctl status libvirt-bin

# ensure the needed kernel modules have been loaded
sudo lsmod | grep kvm

# load the modules if not already
sudo modprobe kvm_intel/kvm_amd

# start the libvirtd daemon at boot time and immediately
sudo systemctl enable --now libvirtd
# Require user action
saytouser="Please Logout & Login or Restart this host"
}

# Convert IMG's to ISO's
IMG2ISO(){
	$SAI ccd2iso
}


# 4 ---------------- KVM USER INPUT --------------------

nameVM(){
    read -rp "Enter the name of the target VM: " VMname
}

# captures user data to create a customized VM
customVM(){
# --------- Custom VM data capture -----------
    read -rp "Enter the name of the target VM: " VMname
    read -rp "Enter the number of Virtual CPU's: " VCPUs
    read -rp "Enter the amount of Virtual RAM required: " VRAM
    read -rp "Enter the size of the virtual HDD: " VHDD
#    read -rp "Enter the OS release version: " OStype
#    read -rp "Enter the required architecture: " ArchType
    read -rp "Enter the SSH key path or blank for default" SSHKEYPATH
#    read -rp "Enter the software to be installed comma space seperated values: " SUAPIN  #-or read a pre made list

# If keypath not set use default location
if [ -z "$SSHKEYPATH" ]; then SSHKEYPATH=/home/$USER/.ssh/id_rsa.pub;
	else 
	echo " looking in $SSHKEYPATH "
	fi

# require user validation of inputed data
cVMarray=("$VMname" "$VCPUs" "$VRAM" "$VHDD" "$OStype" "$ArchType" "$SSHKEYPATH" "$SUAPIN" )
# for loop that iterates over each element in cVMarray
for i in "${cVMarray[@]}"
do
    echo $i
done 
	read -rp "Happy with your choices? hit y to continue or any key to re-enter data." Happy

if [ "$Happy" != "y" ]; then customVM
	else
	echo "Then lets continue "
	
fi
# update chosen release and arhitecture
OSpoolupdate
# put the above data into the below function
	uvt-kvm create $VMname --cpu $VCPUs --memory $VRAM --disk $VHDD --ssh-public-key-file $SSHKEYPATH # --release $OStype --arch $ArchType --packages $SUAPIN
}


# 5 ---------------- UVT-KVM USE --------------------

getVMOS(){
	read -rp "Enter the OS release version: " OStype
    	read -rp "Enter the required architecture: " ArchType
	OSpoolupdate
}
OSpoolupdate(){
	uvt-simplestreams-libvirt --verbose sync release=$OStype arch=$ArchType
}

ImageUpdate(){ 
	uvt-simplestreams-libvirt --verbose sync release=bionic arch=amd64
}
AddSource(){
	uvt-simplestreams-libvirt --verbose sync --source http://cloud-images.ubuntu.com/daily release=bionic arch=amd64
}

# Display downloaded local OS list
OSImages(){
	uvt-simplestreams-libvirt query
}

SSHinto(){
       # uvt-kvm ssh $VMname
        xterm -e "uvt-kvm ssh $VMname"

}
VMip(){
	uvt-kvm ip $VMname
}

basicVM(){
# UVTool command to create a virtual machine
	uvt-kvm create $VMname-basic release=xenial arch=amd64
sleep 3
	uvt-kvm ssh $VMname-basic
}

minimalVM(){
    sudo virt-install --name $VMname --memory 512 --vcpus 1 --disk size=20 --cdrom $UBSE_ISO 
}

M16VM(){
    sudo virt-install --name $VMname --memory 512 --vcpus 1 --disk size=10 --import
#    nohup sudo virt-install --name $VMname --memory 512 --vcpus 1 --disk size=20 --cdrom $UM16_ISO &>/dev/null &
}
#List VM's

detailVM=( "--all" "--transient" "--inactive" "--persistent" "--with-snapshot" "--without-snapshot" "--state-running" "--state-paused" "--state-shutoff" "--state-other" "--autostart" "--no-autostart" "--with-managed-save" "--without-managed-save" "--managed-save" )
# List all VM's
listVMs(){
    virsh list ${detailVM[0]} 
}
showtransientVMs(){
    echo " Transient Virtual Machines "
    virsh list ${detailVM[1]}
}
# List all types of VM

# require an if else do for this bit
autostartVM(){
    virsh autostart $VMname
#    virsh autostart --disable $VMname

}
startVM(){
    virsh start $VMname
}

editVM(){
    virsh edit $VMname
}
rebootVM(){
    virsh reboot $VMname
}
# this stops a VM
destroyVM(){
    virsh destroy $VMname --graceful
}
startVM(){
    virsh start $VMname
}
pauseVM(){
    virsh suspend $VMname
}
resumeVM(){
    virsh resume $VMname
}
# delete a vm
deleteVM(){
    virsh undefine --domain $VMname
    destroyVM
    showtransientVMs
}
cloneVM(){
    clonedVM="cloned-"$VMname
    sudo virt-clone --original $VMname --name $clonedVM --file /var/lib/libvirt/images/$clonedVM.qcow2
}
UVTKVMOptions(){

uvt_kvm_menu(){
    MenuTitle=" - Virtualisation Options - "
    Description=" Virtual Machine software utilising KVM & UVTools to build and manage virtualised environments. "

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"	
	echo " $saytouser "
	echo "1.  Display the downloaded Virtualisation OS list "
	echo "2.  Create a basic ubuntu server	ubuntu16.04 1VCPU, 512Mb RAM & 8Gb Storage"
	echo "3.  Create a customised VM		Interactive VM creation"
	echo "4.  Edit a VM "
	echo "5.  Reboot a VM "
	echo "6.  Pause a VM "
	echo "7.  Resume a VM "
	echo "8.  Stop a VM "
	echo "9.  Start a VM "
#	echo "10. Clone a VM * almost there *"
	echo "11. Set a VM to Autostart "
	echo "12. Delete a VM "
	echo "13. Display transient VM's "
#	echo "14  Save a VM "
#	echo "15  Create a Snapshot "
#	echo "16  Create a VM Template"
#	echo "17  Create a VM from a Template"
	echo "18  Login to a VM. "
	echo "19. Add an OS image to download and Virtualise "
	echo "20. View the IP address of a VM "
	echo "99. Return to Main Menu "
    echo " =========================== "
	echo "Existing Virtual Machines:" && listVMs
}

uvt_kvm_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) OSImages ;;
		2) nameVM && basicVM ;;
		3) customVM ;;
		4) nameVM && editVM ;;
		5) nameVM && rebootVM ;;
		6) nameVM && pauseVM ;;
		7) nameVM && resumeVM ;;
                8) nameVM && destroyVM ;;
		9) nameVM && startVM ;;
		10) nameVM && pauseVM && cloneVM ;;
		11) nameVM && autostartVM ;;
		12) nameVM && deleteVM ;;
		13) clear && showtransientVMs ;;
		18) nameVM && SSHinto ;;
		19) nameVM && getVMOS ;;
		20) nameVM && VMip ;;
		99) quitsub ;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

while true
do
	uvt_kvm_menu
	uvt_kvm_options
done

}
# 5 ------------------------------ Server Menu -----------------------------

servermenu(){

server_menus(){

MenuTitle=" Server Options "
MenuDescription=" Perform maintainance and other usefull server commands "

	echo " "	
	echo " $MenuTitle " 
	echo " $MenuDescription " 
	echo " -------------------------------- "
	echo "10.  Update, Upgrade and Autoremove "
	echo "20.  Midnight Commander "
	echo "21.  TaskSel "
	echo "30.  "
	echo "40.  "
	echo "50.  "
	echo "60.  Login to a Server with SSH "
        echo "70    "
	echo "80.  Reboot Server"
	echo "81.  Shutdown Server"
	echo "90.   "
	echo "99. Return to Main Menu "
	echo
	echo "Download/Clone: $DMT "
}

server_options(){

local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		10) SWupdate && SWupgrade && SWautoremove ;;
		20) mc ;;
		21) runtasksel ;;
		30)  ;;
		40)  ;;
		50)  ;;
		60) loginssh ;;
                70)  ;;
                80) sudo reboot ;;
		81) sudo shutdown now ;;
		99) quitsub ;;
		*) echo -e "${RED} You can't do that..... ${STD}" && sleep 2
	esac

}
    while true
    do
	server_menus
	server_options
    done
}

# 4 ************************ Snap menu *******************************

snapinsta(){
	read -rp "Enter the Snap to install: " instsnap
	sudo snap install $instsnap
}
snapquery(){
	read -rp "Search the SnapStore for: " findsnap
	snap find $findsnap
}
snapupd(){
	read -rp "Enter the Snap to update or leave empty for all: " updsnap
	snap refresh $updsnap
}
snaprem(){
	read -rp "Enter the Snap to remove: " remsnap
	sudo snap remove $remsnap
}
snapdet(){
	read -rp "Display Details about which Snap? " detsnap
	snap info $detsnap
}
snapmen() {
snap_menu() {

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " SNAP - MENU"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. install a Snap Package"
	echo "2. Search for a Snap"
	echo "3. Update one or all Snaps"
	echo "4. Remove a Snap"
	echo "5. list Snaps "
	echo "6. Snap Details"
	echo "7.  "
	echo "99. Return to Main Menu "
}

snap_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) snapinsta ;;
		2) snapquery ;;
		3) snapupd ;;
		4) clear && snap list && snaprem && snap list;;
		5) clear && snap list ;;
		6) clear && snap list && snapdet ;;
		9)  ;;
		99) quitsub ;;
		*) echo -e "${RED} $choice is not available today.${STD}" && sleep 2
	esac
}

while true
do
	snap_menu
	snap_options
done
}

# 3 -------------------- git menu -------------------------


file2git(){
	read -rp "Enter the name of the file or,' . 'for all files: " gitfile 
}
all2git(){
	gitfile="."
}
gitpull(){
	git pull
}
gitadd(){
	git add $gitfile
}
gitcommit(){
	git commit 
}
gitpush(){
	git push 
}
ggyes(){
# this switch makes these details global (not implemented)
	gitglo="--global "
}
gitUserConfig(){
	read -rp "Enter your email: " gitmail
	git config $gitglo user.email "$gitmail"
	read -rp "Enter your username: " gituser
	git config $gitglo user.name "$gituser"
}
newgit(){
	read -rp "Enter the name of your new repository " GitRepo
	read -rp "Enter the path for your new repository or leave empty to place it in your Documents " NewDir
	if [ -z "$NewDir" ]; then NewDir=/home/$USER/Documents;
	else 
	echo " Creating in $NewDir "
	fi
	PathFile=$NewDir/$GitRepo
	echo $PathFile
	git init $PathFile
}
DocsFolder(){
	NewDir=/home/$USER/Documents
}

gitmenu(){
git_menu() {

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " GIT - MENU"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1.  Update the local repository where DMT resides "
	echo "2.  Add files to commit within the repository. "
	echo "3.  Commit you desired changees "
	echo "4.  Push commited changes to a remote repository "
	echo "5.  Add All Files & Commit "
	echo "6.  Add All Files, Commit & Push "
	echo "7.  User Configuration, identify yourself to Git "
	echo "8.  Create and intialise a new repository in a location of your choice " 
	echo "99. Return to Main Menu " 
    echo "------------------------"
    echo " $lastmessage " 
    echo " "
	echo "------------------------"

}

git_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) gitpull ;;
		2) file2git && gitadd ;;
		3) gitcommit ;;
		4) gitpush ;;
		5) all2git && gitadd && gitcommit ;;
		6) all2git && gitadd && gitcommit && gitpush ;;
		7) clear && gitUserConfig ;;
		8) newgit ;;
		99) quitsub ;;
		*) echo -e "${RED} where did you see $choice? ${STD}" && sleep 2
	esac
}

while true
do
	git_menu
	git_options
done
}

# 2 -------------------- LXD Menu ---------------------

# 2a *************** install LXD and its requirements *******************************
LXDsetup(){
# Update, upgrade and autoremove
	sudo apt update
        sudo apt upgrade -y
	sudo apt autoremove -y
# Install lxd:
	$SAI lxd
# Install ZFS
	$SAI zfsutils-linux
# install additional LXC tools
	$SAI lxc-utils
# Install Midnight Commander
	$SAI mc
# Set git push default to stop that message
  git config --global push.default simple
# Initialise LXD (must make a config file for this......) 
	echo "**"
	echo "Starting the interactive configuration,"
        echo "please select the default options:"
	echo "**"
	sudo lxd init 
# Add the current user to the LXD Group:
	sudo usermod --append --groups lxd $USER

# notification message to display on the menu screen
	lastmessage="---LXD/LXC, and Midnight commander installed, 
	please RESTART YOUR MACHINE before you create containers.---"
	
	defaultLXDconfig="config: {}/home/ubuntu/.ssh/id_rsa.pub
networks:
- config:
    ipv4.address: auto
    ipv6.address: none
  description: ""
  managed: false
  name: lxdbr0
  type: ""
storage_pools:
- config:
    size: 17GB
  description: ""
  name: default
  driver: zfs
profiles:
- config: {}
  description: ""
  devices:
    eth0:
      name: eth0
      nictype: bridged
      parent: lxdbr0
      type: nic
    root:
      path: /
      pool: default
      type: disk
  name: default
cluster: null
"

}

# 2b ********************* Creating Containers *******************

# Ephemeral container
Ephemcont(){
	read -rp "The name of this Ephemeral Container is: " newcon
	lxc launch -e ubuntu:16.04 $newcon
	contip=$( lxc list $newcon --format csv -c 4 ) 
	wait4ip
	updupgre
	logubu
	lastmessage=" The Epheremal container $newcon be ready ."
}

#Create a basic Ubuntu16.04 container
makecon(){
	contname
	lxc launch ubuntu:16.04 $newcon
	wait4ip
	updupgre
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "ssh-keygen"
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"
	lastmessage="The basic $newcon container is now ready for use. "
	logubu
}


#Update, upgrade and autoremove
updupgre(){
	lxc exec $newcon -- apt update
        lxc exec $newcon -- apt upgrade -y
	lxc exec $newcon -- apt autoremove -y
}
dpkgfix(){
	lxc exec $newcon -- dpkg --configure -a
}
# Create an nginx container 
makenginx(){
	contname
	lxc launch ubuntu:16.04 $newcon
	wait4ip
	updupgre
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "ssh-keygen"
	lxc exec $newcon -- apt install nginx -y
	lastmessage="The NGINX container $newcon at $contip is now ready for use"
}

# Create a Gogs container 
makeGogs(){
	contname
	lxc launch ubuntu:16.04 $newcon
	wait4ip
	updupgre
	lxc exec $newcon -- sudo snap install gogsgit
	lastmessage="The Gogs container $newcon at $contip is now ready for use"
}

# Create a Wordpress container 
makeWP(){
	contname
	lxc launch ubuntu:16.04 $newcon
	waiting
	updupgre
#	lxc exec $newcon -- $SAI 
	lastmessage="The Wordpress container $newcon at $contip is now ready for use"
}

# Create a ROR container 
makeROR(){
	contname
	lxc launch ubuntu:16.04 $newcon
	wait4ip
	updupgre
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "ssh-keygen"
	RORinstall
}

RORinstall(){
# import required keys and download ROR
	lxc exec $newcon -- sudo --login --user ubuntu sh -c "gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB && cd /tmp && curl -sSL https://get.rvm.io -o rvm.sh && cat /tmp/rvm.sh | bash -s stable --rails" 
# Prepare Ruby for use by users ubuntu & root
	lxc exec $newcon -- sh -c "echo "source /home/ubuntu/.rvm/scripts/rvm" >>~/.profile && source /home/ubuntu/.rvm/scripts/rvm && usermod --append --groups rvm ubuntu && usermod --append --groups rvm root"

	lastmessage="Open a browser to the new $newcon container at $contip:3000 on your host machine to see the rails info screen."
}

RailsDemo(){
 lxc exec $newcon -- sudo --login --user ubuntu sh -c "rails new lxdrailsdemo && cd lxdrailsdemo && rails s -d"
}

# examine file:	less /tmp/rvm.sh


# 2c ******************** Container Commands ******************
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}
#New Container name
contname(){
	read -rp "Enter the name of the target Container: " newcon
}
#List available containers
status(){
	lxc list --format csv -c ncs46tpaSP
}

#Display the contents of the Containers log file
conlog(){
	lxc info $newcon --show-log 
}

# Start Container(s)
start(){
	lxc start $newcon
	wait 2
	lastmessage=" $newcon has started. "
}
# Stop Container(s)
stop(){
	lxc stop $newcon
	lastmessage="The Container $newcon has stopped. "
}
# Restart Container(s)
resta(){
	lxc restart $newcon
	lastmessage=" $newcon has been restarted. "
}
# Delete a Container
delcon(){
	lxc stop -v $newcon 
	echo "Destroying the $newcon Container......"
	sleep 4
	lxc delete $newcon -v
	lastmessage="the Container $newcon is no longer with us... "
	clear
}
# *********************** Login ***********************

# Login to a Container as root
loginroot(){
	status
	"lxc exec $newcon -- /bin/bash"
}
# Login as the user Ubuntu (used after scripted install)
logubu(){
	status
	lxc exec $newcon  -- sudo --login --user ubuntu
}
# Login to a Container via xterm as root
loginrootinaterminal(){
	status
	lxterm -e "lxc exec $newcon -- /bin/bash" & disown
}
# Login via xterm as the user Ubuntu
logubuinaterminal(){
	status
	lxterm -e "lxc exec $newcon  -- sudo --login --user ubuntu" & disown -h
}

# 2d ************ Install Authorized Software *************

#Install Midnight Commander within a conatiner
insmcinacon(){
	lxc exec $newcon -- $SAI mc
}

# Display details of a container
contdetails(){
	contname
	lxc config show --expanded $newcon
}
# shutdown all containers
shtdwn(){
	lxd shutdown
	echo " All Containers Stopped "
}

# This should check when an ip address has been assigned to the container before continuing with the script.
wait4ip(){
# loop to handle when an IP address is provided.
findip4(){
	contip=$( lxc list $newcon --format csv -c 4 ) 
	if [ "$contip" == "" ]; then echo " Waiting for an IP address.... $contip " && sleep 1
	else
	echo "$contip" 
	sleep 1
	fi
}

while [ "$contip" == "" ]
do
	findip4
done
}

# 2e --------------------start and stop containers----------------------

# Menu for controlling container activity
stasto(){
	MenuTitle=" Start & Stop containers "
	Description="Common container commands"
# Display Options
startstop_menu(){

	echo ""	
	echo " $MenuTitle "
	echo " $Description "
	echo " $lastmessage " 
	echo "-----------------------------------"
	echo "1.  Start a Container."
#	echo "3.  Start all Containers."
	echo "5.  Stop a Container."
	echo "6.  Stop all Containers.		-Shutdown LXD"
	echo "7.  Restart a Container."
#	echo "8.  Restart all Containers."
	echo "9.  Delete a Container."
	echo "90. Return to Previous Menu"
	echo "99. Return to Main Menu "
	echo " "

	echo " "
	status
}
# Command to execute
startstop_options(){
	local startstop_choice
	read -p "Enter choice [ 1 - 99] " startstop_choice
	case $startstop_choice in
		1) contname && start ;;
		3)  ;;
		5) status && contname && stop ;;
		6) shtdwn ;;
		7) status && contname && resta ;;
		9) status && contname && delcon ;;
		90) LXDmenu ;;
		99) quitsub ;;
	*) echo -e "${RED}I have no idea what $startstop_choice means.....${STD}" && sleep 3
	esac
}


while true
do
	startstop_menu
	startstop_options
done
}
# 2f -------------------- LXD - menu --------------------

# set and display relevant menu information 
LXDmenu(){
	MenuTitle=" LXD - Options"
	Description=" Tools to manage Containers "
	EXISCON="lxc list --format csv -c ns4" # Display existing containers

LXD_menu(){

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo " $lastmessage " 
	echo "-----------------------------------"
	echo "10. "
	echo "20. List all containers. "
        echo "30. Create a basic Container and Login as ubuntu. "
	echo "40. Create an NGINX container."
	echo "41. Create a Gogs Git container "
	echo "42. Create a Ruby On Rails container "
	echo "43. Install Ruby On Rails & NodeJS " 
	echo "50. Login to a Container as Root."
	echo "51. Login to a Container as ubuntu. "
	echo "52. Login to a Container via xterm as Root."
	echo "53. Login to a Container via xterm as ubuntu. "
	echo "60. Start/Stop Containers. "
        echo "70. Create an Ephemeral container and Login as ubuntu. "
        echo "80. Detailed container information. "
	echo "90. Delete a Container. "
	echo "99. Return to Main Menu "
	echo "-------------------------------" 
	echo " Existing Containers: " 
jobs -l
$EXISCON
}

LXD_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		10)  ;;
		20) clear && status ;;
		30) makecon && wait4ip && updupgre && insmc && contname && logubu ;;
		40) makenginx ;;
		41) makeGogs ;;
		42) makeROR ;;
		43) contname && RORinstal ;;
		50) contname && loginroot ;;
		51) contname && logubu ;;
		52) contname && loginrootinaterminal ;;
		53) contname && logubuinaterminal ;;
		60) stasto ;;
                70) Ephemcont && logubu ;;
                80) clear && contdetails ;;
		90) contname && delcon ;;
		99) quitsub ;;
		*) echo -e "${RED}Error...${STD}" && sleep 3
	esac
}

while true
do
	LXD_menu
	LXD_options
done
}


# 1 -------------------- Admin Menu --------------------

setupansible(){
    SWupdate
    SWupgrade
    $SAI software-properties-common
    sudo apt-add-repository ppa:ansible/ansible -y   
    SWupdate
    $SAI ansible
	$SAI make
    SWautoremove
    cp -R /etc/ansible /home/$USER/ansible
	ssh-keygen # create a key to login to containers securely
#    cd /home/$USER/ansible
	ansible --version
    lastmessage="- installed $AnsibleVersion -" 
}

# Update 
SWupdate(){
	sudo apt update
	lastmessage=" OS Updated "
}
# upgrade 
SWupgrade(){
    sudo apt upgrade -y
    lastmessage=" OS Upgraded " 
}
# autoremove
SWautoremove(){
    sudo apt autoremove -y
    lastmessage=" Removed unecassary software "
}

# fix Broken Packages
dpkgfix(){
	sudo dpkg --configure -a
	lastmessage=" Package Manager Repaired "
}
instasksel(){
	$SAI tasksel tasksel-data
	sudo apt-add-repository ppa:yannubuntu/boot-repair -y
	SWupdate
	$SAI boot-repair
	lastmessage="TaskSel & Boot-Repair installed"
}
#Install Midnight Commander
insmc(){
	$SAI mc 
	lastmessage=" Midnight Commander Installed " 
}

# Install ZFS
InstallZFS(){
	$SAI zfsutils-linux
	lastmessage=" ZFS ready "
}

# SSH Setup
setupssh(){
	read -p " Provide USER@SERVER details for target SSH machine " sshtarget
	ssh keygen
	ssh-copy-id $sshtarget
}
# SSH Login
loginssh(){
	read -p " Provide USER@SERVER details for target SSH machine " namessh
	ssh $namessh
	lastmessage=" Logged into $namessh "
}
#
BootFix(){
	boot-repair
}

runtasksel(){
	sudo tasksel
}
adminmenu(){

MenuTitle=" Admin Menu "
Description=" System administration and management tools "

        admin_menu(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo ""
	echo "1.  Install ZFS"
	echo "2.  Install Ansible "
	echo "3.  Install Midnight Commander "
	echo "4.  Update the Host machine "
	echo "5.  Upgrade the Host machine "
	echo "6.  Cleanup (autoremove) "
    	echo "7.  Update, Upgrade & Autoremove "
	echo "8.  Install UVTool & KVM "
	echo "9.  Fix Broken Packages "
	echo "10. SSH Setup"
	echo "11. SSH Login"
	echo "12. Boot Repair "
	echo "20. Install Tasksel & Boot Repair "
	echo "21. install LXD"
	echo "99. Return to Main Menu " 
    echo "------------------------"
    echo " $lastmessage"
	echo "------------------------"

}
     admin_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1) InstallZFS ;;
		2) setupansible ;;
		3) insmc ;;
		4) SWupdate ;;
		5) SWuppgrade ;;
		6) SWautoremove ;;
       		7) SWupdate && SWupgrade && SWautoremove ;;
		8) installKVM ;;
		9) dpkgfix ;;
		10) setupssh ;;
		11) loginssh ;;
		12) BootFix ;;
		20) instasksel ;;
		21) LXDsetup ;;
		99) clear && quitsub ;;
		*) echo -e "${RED} $choice is not a displayed option.....${STD}" && sleep 2
	esac
}

while true
do
	admin_menu
	admin_options
done
}   
    
# 0 ----------------------- Main Menu -----------------------

MainMenu(){
MenuTitle=" - Development Management Tools - "
Description=" A simple BASH menu for software development, deployment and managment. "

# Display menu options
show_menus() {

	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo ""
	echo "1.  Admin Tools."
	echo "2.  LXD Virtualisation "
	echo "3.  GIT "
	echo "4.  Snap "
	echo "5.  Server maintenance & management. "
	echo "6.  KVM Virtualistaion "
	echo "0.  Readme " 
	echo "10. Exit "
	echo " "
	echo " $lastmessage " 
	echo "  " 
	
}

read_options(){
	local choice
	read -p "Enter the desired item number: " choice
	case $choice in
		1) clear && adminmenu ;;
		2) clear && LXDmenu ;;
		3) clear && gitmenu ;;
		4) clear && snapmen ;;
		5) clear && servermenu ;;
		6) clear && UVTKVMOptions ;;
		0) nano ./README.md ;;
        10) clear && echo " See Ya! " && exit 0;;
		*) echo -e "${RED} $choice is not a displayed option.....${STD}" && sleep 3
	esac
}
 

# - Trap CTRL+C, CTRL+Z and quit singles disabled while developing -
#trap '' SIGINT SIGQUIT SIGTSTP

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

#Start the Main Menu
MainMenu

