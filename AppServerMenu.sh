#!/bin/bash  

# ============ Environment Variables ============= 

# Options to change the printed text colour
STD='\033[0;0;39m'
RED='\033[0;41;30m'
# shortcut for Sudo Apt Install -yes
SAI="sudo apt install -y "
REPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"

# =================== Modules =====================
# Add your desired programme modules

PrintUsername(){
# Change text colour and prints the current user variable.
	echo -e "${RED} Current user is: ${STD}" $USER
}

DateTime(){
# create a variable which defines and executes the desired command (date).
	DT=$(date)
# Change text colour and display contents of the DT variable.
	echo -e "${RED} Today is: ${STD}" $DT
}

# =========== Prep Application Server ============

InstallXrdp(){
# Install xRDP which allows applications to be executed over SSH
# update the host machine before installing software.
	sudo apt update
	$SAI xrdp
	$SAI mesa-utils
	$SAI xinit
# Restart server after install
    sudo reboot
}

InstallRemoteDesktop(){
# required to correct remote desktop via windows10.
	$SAI xserver-xorg-input-all
	$SAI xserver-xorg-core
	$SAI xorgxrdp
}

InstallApps(){
# Install desired apllications for development environment
# WebBrowser
    $SAI firefox
# Terminal
    $SAI xterm
# File Browser
    $SAI nemo
# IDE
    $SAI kate
# Install additional software
    $SAI libcanberra-gtk3-module
# Install firefox drivers for selenium. As a side-effect we get chromium too.    
#    $SAI firefox-geckodriver firefoxdriver
}

SetupDocker(){
# Install Docker CE
## Set up the repository:
### Install packages to allow apt to use a repository over HTTPS
	sudo apt update
	$SAI apt-transport-https ca-certificates curl software-properties-common
	$SAI docker.io docker-compose
### Add Docker’s official GPG key
#sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

### Add Docker apt repository.
# sudo add-apt-repository \
# "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
#  $(lsb_release -cs) \
#  stable"

## Install Docker CE.

# sudo apt install -y containerd.io=1.2.10-3 
# sudo apt install -y docker-ce=5:19.03.4~3-0~ubuntu-$(lsb_release -cs) 
# sudo apt install -y docker-ce-cli=5:19.03.4~3-0~ubuntu-$(lsb_release -cs) 
# sudo apt install -y docker-compose

# Setup daemon.
sudo cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo usermod -aG docker ${USER}

# Restart docker.
	sudo systemctl daemon-reload
	sudo systemctl restart docker

	lastmessage="Please log out for group changes to take effect!"
}

RunEmAll(){
# Run all installed applications
	xterm & disown
	kate & disown 
	nemo & disown 
	firefox & disown 
	
}

# ================== Main Menu ====================

MainMenu(){
# Clears the screen
	clear
# Define this menus title.	
	MenuTitle=" - App Server Menu - "
# Describe what this menu does
	Description=" Cloud environment for developing applications  "

# Display the menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "  
	echo "-----------------------------------"
	echo "0.  "
	echo "1.  Install Remote Application Server."
	echo "2.  Install Xterm, Firefox, chromium, webdrivers, Kate and Nemo."
	echo "3.  Install Docker for a Dev enviroment."
	echo "4.  Install Remote Desktop for win10"
	echo "5.  "
	echo "6.  "
	echo "6.  "
	echo "7.  " 
    echo "8.  "
    echo "9.  "
	echo "10. Open Xterm in a remote window on your device"
	echo "11. Open Kate in a remote window on your device"
	echo "12. Open Nemo in a remote window on your device"
	echo "13. Open Firerfox in a remote window on your device"
#	echo "14. Open Chromium in a remote window on your device (works okay but terminal complains...)"
	echo "15. Open all of the above on your device"
	echo "20. "
	echo "99.  Quit				   Exit this Menu"
	echo " " # Show ServerIP, 
	echo " $lastmessage " 
	jobs -l
	echo " " 
}

read_options(){
# Maps the displayed options to command modules	
	local choice
# Inform user how to proceed and capture input.	
	read -p "Enter the desired item number or command: " choice
# Execute selected command modules	
	case $choice in
        0)    ;;
		1)   InstallXrdp ;;
		2)   InstallApps ;;
		3)   SetupDocker ;;
		4)   InstallRemoteDesktop ;;
		5)   ;;
		6)   ;;
		7)   ;;
		8)   ;;
        9)   ;;
        10)  xterm & disown -h ;;
		11)  kate & disown -h ;;
		12)  nemo & disown -h ;;
		13)  firefox & disown -h ;;
#		14)  chromium-browser & disown -h ;;
		15)  RunEmAll ;;
# Quit this menu
        99) clear && echo " See you later $USER! " && exit 0;;
# Capture non listed inputs and send to BASH.
		*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

# =================== Run Commands ===============
# commands to run in this file at start.
MainMenu

