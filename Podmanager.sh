#!/bin/bash  

# ================ Menu Template =================
# A template with functional examples for creating BaSH menus.
# Save (or copy and paste) this script as your starting point for your file.


# ============ Environment Variables ============= 

# Options to change the printed text colour
STD='\033[0;0;39m'
RED='\033[0;41;30m'

# =================== Modules =====================

Setup(){

	sudo apt install podman containers-storage podman-docker
	sudo systemctl enable --now podman.socket
	curl -H "Content-Type: application/json" --unix-socket /var/run/podman.sock http://localhost/_ping
	
}

CreateNewProject(){
# New Project name
	read -rp "Enter the name of the new Project: " newproject
	if [[ "$newproject" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo "$newproject"
	fi
# Set the namespace to equal the project name.
	NameSpace="$newproject"
	mkdir $HOME/mk8sprojects/$newproject
	cd $HOME/mk8sprojects/$newproject
}

LoadProject(){
# Display existing projects in the folder
	ls $HOME/mk8sprojects
# Load Project 
	read -rp "Enter the name of the desired Project: " loadproject
	if [[ "$loadproject" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo "$loadproject"
	cd $HOME/mk8sprojects/$loadproject
	fi
	
# Set the namespace to equal the project name.
#	NameSpace="$loadproject"
	lastmessage="Current Project: $loadproject"
}

NamePod(){
	sudo podman pod list
# Capture and validate the new desired Container Name
    read -rp "Enter the Desired name: " PodName
    if [ $PodName == "" ];
    then
# Complain about the lack of image name and stop
    echo "Please enter a container name...."   
    else
# Display the selected image.
    echo "You Entered: "$PodName   
    fi   
}  

ChooseImage(){

#	cd $HOME/mk8sprojects/$loadproject/package
	ls $HOME/mk8sprojects/$loadproject/package
# Capture and validate the image Name
    read -rp "Enter the Desired Image name: " Image2Run
    if [ $Image2Run == "" ];
    then
# Complain about the lack of image name and stop
    echo "Please enter a container name...."   
    else
# Display the selected image.
    echo "You Entered: "$Image2Run
    fi
}

ChooseLoadedImage(){
	sudo podman images -a 
		# Capture and validate the image Name
    read -rp "Enter the Desired Image name: " thisimage
    if [ $thisimage == "" ];
    then
# Complain about the lack of image name and stop
    echo "Please enter a name or ID...."   
    else
# Display the selected image.
    echo "You Entered: "$thisimage
    fi
}

CreatePod(){
# Create a Pod with the desired name
    sudo podman pod create --name $PodName
}

SelectContainer(){
# Display available containers
    sudo podman ps -a --pod
# Capture desired container
    read -rp "Enter the Container name: " containername
    if [ $containername == "" ];
    then
# Complain about the lack of image name and stop
    echo "Please enter a container name...."   
    else
# Display the selected image.
    echo "You Entered: "$containername
    fi
}

ChooseFile(){
# Display yaml files in project
	ls $HOME/mk8sprojects/$loadproject/package
# Enter chosen log
	read -rp "Enter the name of the file to use: " thisfile
	if [[ "$thisfile" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo $thisfile
	fi
}

CommitContainer(){
	sudo podman commit --include-volumes --author "Boothe" $containername $PodName

}

CommitContainers(){

	sudo podman commit --include-volumes testmodeller_web testmodeller_web:modified
	sudo podman commit --include-volumes testmodeller_job_engine testmodeller_job_engine:modified
	sudo podman commit --include-volumes testmodeller_tdm testmodeller_tdm:modified
	sudo podman commit --include-volumes testmodeller_meta testmodeller_meta:modified
	sudo podman commit --include-volumes testmodeller_api testmodeller_api:modified
	sudo podman commit --include-volumes testmodeller_zookeeper testmodeller_zookeeper:modified
	sudo podman commit --include-volumes testmodeller_graph testmodeller_neo4j:modified
	sudo podman commit --include-volumes testmodeller_kafka testmodeller_bitnami_kafka:modified
	sudo podman commit --include-volumes testmodeller_nginx testmodeller_nginx:modified
	sudo podman commit --include-volumes testmodeller_db testmodeller_postgres_db:modified

}

ImageData(){
	podman images -a --format "table {{.ID}} {{.Repository}} {{.Tag}} {{.Names}} "

}

KubeGen(){
	mkdir $HOME/mk8sprojects/$loadproject/kubegenop
	cd $HOME/mk8sprojects/$loadproject/kubegenop
	sudo podman generate kube -s -f testmodeller_postgres_db.yaml testmodeller_db 
	sudo podman generate kube -s -f testmodeller_zookeeper.yaml testmodeller_zookeeper
	sudo podman generate kube -s -f testmodeller_bitnami_kafka.yaml testmodeller_kafka 
	sudo podman generate kube -s -f testmodeller_neo4j.yaml testmodeller_graph 
	sudo podman generate kube -s -f testmodeller_job_engine.yaml testmodeller_job_engine 
	sudo podman generate kube -s -f testmodeller_api.yaml testmodeller_api 
	sudo podman generate kube -s -f testmodeller_tdm.yaml testmodeller_tdm 
	sudo podman generate kube -s -f testmodeller_web.yaml testmodeller_web 
	sudo podman generate kube -s -f testmodeller_meta.yaml testmodeller_meta 
	sudo podman generate kube -s -f testmodeller_nginx.yaml testmodeller_nginx
	cd $HOME/mk8sprojects/$loadproject/
	ls $HOME/mk8sprojects/$loadproject/kubegenop
}

KubePlay(){
	cd $HOME/mk8sprojects/$loadproject/kubegenop
	
	sudo podman play kube testmodeller_nginx.yaml
	sudo podman play kube testmodeller_web.yaml
	sudo podman play kube testmodeller_api.yaml
	sudo podman play kube testmodeller_meta.yaml
	sudo podman play kube testmodeller_postgres_db.yaml
	sudo podman play kube testmodeller_zookeeper.yaml
	sudo podman play kube testmodeller_bitnami_kafka.yaml
	sudo podman play kube testmodeller_neo4j.yaml
	sudo podman play kube testmodeller_tdm.yaml
	sudo podman play kube testmodeller_job_engine.yaml

}

KubeDown(){
	cd $HOME/mk8sprojects/$loadproject/kubegenop
	sudo podman play kube --down testmodeller_nginx.yaml
	sudo podman play kube --down testmodeller_web.yaml
	sudo podman play kube --down testmodeller_api.yaml
	sudo podman play kube --down testmodeller_meta.yaml
	sudo podman play kube --down testmodeller_postgres_db.yaml
	sudo podman play kube --down testmodeller_zookeeper.yaml
	sudo podman play kube --down testmodeller_bitnami_kafka.yaml
	sudo podman play kube --down testmodeller_neo4j.yaml
	sudo podman play kube --down testmodeller_tdm.yaml
	sudo podman play kube --down testmodeller_job_engine.yaml

}

RunComposeInTab(){
	cd $HOME/mk8sprojects/$loadproject/package
	gnome-terminal --tab -- sudo docker-compose -f $thisfile up
}

RunCompose(){
	cd $HOME/mk8sprojects/$loadproject/package
	sudo docker-compose -f $thisfile up
#	sudo docker-compose -f docker-compose-k8s.yml down
}

InspectContainer(){
	sudo podman inspect $containername
}

ViewLogs(){
	sudo podman logs $containername
}

Attach2Container(){
    sudo podman attach $containername
}

ImportImages(){
# Import local images
# https://docs.podman.io/en/latest/markdown/podman-import.1.html
#    sudo podman import $desiredtarball
#    podman run -dt --pod
	cd $HOME/mk8sprojects/$loadproject/package
	sudo podman import modeller_api.tar modeller_api.tar:testmodeller
	sudo podman import modeller_db.tar modeller_db.tar:testmodeller
	sudo podman import modeller_job_engine.tar job_engine.tar:testmodeller
	sudo podman import modeller_kafka.tar kafka.tar:testmodeller
	sudo podman import modeller_meta.tar meta.tar:testmodeller
	sudo podman import modeller_neo4j.tar neo4j.tar:testmodeller
	sudo podman import modeller_nginx.tar nginx.tar:testmodeller
	sudo podman import modeller_tdm.tar tdm.tar:testmodeller
	sudo podman import modeller_web.tar web.tar:testmodeller
	sudo podman import modeller_zookeeper.tar zookeeper.tar:testmodeller
} 

LoadImages(){
# Import local images
# https://docs.podman.io/en/latest/markdown/podman-import.1.html
#    sudo podman import $desiredtarball
#    podman run -dt --pod
	cd $HOME/mk8sprojects/$loadproject/package
	sudo podman load --input modeller_api.tar # modeller_api.tar:testmodeller
	sudo podman load --input modeller_db.tar # modeller_db.tar:testmodeller
	sudo podman load --input modeller_job_engine.tar #job_engine.tar:testmodeller
	sudo podman load --input modeller_kafka.tar #kafka.tar:testmodeller
	sudo podman load --input modeller_meta.tar #meta.tar:testmodeller
	sudo podman load --input modeller_neo4j.tar #neo4j.tar:testmodeller
	sudo podman load --input modeller_nginx.tar #nginx.tar:testmodeller
	sudo podman load --input modeller_tdm.tar #tdm.tar:testmodeller
	sudo podman load --input modeller_web.tar #web.tar:testmodeller
	sudo podman load --input modeller_zookeeper.tar #zookeeper.tar:testmodeller
} 

LoadOCIImages(){
# Load created images
# https://docs.podman.io/en/latest/markdown/podman-import.1.html
#    sudo podman import $desiredtarball
#    podman run -dt --pod
	cd $HOME/mk8sprojects/$loadproject/oci
	sudo podman load --input testmodeller_api
	sudo podman load --input testmodeller_job_engine
	sudo podman load --input testmodeller_neo4j
	sudo podman load --input testmodeller_postgres_db
	sudo podman load --input testmodeller_web
	sudo podman load --input testmodeller_bitnami_kafka
	sudo podman load --input testmodeller_meta
	sudo podman load --input testmodeller_nginx
	sudo podman load --input testmodeller_tdm
	sudo podman load --input testmodeller_zookeeper
} 

Add2ModellerPod(){

	sudo podman pod create --name testmodeller  
	sudo podman run -d --name api --pod testmodeller localhost/testmodeller_api:1.16.110
	sudo podman run -d --name postgresdb --pod testmodeller localhost/testmodeller_postgres_db:1.0.3
	sudo podman run -d --name jobengine --pod testmodeller localhost/testmodeller_job_engine:1.16.75 
	sudo podman run -d --name kafka --pod testmodeller localhost/bitnami/kafka:2.8.1 
	sudo podman run -d --name meta --pod testmodeller localhost/testmodeller_meta:1.16.41
	sudo podman run -d --name neo4j --pod testmodeller localhost/neo4j:4.4.4 
	sudo podman run -d --name nginx --pod testmodeller localhost/nginx:1.19 
	sudo podman run -d --name tdm --pod testmodeller localhost/testmodeller_tdm:1.16.54 
	sudo podman run -d --name web --pod testmodeller localhost/testmodeller_web:1.16.122
	sudo podman run -d --name zookeeper --pod testmodeller localhost/zookeeper:3.8


#	sudo podman run -d --name api --pod testmodeller localhost/modeller_api:1.16.93 
#	sudo podman run -d --name postgresdb --pod testmodeller localhost/modeller_db:1.0.3
#	sudo podman run -d --name jobengine --pod testmodeller localhost/job_engine:1.16.75 
#	sudo podman run -d --name kafka --pod testmodeller localhost/bitnami/kafka:2.8.1 
#	sudo podman run -d --name meta --pod testmodeller localhost/meta:1.16.35 
#	sudo podman run -d --name neo4j --pod testmodeller localhost/neo4j:4.4.4 
#	sudo podman run -d --name nginx --pod testmodeller localhost/nginx:1.19 
#	sudo podman run -d --name tdm --pod testmodeller localhost/tdm:1.16.34 
#	sudo podman run -d --name web --pod testmodeller localhost/web:1.16.92 
#	sudo podman run -d --name zookeeper --pod testmodeller localhost/zookeeper:3.8
}

SaveImages(){
# Save local images
# https://docs.podman.io/en/latest/markdown/podman-import.1.html

	cd $HOME/mk8sprojects/$loadproject/kubegenop
	mkdir $HOME/mk8sprojects/$loadproject/saved
	sudo podman save localhost/testmodeller_api:1.16.110 -o $HOME/mk8sprojects/$loadproject/saved/modeller_api.tar
	sudo podman save localhost/testmodeller_postgres_db:1.0.3 -o $HOME/mk8sprojects/$loadproject/saved/modeller_db.tar
	sudo podman save localhost/testmodeller_job_engine:1.16.89 -o $HOME/mk8sprojects/$loadproject/saved/modeller_job_engine.tar
	sudo podman save localhost/bitnami/kafka:2.8.1 -o $HOME/mk8sprojects/$loadproject/saved/modeller_kafka.tar
	sudo podman save localhost/testmodeller_meta:1.16.41 -o $HOME/mk8sprojects/$loadproject/saved/modeller_meta.tar
	sudo podman save localhost/neo4j:4.4.4 -o $HOME/mk8sprojects/$loadproject/saved/modeller_neo4j.tar
	sudo podman save localhost/nginx:1.19 -o $HOME/mk8sprojects/$loadproject/saved/modeller_nginx.tar
	sudo podman save localhost/testmodeller_tdm:1.16.54 -o $HOME/mk8sprojects/$loadproject/saved/modeller_tdm.tar
	sudo podman save localhost/testmodeller_web:1.16.122 -o $HOME/mk8sprojects/$loadproject/saved/modeller_web.tar
	sudo podman save localhost/zookeeper:3.8 -o $HOME/mk8sprojects/$loadproject/saved/modeller_zookeeper.tar
} 

SaveImages2oci(){
# Save local images
# https://docs.podman.io/en/latest/markdown/podman-import.1.html

	
	mkdir $HOME/mk8sprojects/$loadproject/oci
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_postgres_db localhost/testmodeller_postgres_db:1.0.3
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_zookeeper localhost/zookeeper:3.8
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_bitnami_kafka localhost/bitnami/kafka:2.8.1
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_neo4j localhost/neo4j:4.4.4
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_job_engine localhost/testmodeller_job_engine:1.16.75
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_api localhost/testmodeller_api:1.16.93
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_tdm localhost/testmodeller_tdm:1.16.34
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_web localhost/testmodeller_web:1.16.92
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_meta localhost/testmodeller_meta:1.16.35
	sudo podman save --format oci-archive -o $HOME/mk8sprojects/$loadproject/oci/testmodeller_nginx localhost/nginx:1.19
} 

AddImage2Pod(){

    sudo podman run -d --pod $PodName $Image2Run $Command2Run       
#    Image2Run="docker.io/library/alpine:latest"

}

BuildContainerAndPod(){
# https://mohitgoyal.co/2021/04/23/spinning-up-and-managing-pods-with-multiple-containers-with-podman/
	sudo podman run \
-d --restart=always --pod new:$PodName \
#-e MYSQL_ROOT_PASSWORD="myrootpass" \
#-e MYSQL_DATABASE="wp-db" \
#-e MYSQL_USER="wp-user" \
#-e MYSQL_PASSWORD="w0rdpr3ss" \
--name=$containername $Image2Run
}

MakeCon(){
	read -rp "Enter desired port to use: " thisport
	sudo podman run -d -p $thisport:$thisport --restart=always --name=$containername $thisimage
}

MakeWPPod(){
# Create a Pod containing WordPress & MariaDB
# Set environment variables:
DB_NAME='wordpress_db'
DB_PASS='mysupersecurepass'
DB_USER='justbeauniqueuser'
POD_NAME='wordpress_with_mariadb'
CONTAINER_NAME_DB='wordpress_db'
CONTAINER_NAME_WP='wordpress'

mkdir -p html
mkdir -p database

# Remove previous attempts
podman pod rm -f $POD_NAME

# Pull before run, bc: invalid reference format error
podman pull docker.io/mariadb:latest
podman pull docker.io/wordpress

# Create a pod instead of --link. 
# So both containers are able to reach each others.
podman pod create -n $POD_NAME -p 8090:80

podman run --detach --pod $POD_NAME \
-e MYSQL_ROOT_PASSWORD=$DB_PASS \
-e MYSQL_PASSWORD=$DB_PASS \
-e MYSQL_DATABASE=$DB_NAME \
-e MYSQL_USER=$DB_USER \
--name $CONTAINER_NAME_DB -v "$PWD/database":/var/lib/mysql \
docker.io/mariadb:latest

podman run --detach --pod $POD_NAME \
-e WORDPRESS_DB_HOST=127.0.0.1:3306 \
-e WORDPRESS_DB_NAME=$DB_NAME \
-e WORDPRESS_DB_USER=$DB_USER \
-e WORDPRESS_DB_PASSWORD=$DB_PASS \
--name $CONTAINER_NAME_WP -v "$PWD/html":/var/www/html \
docker.io/wordpress

}

MakeSudoWPPod(){
#	sudo podman pod create --name TestPod -p 80:80 #--network=package_default
#	sudo podman run -d --restart=always --pod new:TM 
#	sudo podman run -d --restart=always --pod TestPod --name=TestCon testmodeller_api 
#!/bin/bash
# https://stackoverflow.com/questions/74054932/how-to-install-and-setup-wordpress-using-podman

# Set environment variables:
DB_NAME='wordpress_db'
DB_PASS='mysupersecurepass'
DB_USER='justbeauniqueuser'
POD_NAME='wordpress_with_mariadb'
CONTAINER_NAME_DB='wordpress_db'
CONTAINER_NAME_WP='wordpress'

mkdir -p html
mkdir -p database

# Remove previous attempts
sudo podman pod rm -f $POD_NAME

# Pull before run, bc: invalid reference format error
sudo podman pull docker.io/mariadb:latest
sudo podman pull docker.io/wordpress

# Create a pod instead of --link. 
# So both containers are able to reach each others.
sudo podman pod create -n $POD_NAME -p 8080:80

sudo podman run --detach --pod $POD_NAME \
-e MYSQL_ROOT_PASSWORD=$DB_PASS \
-e MYSQL_PASSWORD=$DB_PASS \
-e MYSQL_DATABASE=$DB_NAME \
-e MYSQL_USER=$DB_USER \
--name $CONTAINER_NAME_DB -v "$PWD/database":/var/lib/mysql \
docker.io/mariadb:latest

sudo podman run --detach --pod $POD_NAME \
-e WORDPRESS_DB_HOST=127.0.0.1:3306 \
-e WORDPRESS_DB_NAME=$DB_NAME \
-e WORDPRESS_DB_USER=$DB_USER \
-e WORDPRESS_DB_PASSWORD=$DB_PASS \
--name $CONTAINER_NAME_WP -v "$PWD/html":/var/www/html \
docker.io/wordpress

}

DeleteLocalImage(){

	# Capture and validate the image Name
    read -rp "Enter the Desired Image name: " thisimage
    if [ $thisimage == "" ];
    then
# Complain about the lack of image name and stop
    echo "Please enter a name or ID...."   
    else
# Display the selected image.
    echo "You Entered: "$thisimage
    fi
	sudo podman image rm -f $thisimage
}

RemoveContainers(){
	# Capture and validate the image Name
    read -rp "Enter the Desired Image name: " containername
    if [ $containername == "" ];
    then
# Complain about the lack of image name and stop
    echo "Please enter a name or ID...."   
    else
# Display the selected image.
    echo "You Entered: "$containername
    fi
	sudo podman rm -f $containername
}


# ================== Main Menu ====================

MainMenu(){
# Clears the screen
	clear
# Define this menus title.	
	MenuTitle=" Pod Manager "
# Describe what this menu does
	Description=" Development tool for managing Pods and Containers "

# Display the menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "  
	echo "-----------------------------------"
	echo "0.  Install required software"
	echo "10.  Name a Pod"
	echo "11.  Create an empty Pod"
	echo "12.  Add an image to an existing pod"
	echo "13.  Create WordPress Pod @ localhost:8090"
	echo "14. Create WordPress Sudo Pod @localhost:8080"
	echo "15. Create Pod and Container from a local Image"
	echo "16. Create container from a loaded image."
	echo "18. Create a new project folder." == "19. Load an existing project."
	echo "20.  Attach to a container.  "
	echo "21.  Inspect a container."
	echo "22.  View container logs."
	echo "30.  Import and tag modeller tarballs"
	echo "31.  Save modeller tarballs"
	echo "32.  Load modeller images"
	echo "33.  Add modeller images 2 modeller pod."
	echo "34.  Restart a container"
	echo "35.  Delete containers of choice."
	echo "36.  Delete all Containers (by force)."
	echo "38.  Save Images as OCI format."
	echo "39.  Load OCI Images."
	echo "40.  Start pod"
	echo "41.  Stop pod"
	echo "42.  remove a pod"
	echo "43.  Pod stats"
	echo "44.  Top containers in pod"
	echo "50.  Generate kube"
	echo "51.  Play Kube"
	echo "52.  Compose Up a file."
	echo "53.  Compose Up a file in a new tab."
	echo "54.  Compose Down a file."
	echo "55.  Kube Down"
	echo "60.  Commit a Container."
	echo "61.  Commit modified Containers."
	echo "70.  Import Images from Package." 
    echo "80.  Display Pods and Containers"
    echo "81.  Display Images"
	echo "90.  Delete a local image (by force)."
	echo "100. "
	echo "99.  Quit				   Exit this Menu"
	echo " "
	echo " $lastmessage " 
	echo " " 
}

read_options(){
# Maps the displayed options to command modules	
	local choice
# Inform user how to proceed and capture input.	
	read -p "Enter the desired item number or command: " choice
# Execute selected command modules	
	case $choice in
        0)   Setup ;;
		10)  NamePod ;;
        11)  NamePod && CreatePod ;;
		12)  NamePod && ChooseImage && AddImage2Pod ;;
		13)  MakeWPPod ;;
		14)  MakeSudoWPPod ;;
		15)  NamePod && ChooseLoadedImage && SelectContainer && BuildContainerAndPod ;;
		16)  SelectContainer && ChooseLoadedImage && MakeCon ;;
		18)  CreateNewProject ;;
		19)  LoadProject ;;
		20)  SelectContainer && Attach2Container ;;
		21)  SelectContainer && InspectContainer ;;
		22)  SelectContainer && ViewLogs ;;
		30)  ImportImages && sudo podman images ;;
		31)  SaveImages && sudo podman images ;;
		32)  LoadImages ;;
		33)  Add2ModellerPod ;;
		34)  SelectContainer && sudo podman restart $containername ;;
		35)  sudo podman ps -a --pod && RemoveContainers ;;
		36)  sudo podman rm --force --all ;;
		38)  SaveImages2oci && sudo podman images ;;
		39)  LoadOCIImages ;;
		40)  NamePod && sudo podman pod start $PodName ;;
		41)  NamePod && sudo podman pod stop $PodName ;;
		42)  NamePod && sudo podman pod rm $PodName ;;
		43)  NamePod && sudo podman pod stats $PodName ;;
		44)  NamePod && sudo podman pod top $PodName ;;
		50)  KubeGen ;;
		51)  KubePlay ;;
		52)  ChooseFile && RunCompose  ;;
		53)  ChooseFile && RunComposeInTab  ;;
		54)  ChooseFile && sudo docker-compose -f $HOME/mk8sprojects/$loadproject/package/$thisfile down ;;
		55)  KubeDown ;;
		60)  SelectContainer && NamePod && CommitContainer ;;
		61)  CommitContainers ;;
		70)  ImportImages ;;
		80)  sudo podman pod list && echo "=========" && sudo podman ps -a --pod ;;
        81)  sudo podman images -a ;;
		82)   ;;
        90)  sudo podman images && DeleteLocalImage ;;
# Quit this menu
        99) clear && echo " See you later $USER! " && exit 0;;
# Capture non listed inputs and send to BASH.
		*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac
}

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done

}

# =================== Run Commands ===============
# commands to run in this file at start.
MainMenu

