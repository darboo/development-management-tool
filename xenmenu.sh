

SAI="sudo apt install -y "


XENinstall(){
	$SAI xen-hypervisor-amd64
	$SAI lvm2
	$SAI bridge-utils
	$SAI xen-tools
# must validate succesful install of XEN before rebooting
#	sudo reboot
}

XENconfig(){
# 		*** Create Logical Volume & Group ***
# configure LVM to create both the volume group called ‘vg0’ and the physical volume (extents) /dev/sdb1 which must be exclusive and have plenty of space.
    
	sudo vgcreate vg0 /dev/sdb1
	sudo vgcreate vg /dev/sdb1
	

# *** Create Network Bridge ***
    
    sudo brctl addbr xenbr0
    
    sudo brctl addif xenbr0 eth0
    
    sudo ifconfig xenbr0 up

# delete interface from bridge
    sudo brctl delif xenbr0 eth0

}

#       *** Data Options ***
BridgeData(){
	brctl show
}

DomSnapshot(){
    read -rp "Enter the name of the target VM: " VMname
    sudo lvcreate -s /dev/vg0/$VMname -n $VMname-backup -L5G
}

XENDetails(){
	sudo xl dmesg
}

listDOMs(){
	sudo xl list
}

HypeData(){
	sudo xl info
}

XenTop(){
	sudo xl top
}

#       *** Create Virtual Guests ***

CreatePVGuest(){
    sudo xl create -f /home/$USER/demato/dematopv.cfg
} 

nameVM(){
    read -rp "Enter the name of the target VM: " VMname
}

# captures user data to create a customized VM
customPVGuest(){
# --------- Custom VM data capture -----------
    read -rp "Enter the name of the target VM: " VMname
    read -rp "Enter the number of Virtual CPU's: " VCPUs
    read -rp "Enter the amount of Virtual RAM required: " VRAM
    read -rp "Enter the size of the virtual HDD: " VHDD
#    read -rp "Enter the OS release version: " OStype
#    read -rp "Enter the required architecture: " ArchType
    read -rp "Enter the SSH key path or blank for default" SSHKEYPATH
#    read -rp "Enter the software to be installed comma space seperated values: " SUAPIN  #-or read a pre made list

# If keypath not set use default location
if [ -z "$SSHKEYPATH" ]; then SSHKEYPATH=/home/$USER/.ssh/id_rsa.pub;
	else 
	echo " looking in $SSHKEYPATH "
	fi

# require user validation of inputed data
cVMarray=("$VMname" "$VCPUs" "$VRAM" "$VHDD" "$OStype" "$ArchType" "$SSHKEYPATH" "$SUAPIN" )
# for loop that iterates over each element in cVMarray
for i in "${cVMarray[@]}"
do
    echo $i
done 
	read -rp "Happy with your choices? hit y to continue or any key to re-enter data." Happy

if [ "$Happy" != "y" ]; then customVM
	else
	echo "Then lets continue "
	
fi
}
#       *** Guest Actions ***

LVsnapshot(){
    sudo lvcreate -s /dev/vg0/database-data -ndatabase-backup -L5G
}



#       *** Menu ***
XENmenu(){
XEN_menu() {

	echo ""	
	echo " XEN - MENU"
	echo "  "
        echo "------------------------"
	echo "10.  Install ZEN "
	echo "20.  Display DOMain list "
	echo "21.  Display XEN Installation Details "
	echo "30.   "
        echo "40.   "
	echo "50.   "
	echo "60.  Create PV Guest "
	echo "70.   "
	echo "80.   " 
	echo "99. Exit " 
        echo "------------------------"
	listDOMs
        echo "------------------------"

}

XEN_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		10) XENinstall ;;
		20) listDOMs ;;
		21) XENDetails & HypeData & XenTop ;;
		30)  ;;
		40)  ;;
		50)  ;;
		60) CreatePVGuest ;;
        61) CustomPVGuest ;;
		70)  ;;
		80)  ;;
		99) clear && echo "Goodbye $USER" && exit ;;
		*) echo -e "${RED} Sorry, can't do $choice. ${STD}" && sleep 2
	esac
}

while true
do
    XEN_menu
    XEN_options
done
}

XENmenu


source https://wiki.xenproject.org/wiki/Xen_Project_Beginners_Guide

