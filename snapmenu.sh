#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

REPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"
clear
SAI="sudo apt install -y "
SSI="sudo snap install"

# 4 ************************ Snap menu *******************************

SnapName(){
	read -rp "Enter the $Release Snap release to install: " instsnap
}

# ------------------------- Install a Snap ---------------------------
snapinsta(){
	Release="Stable"
	SnapName
	sudo snap install $instsnap
}

candidateinstall(){
	Release="Candidate"
	SnapName
	sudo snap install --candidate $instsnap
}

betainstall(){
	Release="Beta"
	SnapName
	snap install --beta $instsnap
}

edgeinstall(){
	Release="Edge"
	SnapName
	snap install --edge $instsnap
}

snapclassic(){
	Release="Stable"
	SnapName
	sudo snap install $instsnap --classic
}

# ------------------------------------------------

snapquery(){
	read -rp "Search the SnapStore for: " findsnap
	snap find $findsnap
}
snapupd(){
	read -rp "Enter the Snap to update or leave empty for all: " updsnap
	snap refresh $updsnap
}
snaprem(){
	read -rp "Enter the Snap to remove: " remsnap
	sudo snap remove $remsnap
}
snapdet(){
	read -rp "Display Details about which Snap? " detsnap
	snap info $detsnap
}
# --------------------------------------------------
snapmenu() {
snap_menu() {

	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " SNAP - MENU"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1.  Install a Stable Snap Package"
	echo "2.  Install a Candidate Snap Package"
	echo "3.  Install a Beta Snap Package"
	echo "4.  Install an Edge Snap Package"
    echo "5.  Install a Stable Snap in 'Classic Mode' "
	echo "20. Search for a Snap"
	echo "30. Update one or all Snaps"
	echo "40. Remove a Snap"
	echo "50. List Snaps "
	echo "60. Snap Details"
	echo "99. Return to Main Menu "
	echo " -- Installed Snaps -- " && snap list
	echo " -- "
}

snap_options(){
	local SnapOptions
	read -p "Enter choice [ 1 - 99] " SnapOptions
	case $SnapOptions in
		1)  snapinsta ;;
		2)  candidateinstall ;;
		3)  betainstall ;;
		4)  edgeinstall ;;
        5)  snapclassic ;;
		20) snapquery ;;
		30) snapupd ;;
		40) clear && snap list && snaprem && snap list;;
		50) clear && snap list ;;
		60) clear && snap list && snapdet ;;
		90)  ;;
		99) exit ;;
		*) echo -e "${RED} $SnapOptions is not available today.${STD}" && sleep 2
	esac
}

while true
do
	snap_menu
	snap_options
done
}

snapmenu
