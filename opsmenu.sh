#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'

REPO=" "
clear
SAI="sudo apt install -y "

RunCheck(){
if [[ ! -f "~/development-management-tool/ops0.txt" ]]; then echo "i wanna install" & servermenu ;
	else 
	lastmessage="Hello $USER" & servermenu
	fi
	
}

# 5 ------------------------------ Server Menu -----------------------------

SystemUUAA(){
    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get autoremove -y
    sudo apt-get autoclean
}


menusetup(){
# Install the required software for this menu 
	sudo apt-get update
	$SAI ssh
	$SAI git
	$SAI xclip
    touch ~/development-management-tool/ops0.txt
}

SSHkeyGenCopy(){
	printf 'y\n' | ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
	HostSSHKey=$(cat $HOME/.ssh/id_rsa.pub)
	DMT="Your SSH Key has been copied to the clipboard"
}

SSHkeycopy(){
	HostSSHKey=$(cat $HOME/.ssh/id_rsa.pub)
	echo $HostSSHKey
	DMT="Your SSH Key has been copied to the clipboard"
}

servermenu(){

server_menus(){

MenuTitle=" Server Options "
MenuDescription=" Perform maintainance and other usefull server commands "

	echo " "	
	echo " $MenuTitle " 
	echo " $MenuDescription " 
	echo " -------------------------------- "
	echo "10.  Update, Upgrade, Autoremove and Autoclean "
	echo "20.  Midnight Commander "
	echo "21.  TaskSel "
	echo "30.  "
	echo "40.  Generate an SSH Key and copy to the HostSSHKey variable"
	echo "41.  Generate a new SSH Key"
	echo "42.  Copy existing SSH Key to the HostSSHKey variable "
	echo "50.  "
	echo "70.   "
    echo "80.  Reboot Server "
	echo "81.  Shutdown Server "
	echo "90.   "
	echo "99. Return to Main Menu "
	echo
	echo "$lastmessage "
}

server_options(){

local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		10) SystemUUAA ;;
		20) mc ;;
		21) runtasksel ;;
		30) ;;
		40) SSHkeyGenCopy ;;
		41) ssh-keygen ;;
        42) SSHkeycopy ;;
		50)  ;;
		60)  ;;
		70)  ;;
		80) sudo reboot ;;
		81) sudo shutdown now ;;
		99) exit ;;
		*) echo -e "${RED} You can't do that..... ${STD}" && sleep 2
	esac

}
    while true
    do
	server_menus
	server_options
    done
}


 RunCheck
 servermenu
# SSHkeycopy