#!/bin/bash

# Ensures application starts in the users home folder.     
    cd $HOME

# ==== Prepare environments ====
STD='\033[0;0;39m'
RED='\033[0;41;30m'
BLU='\033[[38;5;61m'
clear

DevManTooREPO="git clone https://darboo@bitbucket.org/darboo/development-management-tool.git"    

# ==== Capture site details and examine returned data ====    
    
SiteToTest(){

#   Select protocol HTTP, HTTPS, GET, POST 

# define and validate site for test  
#  read -rp "Enter the target URL (http/https) or IP: " TargetUrl
   TargetUrl="https://micrometre.co.uk" 
# Removes the https:// from the URL so a folder with the same name can be created.    
    TidyName=$( echo $TargetUrl | awk -F "//" '{ print $2; }' )
    RequestFolder="/home/$USER/TestData/$TidyName"
}

Prep4Test(){
# Prepare test folders and documents.

# Prints the shortend URL.    
    echo -e "Creating folder in: "$URL2DIR2
    
# Validate folder does not already exist.

# create and enter the new test folder.     
    mkdir -pv $RequestFolder-tests/
    cd $RequestFolder-tests/
    
# Backup existing file (git auto stash/commit)    
    
# Removes existing files    
#    rm -v test-$RequestFolder-head.txt
    
# available user agents with -A switch
#   UserAgent=( Gecko )
}    


CaptureHead(){
# copy the page head contents and in/out comms to the site_url-tests/site_url-head.txt file. 
    curl -vs -I $TargetUrl &> /dev/stdout | tee $RequestFolder-head.txt
}
    

 var4url(){
    urlvar=$( curl -vs -i $TargetUrl &> /dev/stdout )
#    awk -v urlvar="$urlvar" '{ print $0; }'
#    echo $urlvar #| grep "*"
#    finding=$( awk -v urlvar=$urlvar '{ print $0; }' ) #| grep --color "HTTP" )


}

#	lxc exec $newcon -- RealIP=$( ip -4 addr show eth0 | grep inet | awk '{ print $4; }' | sed 's/\/.*$//' ); $RealIP

 Geturl(){
 # Capture target url contents into a -headandBody.txt file.
    curl -vs -i $TargetUrl &> /dev/stdout | tee $RequestFolder-headandBody.txt
    
 # display captured contents
 #   cat curltest/$RequestFolder-headandBody.txt
        
}

# Array containing things to examine target url for.

ShowHeadContents(){
# Display the servers response to mandatory page validation criteria.
# Array for page search variables
    DoesItContain=( Rebuilt Trying HTTP location Date Content-Type )

for var in "${DoesItContain[@]}"
do
  echo "${var}" 
 cat $RequestFolder-head.txt | grep -m 1 --color "${var}" 
 #result=$(( $your_command ) 2>&1)
#   awk -F "HTTP" '{ print $0; }'C
#    grep -w "${var}" curltest/$RequestFolder-headandBody.txt
  # do something on $var
done

}

ValidationTests(){
# These tests gather information in the header regarding the outcome of communications with the chosen server.
# Searches the URL for matching keywords and display some or all off that line.
HeadTestCriteria=( ServerResponseTest ConnectedToTest )
for var in "${HeadTestCriteria[@]}"
do
  ${var} | tee -a test-$RequestFolder-head.txt
  # do something on $var
done

# temporary break to keep functionality while i upgrade the below to the above format
ValidationTestsContinued
}


ServerResponseTest(){
    ServerResponseDescription="Display the server response code returned."
    ServerResponse=$(cat $RequestFolder-head.txt | grep -m 1 "< HTTP" | awk {'print $3" "$4;'})
    ServerResponseTitle="Returned Status Code:"
    echo $ServerResponseTitle"                      "$ServerResponse
}    

ConnectedToTest(){
    ConnectedToDescription="Display which port on the server you are connected to."
    ConnectedTo=$(cat $RequestFolder-head.txt | grep -m 1 "* Connected" | awk {'print $7;'})
    ConnectedToTitle="Connection Port:" 
    echo $ConnectedToTitle"                           "$ConnectedTo
}

ValidationTestsContinued(){
    # Check the server name returned.
    ServerName=$(cat $RequestFolder-head.txt | grep -m 1 "* Rebuilt" | awk {'print $5;'})
    echo "Returned Server Name:                     " $ServerName
    
    # Check the server IP address returned.
    ServerIP=$(cat $RequestFolder-head.txt | grep -m 1 "* Connected" | awk {'print $5;'} | sed -e 's/)//g' | sed -e 's/(//g' )
    echo "Returned IP address:                      " $ServerIP
    
    # Check the HTTP Version in use.
    httpVer=$(cat $RequestFolder-head.txt | grep -m 1 "< HTTP" | awk '{ print $2; }')
    echo "HTTP Version:                             " $httpVer
    
    # Display the agreed protocol to comunicate with server. 
    Agreedprotocol=$(cat $RequestFolder-head.txt | grep -H -m 1 "* ALPN, server" | awk '{print substr($0, index($0, $4))}')
    echo "Agreed protocol:                          " $Agreedprotocol
    
    # Check which web server software you are connected to.
    HostServerSoft=$(cat $RequestFolder-head.txt | grep -m 1 "Server:" | awk {'print $3;'})
    echo "Server Host software:                     " $HostServerSoft
    
    # Check which the web server OS you are connected to.
    HostServerOS=$(cat $RequestFolder-head.txt | grep -m 1 "Server:" | awk {'print $4;'} | sed -e 's/)//g' | sed -e 's/(//g' )
    echo "Server/Host OS:                           " $HostServerOS
    
    # Check which server you are connected to.
    SSLConnection=$(cat $RequestFolder-head.txt | grep -m 1 "* SSL connection using" | awk {'print $5" "$7;'})
    echo "SSL Connection:                           " $SSLConnection
    
    # Check which server you are connected to.
    UserAgent=$(cat $RequestFolder-head.txt | grep -m 1 "> User-Agent:" | awk {'print $3;'})
    echo "Chosen User Agent:                        " $UserAgent
    
    # Display some of the server cetificate information. 
    SSLCertSubject=$(cat $RequestFolder-head.txt | grep -H -m 1 "*  subject:" | awk '{print substr($0, index($0, $3))}')
    echo "Server Certificate-Subject:               " $SSLCertSubject
    
    # Display the agreed protocol to comunicate with server. 
    AltSSLCertSubject=$(cat $RequestFolder-head.txt | grep -H -m 1 "*  subjectAltName" | awk '{print substr($0, index($0, $4))}')
    echo "alternative certificate subject name:     " $AltSSLCertSubject
    
    # Display the agreed protocol to comunicate with server. 
  #  Agreedprotocol=$(cat $RequestFolder-head.txt | grep -H -m 1 "* ALPN, server" | awk '{print substr($0, index($0, $4))}')
  #  echo "Agreed protocol:              " $Agreedprotocol
}

ShowHeadandBodyContents(){

# Array for page search variables
    DoesItContain=( Trying HTTP location Date Content-Type )

for var in "${DoesItContain[@]}"
do
   # echo "${var}" 
 cat $RequestFolder-headandBody.txt | grep --color "${var}" 
 #result=$(( $your_command ) 2>&1)
#   awk -F "HTTP" '{ print $0; }'
#    grep -w "${var}" curltest/$RequestFolder-headandBody.txt
  # do something on $var
done
}

# ***************************************************************************

# ==================== Data Handling ======================
RequestCapture(){
# Capture request data
# menu options to either remove, edit or add a rule
    read -rp "Rule to remove. " RuleRemove
    read -rp "Rule to update. " RuleEdit
    read -rp "Rule to add. " RuleAdd   
# Rule Requirements types option
    read -rp "Singular Line Rule?"
    read -rp "Floor & Ceiling Rule?" 
# Rule statement to validate
    read -rp "Rule description " RuleStatement
# Banner Group
    read -rp "Banner Group" BannerGroup
# Expected Banner
    read -rp "BannerID" BannerID
# Ranking Level
    read -rp "Expected Rank" Ranking
# Target Price

# Ceiling Price

# Floor Price

# Boolean options

    Hascopper=
    HasFibre=

}

NewRequest(){
# Capture the new request ID data
    read -rp "Enter the given Request ID (leave blank to quit): " JobID 
# Create folder based on Request ID.  
    if [ "$JobID" = "" ]; 
    then 
    echo "Please enter a Request ID"
# Prepare test folder.
	else RequestFolder="/$HOME/TestData/$JobID-$USER"
	fi
# Validate folder does not already exist.
if [ ! -f "$RequestFolder" ]; 
	then 
    # create and enter the new test folder.     
    mkdir -pv $RequestFolder/
	lastmessage="Created folder in: $RequestFolder" ;
	else 
	lastmessage="Sorry $USER, That folder already exists, try again."
    RequestID
	fi
}

RequestID(){
# Display existing projects list folders 
    ls /$HOME/TestData/

# Capture the request ID data
    read -rp "Enter the given Request ID (leave blank to quit): " JobID 
# Create folder based on Request ID.  
    if [ "$JobID" = "" ]; 
    then 
    echo "Please enter a Request ID"
# Prepare test folder.
	else ls $RequestFolder
	fi

}

# ========================== SQL Data Handling ==============

CaptureSQLheader(){
# copy the returned header data into an array
    SQLreturn="TestData.txt"
    head -1 $SQLreturn | while IFS=, read -a SQLheader; 
    do   echo ${SQLheader[@]}; 
    # Display number of elements in the array  
    echo "${#SQLheader[@]} header elements captured."
    done

}


SelectorMenu(){

# copy the returned header data into an array
    SQLreturn="TestData.txt"
# Copy the first line of the file, remove " from each field and read into array
   declare -a SQLheader
   SQLheads="$( head -1 $SQLreturn )"
   SQLheader+=($SQLheads) 
    #| sed 's/,//g'  #| while IFS=, read -a SQLheader; 
#  do  
   echo ${SQLheader[@]} ; 

    Choice="${SQLheads[0]}"
echo $Choice
# Display number of elements in the array  
    echo "${#SQLheads[@]} header elements captured."
#    done

selection(){ 
    local selection=$1
    if [[ ${opts[selection]} ]] # toggle
    then
        opts[selection]=
    else
        opts[selection]=Selected
    fi 
}
 
while :
do
 
# Displayed Options
    options=("$Choice ${opts[1]}" "Option 2 ${opts[2]}" "Option 3 ${opts[3]}" "Option 4 ${opts[4]}" "Done")

# Select options 
    select opt in "${options[@]}"
    do
        case $opt in
            "$Choice ${opts[1]}")
                selection 1 # Marks as selected
                break
                ;;
            "Option 2 ${opts[2]}")
                selection 2
                break
                ;;
            "Option 3 ${opts[3]}")
                selection 3
                break
                ;;
            "Option 4 ${opts[4]}") 
                selection 4 
                break ;;
            "Done") break 2 ;;
            *) printf '%s\n' 'invalid option';;
        esac
    done
done

echo " "
printf '%s\n' #'Options chosen:'
for opt in "${!opts[@]}"
do
    if [[ ${opts[opt]} ]]
    then
     #printf '%s\n' "Option $opt"
        chosen="Option $opt"
    fi

echo $chosen

done


}

CaptureSQLdata(){
# copy the returned data into an array
# Set working location of files
    DataFile="/$HOME/TestData/DataLine.txt" # Output File
    SQLreturn="TestData.txt" # Source File
# delete existing output file.
    rm $DataFile
# Setup counter for each pass
    C=1
# pass all lines into Arrays except the first (header).    
    tail -n +2 $SQLreturn | sed '/^$/d' | while IFS=, read -a SQLdata; 
# Append file with each line
    do  echo "Test$C , ${SQLdata[@]}" >> $DataFile
    C=$((C+1))
    done
#    cat /home/$USER/TestData/DataLine.txt
# Display number of elements in the array  
 #   echo "${#SQLdata[@]} elements captured."
}

CaptureSQLdata2(){
# copy the returned data into an array
    SQLreturn="TestData.txt"
# Setup counter for each pass
    C=1
# delete existing file, or not.
    rm /home/$USER/TestData/DataLine2.txt
    rm /home/$USER/TestData/soapUI2.txt
# Display 
    CustID=${SQLdata[0]}

# pass all lines into Arrays except the first (header).   
#   tail -n +2 $SQLreturn | grep -v "^$" | while IFS=, read -a SQLdata; 
    tail -n +2 $SQLreturn | sed '/^$/d' | while IFS=, read -a SQLdata; 

# Append file with each line
    do  echo "TestID_$C,${SQLdata[@]}" >> /home/$USER/TestData/DataLine2.txt
# Create a file for SoapUI to pass to Pega  
    echo "TestID_$C,$CustID" >> /$HOME/TestData/soapUi2.txt
# Increment the counter on each pass 
    C=$((C+1))
    done
    
#   cat /home/$USER/TestData/DataLine.txt
# Display number of elements in the array  
   echo "${#AllSQLdata[@]} elements captured."

}

# ========================= Main Menu ========================

TestOptions(){

MenuTitle=" Test Menu "
Description=" Data aquisition and management tools "

        test_menu(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "
	echo "-----------------------------------"
	echo ""
	echo "1.  Server to test: "$TargetUrl
	echo "2.  Prepare files for testing "
	echo "3.  New Request "
	echo "4.  Existing Request: "$JobID
	echo "5.  Request Statement Capture"
	echo "6.  Scatter Gun query "
    echo "7.  Create SQL Queries  "
    echo "8.   "
	echo "9.   "
	echo "10. Returned SQL Column Headers: " echo "${#SQLheader[@]}"
	echo "11. Returned SQL Data Rows"
	echo "12. Select Headers "
	echo "20. CaptureSQLdata2 "
	echo "21. "
	echo "99. Exit " 
    echo "------------------------"
    echo " $lastmessage "
	echo "------------------------"

}
     test_options(){
	local choice
	read -p "Enter choice [ 1 - 99] " choice
	case $choice in
		1)  SiteToTest ;;
		2)  Prep4Test ;;
		3)  NewRequest ;;
		4)  RequestID ;;
		5)  RequestCapture ;;
		6)   ;;
        7)   ;;
        8)   ;;
		9)   ;;
		10)  CaptureSQLheader ;;
		11)  CaptureSQLdata ;;
		12)  SelectorMenu ;;
		20)  CaptureSQLdata2 ;;
		21)  ;;
		99) clear && exit ;;
		*) echo -e "${RED} $choice is not a displayed option.....${STD}" && sleep 2
        
	esac
}

while true
do
	test_menu
	test_options
done
}   

# required to start the menu
TestOptions

#SoftwareInstall
#BuildServers
# *** Mandatory ***
SiteToTest
Prep4Test
# *** 
#CaptureHead
# 
#ShowHeadContents               # 
ValidationTests                 # also prins to the file test-*-head.txt
#var4url
#Geturl
#ShowHeadandBodyContents

#makeGitLab
#MakeServers

    # validate http response code 200 and continue, else state error code.
    # changing user-agent for curl 
