﻿# Development Management Tools
BASH menu for commonly used software development and management tools.  

As CLI tools require the user to familiarise themselves with each individual language, this menu attempts to display options in a simple, self explanatory way to perform specific tasks in each.  

Easily install GIT, LXD, KVM, Docker, Ansible and other useful tools.  
Assist in performing System Management/Maintenance and more via an interactive shell script.  

# Options  
Create contained environments for development of software within LXD or Docker.  
Setup SSH on host to connect to target container.  
Login to containers via a seperate terminal as Root or Ubuntu users.  
Create an application server with access via SSH -X.  
Install and perform Boot Repair.  
Attempt to fix broken packages.  
And more …  


Software is experimental and incomplete, comes with no warranty or guarentee. there is a great deal more which can be added to this but time means I update what is needed, when It’s needed.   
More scripts are displayed in the list above than are called by the script, these are no longer required as a better method may have been developed ie; deploying pre-configured containers with a script is usefull but prone to breakage as thing change, but an Ansible script to do the same job Is less likely to break as it manages the provision task envoked in the script.  
For now they remain for reference & educational purposes.   


# Installation  
Developing and testing on Ubuntu 18.04 AMD/INTEL/ARM 64BIT devices.  

1. Open a terminal  
2. Clone into your home folder  
3. Run: ~/development-management-tools/devmantoo.sh  

This copies a path in to your bash_aliases file  
You can now type dmt from anywhere to run this script.  

# Usage  
Should all goes well, you should be presented with the below:  

  - Development Management Tools -
  Menu for software development, environment creation and managment.
Working on the Git Branch: * master
-----------------------------------
0.  Update Development Management Tool
1.  System configuration, maintenance & management.
2.  LXD                 Container Virtualisation
3.  GIT                 Version control options
4.  Application Server Menu
6.  KVM/UVT             Virtual Machine tools
7.  Docker              More Container Virtualisation
8.  Testing             Test web & API services
9.  RoR Maker           Menu to create Rails apps
10. Ansible
Q.  Quit                        Exit this Menu

 Welcome back first

Enter the desired item number:


# Whats what  
0.  Update Development Management Tool
Check for and apply any update.

1.  System configuration, maintenance & management.
Contains the submenu:

  Admin Menu
  System administration and management tools
-----------------------------------

1.  Install GIT, SSH, Boot Repair,nano, xclip, etc.
2.  Install Ansible
3.
4.  Update the Host machine
5.  Upgrade the Host machine
6.  Autoremove
7.  Autoclean
8.  Release Upgrade
9.  Fix Broken Packages
10. SSH Setup
11. SSH Login
12. Boot Repair
13. Display all groups and those this user is atached to.
14. Add a user to sudo group (as Root User)
15. view user ID, subuid & subgid info.
20. Create a new User
30. View Source List
31. Add backports to M16 source list
80. Reboot Server
81. Shutdown Server
99. Return to Main Menu
------------------------

------------------------
Enter choice or BaSH command:

Some of these sub-menus contain an option to execute commands directly from the option input line, which saves one from exiting the menu.  

2.  LXD                 Container Virtualisation
Linux container managment 

3.  GIT                 Version control options
4.  Application Server Menu
6.  KVM/UVT             Virtual Machine tools
7.  Docker              More Container Virtualisation
8.  Testing             Test web & API services
9.  RoR Maker           Menu to create Rails apps
10. Ansible
Q.  Quit                        Exit this Menu

 Welcome back first

Enter the desired item number:

 

 
 
 