#!/bin/bash

# V ----------------------- Variables & Arrays-------------------
RED='\033[0;41;30m'
STD='\033[0;0;39m'
SAI="sudo apt install -y "

# -------------- LXC Menu --------------

InstallLXC(){
# Update and autoremove
    sudo apt update && sudo apt autoremove && sudo apt upgrade
	$SAI software-properties-common    
    $SAI build-essential
# Install LXC 
    $SAI LXC
#	Install terminal konsole
	$SAI konsole
#	Browse servers with SSH using Nemo
	$SAI nemo
	$SAI zfsutils-linux
# install additional LXC tools
	$SAI lxc-utils
	$SAI criu
	$SAI libpam-cgfs
	sudo snap install -y opera

# Add user details to lxc-usernet  
    echo -e "$USER veth lxcbr0 10" >> /etc/lxc/lxc-usernet
# Create default.conf in home
    Cp /etc/lxc/default.conf to ~/.config/lxc/default.conf
# Add current user to LXC config.
    echo "lxc.idmap = u 0 100000 65536" >> ~/.config/lxc/default.conf
    echo "lxc.idmap = g 0 100000 65536" >> ~/.config/lxc/default.conf
# Display /etc/subuid and /etc/subgid values to the default.conf file.
    /etc/sub{uid,gid}
# Validate LXC config
    lxc-checkconfig
}

ShowVethBridge(){
#Display info about lxcbr0
    sudo brctl show
# DHCP range
    ps aux | grep lxc-dns | grep -o 'dhcp-range.[0-9].* '

}

ConfigUnprivUser(){
# 1 Create unpriveleged lxc user    
    sudo useradd -s /sbin/bash -c 'unprivileged lxc user' -m mylxcusr
# 2 Set the password for mylxcusr   
    sudo passwd mylxcusr
# 3 Find out allocated subuids and subgids for the lxc user    
    sudo grep mylxcusr /etc/sub{gid,uid}
# 4 # Add user details to lxc-usernet  
    echo -e "mylxcusr veth lxcbr0 10" >> /etc/lxc/lxc-usernet
# Create default.conf in home
    Cp /etc/lxc/default.conf to /home/mylxcusr/.config/lxc/default.conf
# Add current user to LXC config.
    echo "lxc.idmap = u 0 100000 65536" >> /home/mylxcusr/.config/lxc/default.conf
    echo "lxc.idmap = g 0 100000 65536" >> /home/mylxcusr/.config/lxc/default.conf
# Display /etc/subuid and /etc/subgid values to the default.conf file.
    /etc/sub{uid,gid}

}

# ------ Management Commands -------

ContName(){
# Capture Container Name
	read -rp "Enter the name of the target Container: " newcon
# Confirm data entered    
	if [[ "$newcon" == "" ]]
	then 
# Complain about the lack of data    
	echo "Enter a name please....."
	else
	echo "$newcon"
	fi
}

Attach2Conainer(){
# connect to chosen container
    lxc console -n $newcon 
}

StartContainer(){

    $ lxc start -n $newcon -d
}

Stopcontainer(){
    $ lxc stop -n $newcon
  
}

DeleteContainer(){
    lxc destroy -n $newcon
}

ListContainers(){
    lxc ls #--fancy

}

Command2Conainer(){
# Capture Commands for the Container
	read -rp "Enter the command for the target Container: " Com2Con
	if [[ "$Com2Con" == "" ]]
	then 
	echo "Enter a name please....."
	else
	echo "$Com2Con"
	fi
    lxc-attach -n $newcon $Com2Con
}


history(){
    1  lxc
    2  sudo apt install lxc
    3  second veth lxcbr0 10
    4  nano /etc/lxc/lxc-usernet
    5  fg
    6  sudo nano /etc/lxc/lxc-usernet
    7  systemd-run --unit=myshell --user --scope -p "Delegate=yes" lxc-start testcon1
    8  systemd-run --unit=myshell --user=first --scope -p "Delegate=yes" lxc-start testcon1
    9  systemd-run --unit=myshell --user --scope -p "Delegate=yes" lxc-start testcon1
   10  sudo systemd-run --unit=myshell --user --scope -p "Delegate=yes" lxc-start testcon1
   11  systemd-run --unit=myshell --user --scope -p "Delegate=yes" lxc-start testcon1
   12  lxc-create -t download -n testcon1
}



# ================== Main Menu ====================

MainMenu(){
# Clears the screen
	clear
# Define this menus title.	
	MenuTitle=" - LXC Menu  - "
# Describe what this menu does
	Description=" Interface for managing LXC  "

# Display the menu options
show_menus(){
	echo " "	
	echo " $MenuTitle "
	echo " $Description "  
	echo "-----------------------------------"
	echo "0.  Install LXC "
    echo "1.  Show the Virtual Ethernet Bridge"
    echo "2.  Configure unpriveleged User/Container"
	echo "10. Start a Container "
    echo "11. Attach to a Conainer "
    echo "12. Issue a command to a container "
    echo "13. Stop a container "
    echo "14. Delete a container "
	echo "20.  "
	echo "30. Display a list of containers  "
	echo "40.  "
	echo "50.  "
	echo "60.  "
	echo "70.  " 
    echo "80.  "
    echo "90.  "
	echo "98. "
	echo "99.  Quit				   Exit this Menu"
	echo " "
	echo " $lastmessage " 
	echo " " 
}

read_options(){
# Maps the displayed options to command modules	
	local choice
# Inform user how to proceed and capture input.	
	read -p "Enter the desired item number or command: " choice
# Execute selected command modules	
	case $choice in
        0)  InstallLXC ;;
        1)  ShowVethBridge ;;
        2)  ConfigUnprivUser ;;
		10) ContName && StartContainer ;;
        11) ContName && Attach2Conainer ;;
        12) ContName && Command2Conainer ;;
        13) ContName && Stopcontainer ;;
        14) ContName && DeleteContainer ;;
        15) ContName ;;
        16) ContName ;;
		20)  ;;
		30) ListContainers ;;
		40)   ;;
		50)   ;;
		60)   ;;
		70)   ;;
		80)   ;;
        90)   ;;
        100)  ;;
# Quit this menu
        99) clear && echo " See you later $USER! " && exit 0;;
# Capture non listed inputs and send to BASH.
		*) echo -e "${RED} $choice is not a displayed option, trying BaSH.....${STD}" && echo -e "$choice" | /bin/bash
	esac


}

# --------------- Main loop --------------------------
while true
do
	show_menus
	read_options
done
}

# =================== Run Commands ===============
# commands to run in this file at start.
MainMenu
